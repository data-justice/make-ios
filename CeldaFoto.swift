//
//  CeldaFoto.swift
//  FotoCarrete
//
//  Created by Roberto Martínez Román on 4/25/19.
//  Copyright © 2019 Roberto Martinez Roman. All rights reserved.
//

import UIKit

class CeldaOpcion: UICollectionViewCell
{
    var delegado: AnexosVC!
    var indexPath: IndexPath!
    
    @IBOutlet weak var imgFoto: UIImageView!
    
    @IBAction func borrar(_ sender: UIButton) {
        delegado.borrarCelda(indexPath!)
    }
    
}

