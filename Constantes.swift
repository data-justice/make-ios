//
//  Constantes.swift
//  MKE
//
//  Created by Roberto Martínez Román on 2/19/19.
//  Copyright © 2019 Roberto Martinez Roman. All rights reserved.
//

import Foundation
import UIKit

// Servicios
let URL_SERVIDOR = "https://make-sec.org/"


// Signup
let URL_SERVICIO_SIGN_UP = URL_SERVIDOR + "signup"

let URL_SERVICIO_LOGIN = URL_SERVIDOR + "login"
let ERROR_INFO_INVALIDA = 401
let ERROR_PASSWORD_INVALIDO = 422

let OK_LOGIN = 201

// RESTAURAR PASSWORD
let URL_SERVICIO_RESTAURAR_PASSWORD = "https://app.make-sec.org/password/reset"

// Incidentes
let URL_SERVICIO_INCIDENTE = URL_SERVIDOR + "user/incidents/"
let URL_SERVICIO_INCIDENTE_BORRAR = URL_SERVIDOR + "user/incidents/"    // ***id/
let OK_REGISTRO_BORRADO = 204
let ERROR_REGISTRO_NO_EXISTE = 404
// /user/incidents/***:event_id***/complements/
let URL_SERVICIO_INCIDENTE_COMPLEMENTO = "/complements/"

// INCIDENTES del grupo
let URL_GRUPO_INCIDENTES = URL_SERVIDOR + "user/groups/:id/incidents"   //  :id #isla, GET

// Attachments
let URL_SERVICIO_ATTACHMENT = URL_SERVIDOR + "user/attachments/"
let OK_ATTACHMENT_CREADO = 201

// Botón de pánico
let URL_BOTON_PANICO = URL_SERVIDOR + "user/panic_event"
let ERROR_COMPLEMENTO_LLAVE = 400
let NUMERO_TRAZAS = 7   // Además del evento principal
let MAX_CONTACTOS = 7   // 7 contactos máximo, aviso de emergencia

let TIEMPO_TRAZA = 60.0
let TIEMPO_VIDEO = 120.0    // 2 minutos

// Códigos de respuesta
let OK_INCIDENTES = 200
let OK_REGISTRO_CREADO = 201
let OK_REGISTRO_RECIBIDO = 201
let OK_REGISTRO_PANICO_CREADO = 201

let ERROR_REGISTRO_EVENTO_DUPLICADO = 400
let ERROR_MAIL_NO_VERIFICADO = 401
let CREDENCIALES_INVALIDAS_401 = 401
let ERROR_CREDENCIALES_INVALIDAS = 422
let ERROR_SERVIDOR = 500

let ERROR_CONEXION_INTERNET = -1009
let ERROR_SERVIDOR_NO_EXISTE = -1003

// LLAVES
let LLAVE_TOKEN = "token"
let LLAVE_USUARIO = "usuario"
let LLAVE_ID_USUARIO = "id"
let LLAVE_PASSWORD = "password"
let LLAVE_ID_ISLA = "idIsla"

// CÓDIGO POSTAL
let URL_SERVICIO_CODIGO_POSTAL = URL_SERVIDOR + "zipcodes/"  // + 99999
let OK_CONSULTA_CODIGO_POSTAL = 200

// ISLA DE CONFIANZA
let URL_CREA_ISLA = URL_SERVIDOR + "user/groups"   // post
let OK_ISLA_CREADA = 201
let ERROR_ALIAS_NO_EXISTE = 400

let URL_LISTA_GRUPOS = URL_SERVIDOR + "user/groups"    // get
let OK_LISTA_GRUPOS = 200

let URL_LEER_ISLA = URL_SERVIDOR + "user/groups/:id"    // GET
let OK_LEER_ISLA = 200

// ENCUESTAS
let URL_CREA_ENCUESTA = URL_SERVIDOR + "user/polls"   // POST
let OK_ENCUESTA_CREADA = 201
let ERROR_CREA_ENCUESTA = 400   // El grupo no existe o no hay opciones

let URL_LISTA_ENCUESTAS  = URL_SERVIDOR + "user/groups/:id/polls"
let OK_LISTA_ENCUESTAS = 200

let URL_ENVIA_RESPUESTA = URL_SERVIDOR +  "user/polls/votes/"   // get
let OK_ENVIA_RESPUESTA = 201
let ERROR_ENVIA_RESPUESTA = 422

// ROLES
let URL_LISTA_ROLES = URL_SERVIDOR + "group_roles"
let OK_LISTA_ROLES = 200

let URL_TEMPLATE_ROLES = URL_SERVIDOR +  "user/groups/:id/security_plans/template"
let OK_TEMPLATE_ROLES = 200

let URL_CREA_NUEVO_ROL = URL_SERVIDOR + "group_roles"
let OK_NUEVO_ROL_CREADO = 201

// MEMBERSHIPS
let URL_LISTA_MEMBERSHIPS = URL_SERVIDOR + "user/group_memberships"
let URL_ACEPTA_INVITACION = URL_SERVIDOR + "user/group_memberships/"  //:membership_id
let OK_ACEPTA_INVITACION = 200

// PLANES de SEGURIDAD - CREAR PLAN
let URL_CREA_PLAN_SEGURIDAD = URL_SERVIDOR + "user/groups/:id/security_plans"  // :id del grupo/isla  POST
let ERROR_CREAR_PLAN_SEGURIDAD = 422 // Ya existe plan de seguridad

// LISTA  PLANES DE SEGURIDAD - para crear el documento
let URL_LISTA_PLANES_DE_SEGURIDAD = URL_SERVIDOR + "user/groups/:id/security_plans" //GET
let OK_LISTA_PLANES_SEGURIDAD = 200
// Regresa [] si no existe el grupo

// ACTUALIZAR un PLAN DE SEGURIDAD existente - PUT
 let URL_ACTUALIZA_PLAN_SEGURIDAD = URL_SERVIDOR +  "user/groups/:group_id/security_plans/:plan_id"

// ACTIVAR PLAN DE SEGURIDAD
let URL_ACTIVA_PLAN_SEGURIDAD = URL_SERVIDOR + "user/groups/:id/activate_security_plan"    // :id de la isla

// BORRAR / AGREGAR miembros a la isla
let URL_BORRAR_MIEMBRO = URL_SERVIDOR + "user/groups/:id/members"   // + /alias DELETE
let OK_BORRAR_MIEMBRO = 204
let ERROR_BORRAR_MIEMBRO = 404
let ERROR_BORRAR_MIEMBRO_ACTIVO = 422

let URL_AGREGAR_MIEMBRO = URL_SERVIDOR + "user/groups/:id/members" // + alias, PUT
let OK_AGREGA_MIEMBRO = 201

// ASIGNAR MIEMBROS DE MODELOS DE GOBERNANZA
let URL_ASIGNAR_CENTRALIZADO = URL_SERVIDOR + "user/groups/:id/governance_model/centralized"
let OK_ASIGNAR_CENTRALIZADO = 200

// ASIGNA PESOS PARA EL MODELO DE GOBERNANZA PONDERADO (PUT)
let URL_ASIGNAR_PESOS_PONDERADO = URL_SERVIDOR + "user/groups/:id/governance_model/weighted"

// CAMBIA EL MODELO DE GOBERNANZA cuando está estable
let URL_CAMBIA_MG_ESTABLE = URL_SERVIDOR + "user/groups/:id/security_plans/stable"
let OK_CAMBIA_MG_ESTABLE = 200

// VISTA
let ALPHA: CGFloat = 0.75

let MAX_TAM_IMAGEN = 20*1024*1024   // 20 MB
let MAX_TAM_VIDEO = 350*1024*1024   // 350 MB
let MAX_TAM_AUDIO = 20*1024*1024    // 20 MB

let MENSAJE_CREDENCIALES_INVALIDAS = "Tu sesión ha expirado, haz login nuevamente."

// Para el reporte de evento
let arrTipoEvento = [ "Intento de secuestro", "Secuestro", "Robo", "Asalto", "Violación", "Tortura", "Extorsión" ]

let arrEstadosMexico = ["Aguascalientes","Baja California","Baja California Sur", "Campeche", "Chiapas", "Chihuahua", "Coahuila de Zaragoza", "Colima", "Ciudad de México", "Durango", "Guanajuato", "Guerrero", "Hidalgo", "Jalisco", "Michoacán de Ocampo", "Morelos", "México", "Nayarit", "Nuevo León", "Oaxaca", "Puebla", "Querétaro", "Quintana Roo", "San Luis Potosí", "Sinaloa", "Sonora", "Tabasco", "Tamaulipas", "Tlaxcala", "Veracruz de Ignacio de la Llave", "Yucatán", "Zacatecas" ]

// Reporte de incidente/evento
enum TipoEvento
{
    case nuevo
    case complemento
    case consulta
    case consultaComplemento
}

enum TipoRegistro
{
    case principal
    case complemento
}



// URL de mapa
// https://www.google.com/maps/search/?api=1&query=19.596596,-99.226592

// Campo de dirección a 150 caracteres de longitud. Cambiar los mensajes de regreso a error.

// PALETA de colores

let amarillo_make = [251/255, 221/255, 64/255]
let naranja_make = [232/255, 119/255, 34/255]
let morado_make = [101/255, 50/255, 121/255]
let azul_make = [5/255, 195/255, 222/255]

let floral_white = UIColor(displayP3Red: 1, green: 250/255, blue: 240/255, alpha: 1)
//[255/255, 250/255, 240/255]

// floral white     #FFFAF0     (255,250,240)
// linen     #FAF0E6     (250,240,230)

// Redondear botones
// layer.cornerRadius , 5


// TERMINANDO

/*
 3.       Revisar la causa por la que la App truena en UK LT
 
 */
