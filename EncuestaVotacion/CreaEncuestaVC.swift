//
//  CreaEncuestaVC.swift
//  MKE
//
//  Created by Roberto Mtz. Román on 8/27/19.
//  Copyright © 2019 Roberto Martinez Roman. All rights reserved.
//

import UIKit

class CreaEncuestaVC: UIViewController, UITextFieldDelegate, UITextViewDelegate
{
    @IBOutlet weak var scrollVista: UIScrollView!
    @IBOutlet weak var btnCrear: UIButton!
    @IBOutlet weak var vistaContenido: UIView!
    // Información para JSON
    @IBOutlet weak var tfTitulo: UITextField!
    @IBOutlet weak var tvDescripcion: UITextView!
    @IBOutlet weak var tfOpcion1: UITextField!
    @IBOutlet weak var tfOpcion2: UITextField!
    @IBOutlet weak var tfOpcion3: UITextField!
    @IBOutlet weak var tfOpcion4: UITextField!
    
    @IBOutlet weak var activEspera: UIActivityIndicatorView!
    
    // Scroll
    private var tfEditandoAhora: UITextField!
    
    // idIsla del controlador previo
    var idIsla: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tvDescripcion.layer.borderWidth = 0.5
        tvDescripcion.layer.cornerRadius = 3
        tvDescripcion.layer.borderColor = UIColor.lightGray.cgColor
        
        // Para hacer scroll de los campos
        NotificationCenter.default.addObserver(self, selector: #selector(tecladoAparece(notif:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(tecladoDesaparece(notif:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        // Calcula alto de la vista
        var frame = vistaContenido.frame
        if self.scrollVista.frame.height < btnCrear.frame.height + btnCrear.frame.origin.y {
            frame.size.height = btnCrear.frame.height + btnCrear.frame.origin.y + 10
        } else if frame.size.height > self.scrollVista.frame.height {
            frame.size.height = self.scrollVista.frame.height
        }
        vistaContenido.frame = frame
        // Calcula el tamaño del scroll
        scrollVista.contentSize = self.vistaContenido.frame.size
    }
    
    @IBAction func crearEncuesta(_ sender: UIButton) {
        enviarCreaEncuesta()
    }
    
    
    func enviarCreaEncuesta() {
        DispatchQueue.main.async {
            self.activEspera.startAnimating()
        }
        // Leer campos
        let titulo = self.tfTitulo.text!
        let descripcion = self.tvDescripcion.text!.addingPercentEncoding(withAllowedCharacters: .alphanumerics)
        let opcion1 = self.tfOpcion1.text!
        let opcion2 = self.tfOpcion2.text!
        let opcion3 = self.tfOpcion3.text!
        let opcion4 = self.tfOpcion4.text!
        
        let grupo = self.idIsla != nil ? self.idIsla! : 0
        
        var listaOpciones = [[String:String]]()
        
        if opcion1.count > 0 {
            let dOpcion = ["title":opcion1]
            listaOpciones.append(dOpcion)
        }
        if opcion2.count > 0 {
            let dOpcion = ["title":opcion2]
            listaOpciones.append(dOpcion)
        }
        if opcion3.count > 0 {
            let dOpcion = ["title":opcion3]
            listaOpciones.append(dOpcion)
        }
        if opcion4.count > 0 {
            let dOpcion = ["title":opcion4]
            listaOpciones.append(dOpcion)
        }
        
        if listaOpciones.count < 1 {
            DispatchQueue.main.async {
                self.activEspera.stopAnimating()
            }
            // Error
            mostrarMensajeInfo(mensaje: "Debes capturar opciones", cerrar: false)
            return
        }
        
        var jsonOpciones = ""
        // Formatea la lista de diccionarios
        if let jsonData = try? JSONSerialization.data(withJSONObject: listaOpciones, options: .prettyPrinted), let jsonText = String(data: jsonData, encoding: .utf8) {
            jsonOpciones = jsonText
        } else {
            //print("Error en el formato JSON")
        }
        
        // Token para authorization
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        // Enviar información al servidor, recibe token
        // 400 error en la petición, 201 ok
        let url = URL(string: URL_CREA_ENCUESTA)
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-type")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.httpBody = """
            {
            "group": \(grupo),
            "title": "\(titulo)",
            "description": "\(descripcion!)",
            "choices": \(jsonOpciones)
            }
            """.data(using: .utf8)

        //print("Opciones: \(listaOpciones), len=\(listaOpciones.count)")
        //print(String(data: request.httpBody!, encoding: .utf8) ?? "no existen datos")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if let response = response {
                //print("Response  evento: \(response)")
                let respuesta = response as! HTTPURLResponse
                //print("Respuesta: \(respuesta)")
                if respuesta.statusCode == OK_ENCUESTA_CREADA {
                    if data != nil {
                        //let body = String(data: data!, encoding: .utf8)
                        //print("Body reporte: \(body!)")
                        // Mensaje de éxito
                        self.mostrarMensajeInfo(mensaje: "Se ha creado con éxito la encuesta", cerrar: true)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error en los datos del usuario", cerrar: false)
                    }
                } else if respuesta.statusCode == ERROR_CREA_ENCUESTA {
                    if data != nil {
                        //let body = String(data: data!, encoding: .utf8)
                        //print("Body-> \(body!)")
                        self.mostrarMensajeInfo(mensaje: "Error al crear la encuesta", cerrar: false)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error en los datos del usuario", cerrar: false)
                    }
                } else if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                    self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                } else {
                    self.mostrarMensajeInfo(mensaje: "Error en el servidor...", cerrar: false)
                }
            } else {
                if let error = error {
                    //print("Error desconocido, \(error)")
                    let tipoError = error as NSError
                    if tipoError.code == ERROR_CONEXION_INTERNET {
                        self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error, al enviar tu información", cerrar: false)
                    }
                }
            }
            DispatchQueue.main.async {
                self.activEspera.stopAnimating()
            }
        }
        task.resume()
    }
    
    // Muestra un mensaje de información en la pantalla
    func mostrarMensajeInfo(mensaje: String, cerrar: Bool) {
        DispatchQueue.main.async {
            let alerta = UIAlertController(title: "Aviso", message: mensaje, preferredStyle: .alert)
            let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: { (alerta) in
                if mensaje == MENSAJE_CREDENCIALES_INVALIDAS {
                    self.navigationController?.popToRootViewController(animated: true)
                } else
                if cerrar {
                    self.navigationController?.popViewController(animated: true)
                }
            })
            alerta.addAction(aceptar)
            self.present(alerta, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func tapSobreVista(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
        if self.tfEditandoAhora != nil {
            self.tfEditandoAhora.resignFirstResponder()
            self.tfEditandoAhora = nil
        }
    }
    
    
    // MARK: - Maneja teclado
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.tfEditandoAhora = textField
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.tfEditandoAhora.resignFirstResponder()
        self.tfEditandoAhora = nil
        return true
    }
    
    @objc func tecladoAparece(notif: Notification) {
        let info = notif.userInfo!
        
        let tamTeclado = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
        let alturaTeclado = tamTeclado.height
        
        let contentInset = UIEdgeInsets(top: 0, left: 0, bottom: alturaTeclado, right: 0)
        self.scrollVista.contentInset = contentInset
        self.scrollVista.scrollIndicatorInsets = contentInset
        
        var frameVista = self.view.frame
        frameVista.size.height -= alturaTeclado
        
        // Mueve scroll para que se vea el campo
        if self.tfEditandoAhora == nil {
            return
        }
        if !frameVista.contains(self.tfEditandoAhora.frame.origin) {
            self.scrollVista.scrollRectToVisible(self.tfEditandoAhora.frame, animated: true)
        }
    }
    
    @objc func tecladoDesaparece(notif: Notification) {
        let contentInset = UIEdgeInsets.zero
        self.scrollVista.contentInset = contentInset;
        self.scrollVista.scrollIndicatorInsets = contentInset;
        self.tfEditandoAhora = nil
    }
}

