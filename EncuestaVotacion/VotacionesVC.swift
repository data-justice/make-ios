//
//  VotacionesVC.swift
//  MKE
//
//  Created by Roberto Mtz. Román on 9/3/19.
//  Copyright © 2019 Roberto Martinez Roman. All rights reserved.
//

import UIKit

class VotacionesVC: UIViewController
{
    var arrEncuestas = NSMutableArray()
    
    var idIsla: Int!
    var dIsla: [String:AnyObject]!
    
    var alias: String = ""
    
    @IBOutlet weak var activEspera: UIActivityIndicatorView!
    @IBOutlet weak var tablaEncuestas: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //alias = UserDefaults.standard.string(forKey: LLAVE_USUARIO)!
        alias = KeychainWrapper.standard.string(forKey: LLAVE_USUARIO)!
        //print(dIsla!)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        // Después de cargar datos de la Isla
        //descargarEncuestas()
        descargarIsla()
    }
    
    func descargarIsla() {
        activEspera.startAnimating()
        // Token para authorization
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        // Enviar información al servidor,
        let direccion = URL_LEER_ISLA.replacingOccurrences(of: ":id", with: "\(self.idIsla!)")
        let url = URL(string: direccion)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-type")
        request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.activEspera.stopAnimating()
            }
            if error == nil {   // No hay error
                if let response = response {
                    //print(response)
                    let respuesta = response as! HTTPURLResponse
                    if respuesta.statusCode == OK_LEER_ISLA {
                        if let body = String(data: data!, encoding: .utf8) {
                            //print("Body LEER ISLA: \(body)")
                            //self.extraerEncuestas(body)
                            self.dIsla = try? (JSONSerialization.jsonObject(with: body.data(using: .utf8)!, options: .mutableContainers) as! [String:AnyObject])
                            DispatchQueue.main.async {
                                self.descargarEncuestas()
                            }
                        } else {
                            self.mostrarMensajeInfo(mensaje: "Error al solicitar encuestas al servidor", cerrar: false)
                        }
                        
                    } else if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                        self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error al solicitar encuestas al servidor", cerrar: false)
                    }
                }
            } else {
                if let error = error {
                    //print("Error desconocido, \(error)")
                    let tipoError = error as NSError
                    if tipoError.code == ERROR_CONEXION_INTERNET {
                        self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error en el servidor", cerrar: false)
                    }
                }
            }
        }
        task.resume()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueCreaEncuesta" {
            let vc = segue.destination as! CreaEncuestaVC
            vc.idIsla = self.idIsla
        }
    }
    
    func descargarEncuestas() {
        activEspera.startAnimating()
        // Token para authorization
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        // Enviar información al servidor,
        let direccion = URL_LISTA_ENCUESTAS.replacingOccurrences(of: ":id", with: "\(self.idIsla!)")
        let url = URL(string: direccion)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-type")
        request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.activEspera.stopAnimating()
            }
            if error == nil {   // No hay error
                if let response = response {
                    //print(response)
                    let respuesta = response as! HTTPURLResponse
                    if respuesta.statusCode == OK_LISTA_ENCUESTAS {
                        if let body = String(data: data!, encoding: .utf8) {
                            //print("Body LISTA encuestas: \(body)")
                            self.extraerEncuestas(body)
                        } else {
                            self.mostrarMensajeInfo(mensaje: "Error al solicitar encuestas al servidor", cerrar: false)
                        }
                        
                    } else if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                        self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error al solicitar encuestas al servidor", cerrar: false)
                    }
                }
            } else {
                if let error = error {
                    //print("Error desconocido, \(error)")
                    let tipoError = error as NSError
                    if tipoError.code == ERROR_CONEXION_INTERNET {
                        self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error en el servidor", cerrar: false)
                    }
                }
            }
        }
        task.resume()
    }
    
    func extraerEncuestas(_ body: String) {
        
        if let arrEncuestas = try? (JSONSerialization.jsonObject(with: body.data(using: .utf8)!, options: .mutableContainers) as! NSMutableArray)  {
            self.arrEncuestas.removeAllObjects()
            for encuesta in arrEncuestas  {
                self.arrEncuestas.add(encuesta)
            }
            
            DispatchQueue.main.async {
                self.tablaEncuestas.reloadData()
            }
        }
    }
    
    func enviarRespuesta(idTopic: Int, idRespuesta: Int, textoRespuesta: String) {
        activEspera.startAnimating()
        // Token para authorization
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        // Enviar información al servidor, recibe token
        let url = URL(string: URL_ENVIA_RESPUESTA)
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-type")
        request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        request.httpBody = """
            {
            "topic": \(idTopic),
            "choice": "\(idRespuesta)"
            }
            """.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.activEspera.stopAnimating()
            }
            if error == nil {   // No hay error
                if let response = response {
                    //print(response)
                    let respuesta = response as! HTTPURLResponse
                    if respuesta.statusCode == OK_ENVIA_RESPUESTA {
                        if let _ = String(data: data!, encoding: .utf8) {
                            //print("Body RESPUESTA encuesta: \(body)")
                            self.mostrarMensajeInfo(mensaje: "Se registró tu respuesta '\(textoRespuesta)' correctamente", cerrar: false)
                        }
                        DispatchQueue.main.async {
                            self.descargarEncuestas()
                        }
                    } else if respuesta.statusCode == ERROR_ENVIA_RESPUESTA {
                        self.mostrarMensajeInfo(mensaje: "Ya respondiste la encuesta", cerrar: false)
                    } else if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                        self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error en el servicio", cerrar: false)
                    }
                }
            } else {
                if let error = error {
                    //print("Error desconocido, \(error)")
                    let tipoError = error as NSError
                    if tipoError.code == ERROR_CONEXION_INTERNET {
                        self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error, al registrar respuesta", cerrar: false)
                    }
                }
            }
        }
        task.resume()
    }
    
    // Muestra un mensaje de información en la pantalla
    func mostrarMensajeInfo(mensaje: String, cerrar: Bool) {
        DispatchQueue.main.async {
            let alerta = UIAlertController(title: "Aviso", message: mensaje, preferredStyle: .alert)
            let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: { (alerta) in
                if mensaje == MENSAJE_CREDENCIALES_INVALIDAS {
                    self.navigationController?.popToRootViewController(animated: true)
                } else
                if cerrar {
                    self.navigationController?.popViewController(animated: true)
                }
            })
            alerta.addAction(aceptar)
            self.present(alerta, animated: true, completion: nil)
        }
    }
}


extension VotacionesVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrEncuestas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celda = tableView.dequeueReusableCell(withIdentifier: "celdaEncuesta", for: indexPath)
        
        let dEncuesta = self.arrEncuestas.object(at: indexPath.row) as! NSDictionary
        celda.textLabel?.text = dEncuesta.value(forKey: "title") as?  String
        
        let contestada = self.estaContestada(encuesta: dEncuesta)
        if contestada {
            celda.accessoryType = .checkmark
        } else {
            celda.accessoryType = .none
        }
        return celda
    }
    
    func estaContestada(encuesta: NSDictionary) -> Bool {
        let arrVotos = encuesta.value(forKey: "votes") as! NSArray
        for voto in arrVotos {
            let dVoto = voto as! NSDictionary
            let user = dVoto.value(forKey: "user") as! String
            if user == self.alias {
                // Lo encontró
                return true
            }
        }
        return false
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //mostrar encuesta y  responder. O MOSTRAR RESULTADOS DE LA ENCUESTA
        let celda = tableView.cellForRow(at: indexPath)!
        if celda.accessoryType == .none {
            // VA A CONTESTAR la encuesta
            let dEncuesta = self.arrEncuestas.object(at: indexPath.row) as! NSDictionary
            let titulo = dEncuesta.value(forKey: "title") as!  String
            let descripcion =  dEncuesta.value(forKey: "description") as!  String
            let texto = descripcion.removingPercentEncoding
            let alerta = UIAlertController(title: titulo, message: texto, preferredStyle: .actionSheet)
            
            // Opciones
            let idTopic = dEncuesta.value(forKey: "id") as! Int
            let arrOpciones = dEncuesta.value(forKey: "choices") as! NSArray
            for dOpcion in arrOpciones {
                let opcion = dOpcion as! NSDictionary
                let id = opcion.value(forKey: "id") as! Int
                //print(dOpcion)
                let textoOpcion = opcion.value(forKey: "title") as! String
                let opcionAction = UIAlertAction(title: textoOpcion, style: .default) { (accion) in
                    // Enviar respuesta
                    self.enviarRespuesta(idTopic: idTopic, idRespuesta: id, textoRespuesta: textoOpcion)
                }
                alerta.addAction(opcionAction)
            }
            
            let cancelar = UIAlertAction(title: "Cancelar", style: .cancel)
            alerta.addAction(cancelar)
            
            if ( UIDevice.current.userInterfaceIdiom  == UIUserInterfaceIdiom.pad )
            {
                if let ppOver = alerta.popoverPresentationController {
                    ppOver.sourceView = self.tablaEncuestas
                    ppOver.sourceRect = self.tablaEncuestas.frame
                    self.present(alerta, animated: true)
                }
            } else {
                present(alerta, animated: true)
            }
            
            //self.present(alerta, animated: true)
        } else {
            // MOSTRAR RESULTADOS
            let dEncuesta = self.arrEncuestas.object(at: indexPath.row) as! NSDictionary
            self.mostrarResultadoEncuesta(dEncuesta)
        }
    }
    
    func mostrarResultadoEncuesta(_ encuesta: NSDictionary) {
        
        //print("\(dIsla!)")
        let modeloGobernanza = dIsla["governance_model"] as! String
        var usuarioCentral = ""
        if modeloGobernanza == "centralized" {
            usuarioCentral = dIsla["centralized_user"] as! String
        }
        var pesoUsuarios = [String:Int]()
        if modeloGobernanza == "weighted" {
            pesoUsuarios = dIsla["user_weights"] as! [String:Int]
        }
        
        let descripcion = encuesta.value(forKey: "description") as! String
        let dOpciones = encuesta.value(forKey: "choices") as! NSArray
        //print("encuesta: -> \(encuesta)")
        
        // Crear diccionario [id:titulo]
        var dIdTitulo = Dictionary<Int,String>()
        var dVotos = Dictionary<Int,Int>()
        for opcion in dOpciones {
            let dOpcion = opcion as! NSDictionary
            let idTitulo = dOpcion.value(forKey: "id") as! Int
            dVotos[idTitulo] = 0
            // Diccionario id:titulo
            dIdTitulo[idTitulo] = dOpcion.value(forKey: "title") as? String ?? "-"
        }
        //print("dTitulos: \(dIdTitulo)")
        let arrVotos = encuesta.value(forKey: "votes") as! NSArray
        for voto in arrVotos {
            let dVoto = voto as! NSDictionary
            let respuesta = dVoto.value(forKey: "vote_choice") as! NSDictionary
            let idVoto = respuesta.value(forKey: "id") as! Int
            let aliasVoto = dVoto.value(forKey: "user") as! String
            
            if modeloGobernanza == "democratic" {
                dVotos[idVoto] = dVotos[idVoto]! + 1
            } else if modeloGobernanza == "weighted" {
                dVotos[idVoto] = dVotos[idVoto]! + pesoUsuarios[aliasVoto]!
            } else if modeloGobernanza == "centralized" {
                if aliasVoto == usuarioCentral {
                    dVotos[idVoto] = 1
                }
            }
        }
        
        let mensaje = NSMutableAttributedString()
        mensaje.normal("Ya contestaste la encuesta, éstas son las votaciones hasta el momento.\n\n")
        mensaje.bold("\(descripcion.removingPercentEncoding!)\n\n")
        
        for id in dVotos.keys.sorted() {
            let totalVotos = dVotos[id]
            mensaje.bold("\(dIdTitulo[id]!) - \(totalVotos!) votos\n")
        }
        
        let alerta = UIAlertController(title: "Resultado de la encuesta", message: mensaje.string, preferredStyle: .alert)
        alerta.setValue(mensaje, forKey: "attributedMessage")
        let aceptar = UIAlertAction(title: "Aceptar", style: .default)
        alerta.addAction(aceptar)
        self.present(alerta, animated: true)
    }
}

extension NSMutableAttributedString
{
    @discardableResult func bold(_ texto: String) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key: Any] = [.font: UIFont(name: "Futura", size: 20)!]
        let boldString = NSMutableAttributedString(string:texto, attributes: attrs)
        append(boldString)
        return self
    }
    
    @discardableResult func normal(_ texto: String) -> NSMutableAttributedString {
        let normal = NSAttributedString(string: texto)
        append(normal)
        return self
    }
}
