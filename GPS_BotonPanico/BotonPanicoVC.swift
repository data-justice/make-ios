//
//  BotonPanicoVC.swift
//  MKE
//
//  Created by Roberto Martínez Román on 6/13/19.
//  Copyright © 2019 Roberto Martinez Roman. All rights reserved.
//
// Botón de emergencia

import UIKit
import CoreLocation
import ContactsUI

import MessageUI.MFMessageComposeViewController

class BotonPanicoVC: UIViewController, GPS_Delegado
{
    // Datos de posición/dirección. Para llenar automáticamente el registro
    var diccionarioInfo = Dictionary<String, Any>()
    var posicionActual: CLLocation!
    
    let gps = GPS()
    
    @IBOutlet weak var pickerTipoEvento: UIPickerView!
    @IBOutlet weak var lblRed: UILabel!
    @IBOutlet weak var lblDireccion: UILabel!
    @IBOutlet weak var lblGps: UILabel!
    @IBOutlet weak var tablaContactos: UITableView!
    
    @IBOutlet weak var lblContactos: UILabel!
    
    // Envía trazas a los contactos
    var timer: Timer!
    var numeroTraza = 0
    var tipoEvento = ""
    // Para saber si puede detener el GPS al salir de la pantalla
    var enviandoSMS = false
    
    var idPanico = 0   // Para enviar los 7 complementos posteriores
    
    @IBOutlet weak var tvConsola: UITextView!
    @IBOutlet weak var vistaConsola: UIView!
    @IBOutlet weak var btnCancelarTrazas: UIButton!
    @IBOutlet weak var activEspera: UIActivityIndicatorView!
    
    // Video
    @IBOutlet weak var vistaVideo: UIView!
    var estaGrabando = false
    var grabadora: GrabadorVideo = GrabadorVideo()
    var timerVideo: Timer!
    var urlVideo: URL!
    var idVideoAnexo: Int!
    
    var quiereVideo = true
    @IBOutlet weak var btnQuiereVideo: UIButton!
    @IBOutlet weak var btnDetenerVideo: UIButton!
    
    // Contactos del usuario <Contacto>
    var arrContactos = [Contacto]()
    
    //MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.hidesBackButton = false
        
        gps.iniciarGPS()
        gps.delegado = self
        
        configurarEstado()
        cargarContactos()
        
        self.title = "EMERGENCIA"
    }
    
    // Saliendo de la pantalla de Botón de pánico
    override func viewWillDisappear(_ animated: Bool) {
        if !enviandoSMS {
            gps.detenerGPS()
            gps.delegado = nil
            apagarEnvioTrazas()
        }
    }

    // Obtiene los contactos previamente guardados
    func cargarContactos() {
        let prefs = UserDefaults.standard
        let decoded  = prefs.object(forKey: "contactos") as? Data
        if decoded != nil {
            if let arreglo = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded!) as? [Contacto] {
                self.arrContactos = arreglo
            }
        }
    }
    
    func configurarEstado() {
        ponerImagen(etiqueta: lblRed, texto: "Red\n", prendido: false)
        ponerImagen(etiqueta: lblGps, texto: "GPS\n", prendido: false)
        ponerImagen(etiqueta: lblDireccion, texto: "Dirección\n", prendido: false)
    }
    
    func ponerImagen(etiqueta: UILabel, texto: String, prendido: Bool) {
        let imgTexto = NSTextAttachment()
        var img = UIImage(named: "tache.png")
        if prendido {
            img = UIImage(named: "check.png")
        }
        imgTexto.image = img
        let attrTitulo = NSAttributedString(attachment: imgTexto)
        let titulo = NSMutableAttributedString(string: texto)
        titulo.append(attrTitulo)
        
        etiqueta.attributedText = titulo
    }
    
    // Detiene el video, solo manda lo que tenga hasta el momneto
    @IBAction func detenerVideoBtn(_ sender: UIButton) {
        DispatchQueue.main.async {
            self.grabadora.detenerGrabacion()
        }
    }
    
    
    // Inicia el envío del evento de pánico, RESPONDE al botón ENVIAR
    @IBAction func enviarEventoPanico(_ sender: UIButton) {
        self.idPanico = 0
        // BLOQUEAR REGRESO y PICKERVIEW
        self.navigationItem.hidesBackButton = true
        self.pickerTipoEvento.isUserInteractionEnabled = false
        // Muestra la vista de video, SOLO SI EL USUARIO LO ACTIVÓ
        if quiereVideo {
            self.vistaVideo.isHidden = false
        }
        // Muestra la consola de mensajes
        self.vistaConsola.isHidden = false
        self.activEspera.startAnimating()
        self.tvConsola.text = ""
        self.agregarMensajeConsola("\nGenerando señal de pánico")
        // Detiene el 'apagado' de la pantalla
        UIApplication.shared.isIdleTimerDisabled = true
        // Envíar registro padre
        self.enviarReportePanico()  // Al servidor, después -> complementos
        //print("Envió reporte pánico")
        // En paralelo
        // A los contactos, al regresar inicia el video que será el primer anexo del complemento
        self.enviarTrazaIndividualSMS()
        //print("Envió TrazaIndividualSMS")
        // Enviar 7 trazas a los contactos
        //self.enviarTrazasContactos()    // A los contactos (necesita el id del evento principal)
        // Esperar hasta que haya mandado el evento principal
    }
    
    // Van datos al servidor (Evento principal)
    func enviarReportePanico() {
        // Leer campos
        let indice = self.pickerTipoEvento.selectedRow(inComponent: 0)
        let categoria = arrTipoEvento[indice]
        let fecha = self.obtenerFechaActual()
        let hora = self.obtenerHoraActual()
        var latitud = "0"
        var longitud = "0"
        if posicionActual != nil {
            latitud = "\(posicionActual.coordinate.latitude)"
            longitud = "\(posicionActual.coordinate.longitude)"
        }
        // Datos opcionales
        var direccion = "Desconocida"
        var pais = "México"
        var estado = "desconocido"
        var municipio = "desconocido"
        var cp = "0"
        if diccionarioInfo.count > 0 {
            // Si hay datos de dirección
            direccion = diccionarioInfo["direccion"] as? String ?? "desconocida"
            pais = diccionarioInfo["pais"] as? String ?? "desconocido"
            estado = diccionarioInfo["estado"] as? String ?? "desconocido"
            municipio = diccionarioInfo["municipio"] as? String ?? "desconocido"
            cp = diccionarioInfo["cp"] as? String ?? "0"
        }
        
        var group_id = ""
        if let idIsla = UserDefaults.standard.string(forKey: LLAVE_ID_ISLA) {
            group_id = """
            ,
            "group_id": \(idIsla)
            """
        }
        
        // Token para authorization
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        // Enviar información al servidor
        let url = URL(string: URL_BOTON_PANICO)
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-type")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.httpBody = """
            {
            "category": "\(categoria)",
            "date": "\(fecha)",
            "time": "\(hora)",
            "location_latitude": \(latitud),
            "location_longitude": \(longitud),
            "location_address": "\(direccion)",
            "location_country": "\(pais)",
            "location_state": "\(estado)",
            "location_city": "\(municipio)",
            "location_zip_code": "\(cp)"
            \(group_id)
            }
            """.data(using: .utf8)

        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil {   // No hay error
                if let response = response {
                    let respuesta = response as! HTTPURLResponse
                    if respuesta.statusCode == OK_REGISTRO_PANICO_CREADO {
                        if data != nil {
                            let body = String(data: data!, encoding: .utf8)!
                            // Guardar el id para los complementos
                            let dInfo = try? JSONSerialization.jsonObject(with: body.data(using: .utf8)!, options: .mutableContainers) as? NSDictionary
                            if let dInfo = dInfo {
                                self.idPanico = dInfo["id"] as? Int ?? 0
                            }
                            // Mensaje de éxito
                            self.agregarMensajeConsola("\nSe ha enviado con éxito el evento de pánico al servidor\n")
                        } else {
                            self.agregarMensajeConsola("\n\nError al enviar el evento de pánico")
                            self.idPanico = 0
                        }
                    } else if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                        self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                    }
                }
            } else {
                if let error = error {
                    let tipoError = error as NSError
                    if tipoError.code == ERROR_CONEXION_INTERNET {
                        self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error, al enviar tu información", cerrar: false)
                    }
                    self.idPanico = 0
                }
            }
        }
        task.resume()
    }
    
    // Envíos posteriores al evento de pánico principal (idPanico)
    func enviarComplementoPanico() {
        // Leer campos
        let fecha = self.obtenerFechaActual()
        let hora = self.obtenerHoraActual()
        var latitud = "0"
        var longitud = "0"
        if posicionActual != nil {
            latitud = "\(posicionActual.coordinate.latitude)"
            longitud = "\(posicionActual.coordinate.longitude)"
        }
        // Datos opcionales
        var descripcion = "Complemento, botón de pánico: \(idPanico)"
        descripcion.append("\n\nPosición actualizada:\n\(self.obtenerURL())")
        descripcion = descripcion.addingPercentEncoding(withAllowedCharacters: .alphanumerics)!
        let tituloCorto = "Complemento, botón de pánico"
        var direccion = "Desconocida"

        if diccionarioInfo.count > 0 {
            // Si hay datos de dirección
            direccion = diccionarioInfo["direccion"] as! String
        }
        
        var listaAnexos: [Int] = []
        if self.idVideoAnexo != nil {
            listaAnexos.append(self.idVideoAnexo)
            self.idVideoAnexo = nil
        }
        
        // Token para authorization
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        // Enviar información al servidor, recibe token
        // 400 credenciales incorrectas, 201 ok
        let url = URL(string: URL_SERVICIO_INCIDENTE+"\(idPanico)"+URL_SERVICIO_INCIDENTE_COMPLEMENTO)
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-type")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")

        request.httpBody = """
            {
            "incident": \(idPanico),
            "title": "\(tituloCorto)",
            "description": "\(descripcion)",
            "date": "\(fecha)",
            "time": "\(hora)",
            "attachments": \(listaAnexos),
            "location_latitude": \(latitud),
            "location_longitude": \(longitud),
            "location_address": "\(direccion)"
            }
            """.data(using: .utf8)
        
        //print(String(data: request.httpBody!, encoding: .utf8) ?? "no existen datos")
        //print(request.allHTTPHeaderFields!)
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if error == nil {
                let respuesta = response! as! HTTPURLResponse
                //print("Respuesta complemento PANICO: \(respuesta)")
                if respuesta.statusCode == OK_REGISTRO_CREADO {
                    self.agregarMensajeConsola("Se ha creado con éxito el complemento")
                } else if respuesta.statusCode == ERROR_COMPLEMENTO_LLAVE {
                    self.agregarMensajeConsola("Clave primaria inválida, objeto no existe")
                } else if respuesta.statusCode == ERROR_SERVIDOR {
                    self.agregarMensajeConsola("Error en el servidor, intente más tarde")
                } else if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                    self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                } else {
                    self.agregarMensajeConsola("Error en el servidor...")
                }
            } else {
                if let error = error {
                    //print("Error desconocido, \(error)")
                    let tipoError = error as NSError
                    if tipoError.code == ERROR_CONEXION_INTERNET {
                        self.agregarMensajeConsola("Error, revisa tu conexión a Internet e intenta nuevamente")
                    } else {
                        self.agregarMensajeConsola("Error, al enviar tu información")
                    }
                }
            }
        }
        task.resume()
    }
    
    // Envía el video al servidor
    func enviarAnexo(urlArchivoVideo: URL) {

        // Token del usuario
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        var urlRequest = URLRequest(url: URL(string: URL_SERVICIO_ATTACHMENT)!)
        urlRequest.httpMethod = "POST"
        let datosBin = try? Data(contentsOf: urlArchivoVideo)
        
        let filename = "video.mp4"
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // String para separar datoss
        let boundary = UUID().uuidString
        
        urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        var data = Data()
        
        // Add the image data to the raw http request data
        data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
        data.append("Content-Disposition: form-data; name=\"file\"; filename=\"\(filename)\"\r\n".data(using: .utf8)!)
        data.append("Content-Type: video/mp4\r\n\r\n".data(using: .utf8)!)
        data.append(datosBin!)
        
        data.append("\r\n--\(boundary)--\r\n".data(using: .utf8)!)
        
        session.uploadTask(with: urlRequest, from: data, completionHandler: { data, response, error in
            
            if let response = response {
                //print("Response: \(response)")
                let respuesta = response as! HTTPURLResponse
                if respuesta.statusCode == OK_ATTACHMENT_CREADO {
                    if let datosBin = data, let _ = String(data: datosBin, encoding: .utf8) {
                        let resultado = try! JSONSerialization.jsonObject(with: datosBin, options: []) as? NSDictionary ?? [:]
                        let idAnexo = resultado["id"]! as! Int
                        self.idVideoAnexo = idAnexo
                        // Programar envio de complementos (el primero lleva la liga)
                        DispatchQueue.main.async {
                            self.enviarTrazasContactos()
                        }
                    }
                } else if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                    self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                } else {
                    self.mostrarMensajeInfo(mensaje: "Error al enviar el anexo", cerrar: false)
                }
            } else {
                if let error = error {
                    //print("Error desconocido, \(error)")
                    let tipoError = error as NSError
                    if tipoError.code == ERROR_CONEXION_INTERNET {
                        self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error, al enviar el anexo", cerrar: false)
                        DispatchQueue.main.async {
                            self.enviarTrazasContactos()
                        }
                    }
                }
            }
        }).resume()
    }
    
    // 1 traza cada minuto (7 en total)
    func enviarTrazasContactos() {
        numeroTraza = 0;
        if self.idPanico != 0 {
            numeroTraza += 1
            self.agregarMensajeConsola("Enviando traza \(self.numeroTraza)/\(NUMERO_TRAZAS) con ubicación actualizada")
            // Mandar inicial, agregar anexo
            self.enviarComplementoPanico()  // Complemento de pánico
        }
        
        // Programar envío de complemento cada minuto
        self.timer = Timer.scheduledTimer(withTimeInterval: TIEMPO_TRAZA, repeats: true, block: { (timer) in
            self.numeroTraza += 1
            //print("Manda traza \(self.numeroTraza)/\(NUMERO_TRAZAS) con ubicación actualizada")
            self.agregarMensajeConsola("Enviando traza \(self.numeroTraza)/\(NUMERO_TRAZAS) con ubicación actualizada")
            //self.enviarTrazaIndividual()  // Ya no envía SMS
            self.enviarComplementoPanico()  // Complemento de pánico
            if self.numeroTraza>=NUMERO_TRAZAS {
                self.apagarEnvioTrazas()
                self.agregarMensajeConsola("\n\nTermina envío de trazas")
                DispatchQueue.main.async {
                    self.btnCancelarTrazas.setTitle("Salir", for: .normal)
                    self.activEspera.stopAnimating()
                    UIApplication.shared.isIdleTimerDisabled = false
                }
            }
        })
    }
    
    // El usuario debe confirmar el envío
    func enviarTrazaIndividualSMS() {
        // Recupera nombre de usuario
        //let prefs = UserDefaults.standard
        //let usuario = prefs.string(forKey: LLAVE_USUARIO)!
        let usuario = KeychainWrapper.standard.string(forKey: LLAVE_USUARIO) ?? "alias"

        let mensaje = "\nEnviando traza inicial"
        self.agregarMensajeConsola(mensaje)
        // Obtener contactos
        let contactos = obtenerContactos()
        let url = obtenerURL()
        let direccion = diccionarioInfo["direccion"] as? String ?? "No disponible"
        let mensajeSMS = "Emergencia de: \(usuario)\nTipo: \(self.tipoEvento)\nUbicación: \(url)\nDirección: \(direccion)"
        
        if MFMessageComposeViewController.canSendText(){
            if self.arrContactos.count > 0 {
                let controlador = MFMessageComposeViewController()
                controlador.body = mensajeSMS
                controlador.recipients = contactos
                controlador.messageComposeDelegate = self
                self.enviandoSMS = true
                self.present(controlador, animated: true, completion: nil)
            } else {
                self.agregarMensajeConsola("\nNo has seleccionado contactos, para enviarles SMS")
                self.programarGrabacionVideo()
            }
        } else {
            self.agregarMensajeConsola("\n*** Este dispositivo no puede enviar SMS")
            self.agregarMensajeConsola("Las trazas se siguen enviando al servidor ***\n")
            // No hay SMS, inicia el video
            self.programarGrabacionVideo()
        }
    }
    
    func agregarMensajeConsola(_ mensaje: String) {
        DispatchQueue.main.async {
            self.tvConsola.text.append(mensaje)
            self.tvConsola.text.append("\n")
            let longitud = self.tvConsola.text.count-1
            self.tvConsola.scrollRangeToVisible( NSMakeRange(longitud, 1))
        }
    }
    
    func  obtenerContactos()->[String] {
        var contactos: [String] = [String]()
        
        for i in  0..<arrContactos.count {
            let contacto = arrContactos[i]
            contactos.append(contacto.telefono)
        }
        
        return contactos
    }
    
    // Para desplegar el mapa
    func obtenerURL()->String {
        var latitud = 0.0
        var longitud = 0.0
        if let posicion = self.posicionActual {
            latitud = posicion.coordinate.latitude
            longitud = posicion.coordinate.longitude
        }
        let url = "https://www.google.com/maps/search/?api=1&query=\(latitud),\(longitud)"
        return url
    }
    
    
    @IBAction func cancelarEnvioTrazas(_ sender: Any) {
        self.apagarEnvioTrazas()
        
        self.numeroTraza = 0
        self.btnCancelarTrazas.setTitle("Cancelar", for: .normal)
        self.vistaConsola.isHidden = true
        self.activEspera.stopAnimating()
    }
    
    func apagarEnvioTrazas() {
        if timer != nil && self.timer.isValid {
            self.timer.invalidate()
            self.numeroTraza = 0
        }

        // Habilita back y pickerview
        self.navigationItem.hidesBackButton = false
        self.pickerTipoEvento.isUserInteractionEnabled = true
        // Oculta vista de video
        self.vistaVideo.isHidden = true
        
        //
        UIApplication.shared.isIdleTimerDisabled = false
    }
    
    // Agrega contactos
    @IBAction func agregarContacto(_ sender: UIButton) {
        if self.arrContactos.count < MAX_CONTACTOS {
            let vc = CNContactPickerViewController()
            vc.delegate = self
            self.present(vc, animated: true)
        } else {
            // Ya no debe agregar más
            mostrarMensajeInfo(mensaje: "Solo puedes agregar \(MAX_CONTACTOS) contactos", cerrar: false)
        }
    }
    
    func obtenerFechaActual() -> String {
        let fecha = Date()
        let formato = DateFormatter()
        formato.dateFormat = "yyyy-MM-dd"
        return formato.string(from: fecha)
    }
    
    func obtenerHoraActual() -> String {
        let fecha = Date()
        let calendario = Calendar.current
        let hora = calendario.component(.hour, from: fecha)
        let minuto = calendario.component(.minute, from: fecha)
        return "\(hora):\(minuto)"
    }
    
    // Muestra un mensaje de información en la pantalla
    func mostrarMensajeInfo(mensaje: String, cerrar: Bool) {
        DispatchQueue.main.async {
            let alerta = UIAlertController(title: "Aviso", message: mensaje, preferredStyle: .alert)
            let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: { (alerta) in
                if mensaje == MENSAJE_CREDENCIALES_INVALIDAS {
                    self.navigationController?.popToRootViewController(animated: true)
                } else
                if cerrar {
                    self.navigationController?.popViewController(animated: true)
                }
            })
            alerta.addAction(aceptar)
            self.present(alerta, animated: true, completion: nil)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if CheckInternet.Connection() {
            self.ponerImagen(etiqueta: self.lblRed, texto: "Red\n", prendido: true)
        }
    }
    
    
    @IBAction func cambiarQuiereVideo(_ sender: Any) {
        quiereVideo = !quiereVideo
        let imgBtn = UIImage(named: (quiereVideo ? "videoIconoOn":"videoIconoOff"))!
        
        UIView.transition(with: self.view, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.btnQuiereVideo.setImage(imgBtn, for: .normal)
        })
    }
    
    // Programar grabación de video
    func programarGrabacionVideo() {
        if quiereVideo {
            // Prende el video
            self.grabadora.delegado = self
            self.grabadora.vistaCamara = self.vistaVideo
            if self.grabadora.iniciarGrabador() {
                self.grabadora.iniciarGrabacion()
                self.vistaVideo.bringSubviewToFront(self.btnDetenerVideo)
                self.timerVideo = Timer.scheduledTimer(withTimeInterval: TIEMPO_VIDEO, repeats: false, block: { (timer) in
                    DispatchQueue.main.async {
                        self.grabadora.detenerGrabacion()
                    }
                })
            } else {
                self.vistaVideo.isHidden = true
                // No hay video, Inicia complementos/Sin videos
                self.enviarTrazasContactos()
            }
        } else {
            // Inicia complementos/Sin videos
            self.enviarTrazasContactos()
        }
    }
    
    // MARK: GPS
    func gps(nuevaPosicion: CLLocation) {
        self.posicionActual = nuevaPosicion
        DispatchQueue.main.async {
            self.ponerImagen(etiqueta: self.lblGps, texto: "GPS\n", prendido: true)
        }
    }
    
    func gps(nuevaDireccion: String) {
        
    }
    
    func gps(nuevosDatos: [String : Any]) {
        //print("Hay información completa:\n\(nuevosDatos)")
        self.diccionarioInfo = nuevosDatos
        DispatchQueue.main.async {
            self.ponerImagen(etiqueta: self.lblDireccion, texto: "Dirección\n", prendido: true)
        }
    }
    
    func mostrarMensaje(mensaje: String) {
        DispatchQueue.main.async {
            let alerta = UIAlertController(title: "Aviso", message: mensaje, preferredStyle: .alert)
            let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: nil)
            alerta.addAction(aceptar)
            self.present(alerta, animated: true, completion: nil)
        }
    }
}

extension BotonPanicoVC: UIPickerViewDataSource, UIPickerViewDelegate
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrTipoEvento.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrTipoEvento[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.tipoEvento = arrTipoEvento[row]
    }
}

extension BotonPanicoVC: UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.lblContactos.text = "Contactos (\(self.arrContactos.count)/\(MAX_CONTACTOS))"
        return arrContactos.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celda = tableView.dequeueReusableCell(withIdentifier: "celdaContacto", for: indexPath)

        let registro = self.arrContactos[indexPath.row]
        let nombre = registro.nombre // registro.givenName
        celda.textLabel?.text = nombre
        celda.detailTextLabel?.text = registro.telefono
        return celda;
    }
    
    // Borrar contactos
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Borrar"
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            //self.arrContactos.removeObject(at: indexPath.row)
            self.arrContactos.remove(at: indexPath.row)
            //
            let prefs = UserDefaults.standard
            if let encoded = try?  NSKeyedArchiver.archivedData(withRootObject: arrContactos, requiringSecureCoding: true) {
                prefs.set(encoded, forKey: "contactos")
                prefs.synchronize()
            }
            tablaContactos.reloadData()
        }
    }
}

extension BotonPanicoVC: CNContactPickerDelegate
{
    func actualizarArrContactos() {
        
        
        //arrContactos.add( contact)
        self.tablaContactos.reloadData()
        let prefs = UserDefaults.standard

        if let encoded = try? NSKeyedArchiver.archivedData(withRootObject: arrContactos, requiringSecureCoding: false) {
            prefs.set(encoded, forKey: "contactos")
        }
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        picker.dismiss(animated: true)
        if (contact.phoneNumbers.count > 1) {
            //Mostrar cada uno y dejar solo el seleccionado
            filtrarNumeros(contact)
        } else {
            // Extraer nombre y teléfono
            let nombre = contact.givenName
            let telefono = (contact.phoneNumbers.first?.value.stringValue)!
            arrContactos.append(Contacto(nombre: nombre, telefono: telefono))
            
            actualizarArrContactos()
        }
    }
    
    func filtrarNumeros(_ contact: CNContact) {
        // Despliega una lista de números y deja solo el seleccionado
        let nombre = contact.givenName
        let alerta = UIAlertController(title: nombre, message: "Selecciona el número que quieres utilizar para este contacto", preferredStyle: .actionSheet)
        // Lista de números
        let listaNumeros = contact.phoneNumbers
        for contacto in listaNumeros {
            let opcion = UIAlertAction(title: contacto.value.stringValue, style: .default) { (action) in
                // Borra los otros
                let telefono = contacto.value.stringValue
                self.arrContactos.append(Contacto(nombre: nombre, telefono: telefono))
                self.actualizarArrContactos()
            }
            alerta.addAction(opcion)
        }

        if ( UIDevice.current.userInterfaceIdiom  == UIUserInterfaceIdiom.pad )
        {
            if let ppOver = alerta.popoverPresentationController {
                ppOver.sourceView = self.lblContactos
                ppOver.sourceRect = self.lblContactos.frame
                self.present(alerta, animated: true)
            }
        } else {
            present(alerta, animated: true)
        }
        //self.present(alerta, animated: true)
    }
}

extension BotonPanicoVC: MFMessageComposeViewControllerDelegate
{
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        /*
        if result == .sent {
            //print("Termina de enviar mensaje")
        } else if result == .failed {
            //print("Error al enviar el mensaje")
        } else {
            //print("Mensaje cancelado")
        }*/
        
        controller.dismiss(animated: true, completion: nil)
        DispatchQueue.main.async {
            self.enviandoSMS = false
            // Por ahora, abligamos al usuario a cerrar la app de SMS
            self.programarGrabacionVideo()
        }
    }
}

extension BotonPanicoVC: DelegadoGrabadorVideo
{
    func terminaGrabacion(urlArchivo: URL) {
        //print("TENEMOS video: \(urlArchivo)")
        self.vistaVideo.isHidden = true
        // Enviar anexo, obtiene ligaAnexo -> primer complemento
        self.enviarAnexo(urlArchivoVideo: urlArchivo)
    }
}
