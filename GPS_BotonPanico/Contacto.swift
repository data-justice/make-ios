//
//  Contacto.swift
//  MKE
//
//  Created by Roberto Martínez Román on 6/18/19.
//  Copyright © 2019 Roberto Martinez Roman. All rights reserved.
//

import Foundation

class Contacto: NSObject, NSCoding
{
    var nombre: String
    var telefono: String
    
    func encode(with coder: NSCoder) {
        coder.encode(nombre, forKey: "nombre")
        coder.encode(telefono, forKey: "telefono")
    }
    
    required convenience init?(coder: NSCoder) {
        let nombreR = coder.decodeObject(forKey: "nombre") as! String
        let telefonoR = coder.decodeObject(forKey: "telefono") as! String
        self.init(nombre: nombreR, telefono: telefonoR)
    }
    
    init(nombre: String, telefono: String) {
        self.nombre = nombre
        self.telefono = telefono
    }
}
