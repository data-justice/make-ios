//
//  GPS.swift
//  MKE
//
//  Created by Roberto Martínez Román on 6/15/19.
//  Copyright © 2019 Roberto Martinez Roman. All rights reserved.
//

import UIKit
import CoreLocation

protocol GPS_Delegado
{
    // Coordenadas gps
    func gps(nuevaPosicion: CLLocation)
    // Dirección en texto
    func gps(nuevaDireccion: String)
    // Diccionario de información
    // "posicion":CLLocation, "pais":String, "estado":String, "municipio":String, "cp":String, "direccion":String
    func gps(nuevosDatos: [String:Any])
    func mostrarMensaje(mensaje: String)
}

class GPS: NSObject, CLLocationManagerDelegate
{
    let gps = CLLocationManager()
    var posicionActual: CLLocation!
    var diccionarioInfo = Dictionary<String,Any>()
    
    var delegado: GPS_Delegado!
    
    func iniciarGPS() {
        gps.delegate = self
        gps.requestWhenInUseAuthorization()
    }
    
    func detenerGPS() {
        gps.stopUpdatingLocation()
        gps.delegate = nil
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        switch status {
        case .authorizedWhenInUse, .authorizedAlways:
            // Autorizado
            gps.desiredAccuracy = kCLLocationAccuracyBest
            gps.startUpdatingLocation()
            //print("autorizado en uso")
        case .denied:
            // Mandar mensaje a través del delegado para autorizar
            delegado.mostrarMensaje(mensaje: "Debe activar el gps en Configuración-Privacidad-Localización")   
        default:
            return
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        // Nueva ubicación
        if let delegado = self.delegado, let posicion = locations.last {
            posicionActual = posicion
            delegado.gps(nuevaPosicion: posicionActual)
            // Recupera dirección en texto
            self.obtenerDireccionTexto()
        }
    }
    
    func obtenerDireccionTexto() {
        self.diccionarioInfo["posicion"] = self.posicionActual
        // pedir dirección de estas coordenadas
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(posicionActual) { (arrPlacemark, error) in
            if let arrPlacemark = arrPlacemark {
                if arrPlacemark.count == 0 {
                    //print("Dirección desconocida")
                    self.diccionarioInfo["pais"] = "Desconocido"
                    self.diccionarioInfo["cp"] = "0"
                    return
                }
                let diccionario = arrPlacemark[0]
                
                if let pais = diccionario.country {
                    self.diccionarioInfo["pais"] = pais
                }
                
                self.diccionarioInfo["estado"] = "desconocido"
                self.diccionarioInfo["municipio"] = "desconocido"
                
                // Dirección
                var direccionTexto = ""
                var nombre = ""
                var codigoPostal = ""
                if diccionario.name != nil {
                    nombre = diccionario.name!
                    direccionTexto.append(nombre.appending(", "))
                }
                
                if let calle = diccionario.thoroughfare {
                    if !calle.contains(nombre) && !nombre.contains(calle){
                        direccionTexto.append(calle.appending(", "))
                    }
                }
                if let colonia = diccionario.subLocality {
                    direccionTexto.append(colonia.appending(", "))
                }
                if let cp = diccionario.postalCode {
                    direccionTexto.append(cp.appending(", "))
                    self.diccionarioInfo["cp"] = cp
                    codigoPostal = cp
                }
                if let ciudad = diccionario.locality {
                    direccionTexto.append(ciudad.appending(", "))
                     self.diccionarioInfo["municipio"] = ciudad
                }
                if let estado = diccionario.administrativeArea {
                    direccionTexto.append(estado.appending(", "))
                     self.diccionarioInfo["estado"] = estado
                }

                if let pais = diccionario.country {
                    direccionTexto.append(pais)
                    //print("-> CP -> \(codigoPostal)")
                    self.diccionarioInfo["direccion"] = "\(direccionTexto)"
                    // Obtener estado y municipio con el código postal
                    if pais == "México" || pais == "Mexico" {
                        self.obtenerEstadoMunicipio(codigoPostal)
                    } else {
                        if self.delegado != nil {
                            self.delegado.gps(nuevosDatos: self.diccionarioInfo)
                        }
                    }
                }
                //print("*\(direccionTexto)*")
            } else if let _ = error {
                // Error
                //print("Error al obtener dirección \(error.localizedDescription)")
                self.delegado.mostrarMensaje(mensaje: "Error obteniendo la dirección en texto")
            }
        }
    }
    
    func obtenerEstadoMunicipio(_ codigoPostal: String) {
        let url = URL(string: URL_SERVICIO_CODIGO_POSTAL+codigoPostal)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-type")
        
        // Token para authorization
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil {
                self.diccionarioInfo["estado"] = "desconocido"
                self.diccionarioInfo["municipio"] = "desconocido"
                
                let respuesta = response as! HTTPURLResponse
                if respuesta.statusCode == OK_CONSULTA_CODIGO_POSTAL {
                    if let data = data, let body = String(data: data, encoding: .utf8) {
                        let arrCodigos = try? (JSONSerialization.jsonObject(with: body.data(using: .utf8)!, options: .mutableContainers) as! NSArray)
                        let diccionario = arrCodigos?[0] as! NSDictionary
                        
                        if let estado = diccionario["d_estado"] {
                            self.diccionarioInfo["estado"] = estado
                        }
                        if let municipio = diccionario["D_mnpio"] {
                            self.diccionarioInfo["municipio"] = municipio
                        }
                    }
                } else {
                    //print("*Error al consultar código postal \(respuesta.statusCode)")
                }
                if self.delegado != nil {
                    self.delegado.gps(nuevosDatos: self.diccionarioInfo)
                }
            } else {
                //print("Error al consultar código postal: \(error!.localizedDescription)")
                if self.delegado != nil {
                    self.delegado.gps(nuevosDatos: self.diccionarioInfo)
                }
            }
        }
        task.resume()
    }

}
