//
//  GrabadorVideo.swift
//  PruebaVideo
//
//  Created by Roberto Martínez Román on 6/29/19.
//  Copyright © 2019 Roberto Martinez Roman. All rights reserved.
//

import UIKit
import AVFoundation

protocol DelegadoGrabadorVideo {
    func terminaGrabacion(urlArchivo: URL)
}

class GrabadorVideo: NSObject, AVCaptureFileOutputRecordingDelegate
{
    var delegado: DelegadoGrabadorVideo!
    var vistaCamara: UIView!  // Para visualizar el video
    
    let sesionCaptura = AVCaptureSession()  // camara -> archivo
    let salidaVideo = AVCaptureMovieFileOutput()    // archivo
    var vistaPrevia: AVCaptureVideoPreviewLayer!   // Capa video capturando
    var entradaActiva: AVCaptureDeviceInput!  // Dispositivo entrada (camara)
    var urlArchivoCapturado: URL!
    
    func iniciarGrabador() -> Bool {
        if configurarSesion() {
            configurarVistaPrevia()
            iniciarSesion()
            return true
        } else {
            //print("No hay cámara")
            // Qué hacemos???????
            return false
        }
    }
    
    // Agrega la capa de preview a la vista de la cámara
    func configurarVistaPrevia() {
        vistaPrevia = AVCaptureVideoPreviewLayer(session: sesionCaptura)
        vistaPrevia.frame = vistaCamara.bounds
        vistaPrevia.videoGravity = AVLayerVideoGravity.resizeAspectFill
        vistaCamara.layer.addSublayer(vistaPrevia)
    }
    
    //MARK:- Inicializa la cámara
    func configurarSesion() -> Bool {
        
        if sesionCaptura.canSetSessionPreset(.hd1280x720) {
            sesionCaptura.sessionPreset = .hd1280x720
        }
        
        // Configura cámara ** VERIFICAR QUE SE PUEDE USAR LA CÁMARA
        if let camara = AVCaptureDevice.default(for: .video) {
            
            do {
                let entrada = try AVCaptureDeviceInput(device: camara)
                if sesionCaptura.canAddInput(entrada) {
                    sesionCaptura.addInput(entrada)
                    entradaActiva = entrada
                }
            } catch {
                //print("No puede crear cámara: \(error)")
                return false
            }
            
            // Micrófono
            let microfono = AVCaptureDevice.default(for: .audio)!
            
            do {
                let microEntrada = try AVCaptureDeviceInput(device: microfono)
                if sesionCaptura.canAddInput(microEntrada) {
                    sesionCaptura.addInput(microEntrada)
                }
            } catch {
                //print("No puede crear micrófono: \(error)")
                return false
            }
            
            // Salida, dónde deja el resultado
            if sesionCaptura.canAddOutput(salidaVideo) {
                sesionCaptura.addOutput(salidaVideo)
            }
            
            // Configuración completa
            return true
        } else {
            // No hay cámara
            return false
        }
    }
    
    //MARK:- Sesión para grabar
    func iniciarSesion() {
        //print("111-self.sesionCaptura.startRunning()")
        if !sesionCaptura.isRunning {
            self.sesionCaptura.startRunning()
            //print("self.sesionCaptura.startRunning()")
        }
    }
    
    func detenerSesion() {
        if sesionCaptura.isRunning {
            DispatchQueue.main.async {
                self.sesionCaptura.stopRunning()
            }
        }
    }

    func obtenerOrientacion() -> AVCaptureVideoOrientation {
        var orientacion: AVCaptureVideoOrientation
        
        switch UIDevice.current.orientation {
        case .portrait:
            orientacion = AVCaptureVideoOrientation.portrait
        case .landscapeRight:
            orientacion = AVCaptureVideoOrientation.landscapeLeft
        case .portraitUpsideDown:
            orientacion = AVCaptureVideoOrientation.portraitUpsideDown
        default:
            orientacion = AVCaptureVideoOrientation.landscapeRight
        }
        
        return orientacion
    }
    
    // Url del archivo de salida
    func obtenerURLsalida() -> URL? {
        let directorio = NSTemporaryDirectory() as NSString
        if directorio != "" {
            let path = directorio.appendingPathComponent(NSUUID().uuidString + ".mp4")
            return URL(fileURLWithPath: path)
        }
        return nil
    }
    
    // Inicia la grabación, los datos los pone en el archivo de salida
    func iniciarGrabacion() {
        //print("Iniciando grabación")
        if !salidaVideo.isRecording {
            let conexion = salidaVideo.connection(with: .video)
            if (conexion?.isVideoOrientationSupported)! {
                conexion?.videoOrientation = obtenerOrientacion()
            }

            let dispositivo = entradaActiva.device
            if (dispositivo.isSmoothAutoFocusSupported) {
                do {
                    try dispositivo.lockForConfiguration()
                    dispositivo.isSmoothAutoFocusEnabled = false
                    dispositivo.unlockForConfiguration()
                } catch {
                    //print("Error al cambiar autoFocus: \(error)")
                }
            }
            
            urlArchivoCapturado = obtenerURLsalida()
            //print("Recording...")
            salidaVideo.startRecording(to: urlArchivoCapturado, recordingDelegate: self)
        } else {
            //print("Entróa l else")
        }
    }
    
    func detenerGrabacion() {
        //print("detener grabación")
        if salidaVideo.isRecording == true {
            salidaVideo.stopRecording()
            //print("Deteniendo -<")
        }
    }
    
    func fileOutput(_ output: AVCaptureFileOutput, didStartRecordingTo fileURL: URL, from connections: [AVCaptureConnection]) {
        //print("Inicia la grabación ---<<")
    }
    
    // Termina la grabación
    func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
        if (error != nil) {
            //print("Error al grabar el video: \(error!.localizedDescription)")
        } else {
            let videoUrl = outputFileURL
            //print("*** video url: ", videoUrl)
            //let data = try? Data(contentsOf: videoUrl)
            //print("Tamaño del video: \(Double(data!.count) / 1048576.0) MB")
            // NOTIFICAR QUE TENEMOS EL VIDEO !!!!!!!!!
            if delegado != nil {
                delegado.terminaGrabacion(urlArchivo: videoUrl)
            }
            self.detenerSesion()
        }
    }
}

