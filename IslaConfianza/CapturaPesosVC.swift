//
//  CapturaPesosVC.swift
//  MKE
//
//  Created by Roberto Martínez Román on 12/7/19.
//  Copyright © 2019 Roberto Martinez Roman. All rights reserved.
//

import UIKit

class CapturaPesosVC: UIViewController
{
    var arrAlias: Array<String>!
    
    @IBOutlet weak var tablaPesos: UITableView!
    var delegado: DelegadoAlerta!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func cancelar(_ sender: Any) {
        self.delegado.obtenerValores([String:Int]())
        dismiss(animated: true)
    }
    @IBAction func cerrar(_ sender: Any) {
        
        if self.delegado != nil {
            // Crea el diccionario de regreso
            var dPesos = [String:Int]()
            
            let numCeldas = self.tablaPesos.numberOfRows(inSection: 0)
            for renglon in  0..<numCeldas {
                let index = IndexPath(row: renglon, section: 0)
                let celda = self.tablaPesos.cellForRow(at: index) as! CeldaPesos
                dPesos[celda.lblAlias.text!] = Int(celda.lblPeso.text!)!
            }
            self.delegado.obtenerValores(dPesos)
        }
        
        dismiss(animated: true)
    }
}

protocol DelegadoAlerta {
    func obtenerValores(_ diccionarioPesos: [String:Int])
}

extension CapturaPesosVC: UITableViewDataSource, UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAlias?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celda = tableView.dequeueReusableCell(withIdentifier: "celdaPeso", for: indexPath) as! CeldaPesos
        
        celda.lblAlias.text  = arrAlias[indexPath.row]
        
        return celda
    }
}


class CeldaPesos: UITableViewCell {
    
    @IBOutlet weak var lblAlias: UILabel!
    @IBOutlet weak var lblPeso: UILabel!
    
    @IBAction func cambiarPeso(_ sender: UIStepper) {
        
        lblPeso.text = "\(Int(sender.value))"
    }
}
