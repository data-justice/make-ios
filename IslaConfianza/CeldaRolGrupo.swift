//
//  CeldaRolGrupo.swift
//  MKE
//
//  Created by Roberto Mtz. Román on 8/20/19.
//  Copyright © 2019 Roberto Martinez Roman. All rights reserved.
//

import UIKit

class CeldaRolGrupo: UICollectionViewCell
{
    @IBOutlet weak var texto: UILabel!
    @IBOutlet weak var vistaSombra: UIView!
}
