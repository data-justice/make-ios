//
//  CreaIslaConfianzaVC.swift
//  MKE
//
//  Created by Roberto Mtz. Román on 8/7/19.
//  Copyright © 2019 Roberto Martinez Roman. All rights reserved.
//

import UIKit

class CreaIslaConfianzaVC: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    // Para regresar al menu de Plan de Seguridad
    var planSeguridadVC: UIViewController!
    
    var arrAlias = [String]()
    
    @IBOutlet weak var tablaAlias: UITableView!
    @IBOutlet weak var tfNombreIsla: UITextField!
    @IBOutlet weak var actEspera: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    @IBAction func capturarContacto(_ sender: UIButton) {
        let dialogoEntrada = UIAlertController(title: "Nuevo miembro", message: "Escribe el alias del miembro que quieres agregar:", preferredStyle: .alert)
        dialogoEntrada.addTextField { (tfAlias) in
            tfAlias.placeholder = "Alias"
        }
        
        let aceptar = UIAlertAction(title: "Aceptar", style: .default) { (alerta) in
            self.view.endEditing(true)
            let tfEntrada = dialogoEntrada.textFields![0]
            if (tfEntrada.text?.count)! > 0 { // Hay texto?
                self.arrAlias.append(tfEntrada.text!)
                self.tablaAlias.reloadData()
            }
        }
        
        dialogoEntrada.addAction(aceptar)
        self.present(dialogoEntrada, animated: true)
    }
    
    // MARK: - TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAlias.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celda = tableView.dequeueReusableCell(withIdentifier: "celdaAlias", for: indexPath)
        celda.textLabel?.text = arrAlias[indexPath.row]
        return celda
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Borrar"
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            arrAlias.remove(at: indexPath.row)
            self.tablaAlias.reloadData()
        }
    }
    
    // MARK: - Eventos
    
    @IBAction func enviarInvitacion(_ sender: UIButton) {
        DispatchQueue.main.async {
            self.enviarCrearIsla()
        }
    }
    
    // MARK: -
    func enviarCrearIsla() {
        if arrAlias.count == 0 {
            mostrarMensajeInfo(mensaje: "No puedes crear una isla vacía", cerrar: false)
            return
        }
        let nombreIsla = self.tfNombreIsla.text!
        
        if nombreIsla.count == 0 {
            mostrarMensajeInfo(mensaje: "Escribe el nombre de la isla primero", cerrar: false)
            return
        }
        
        self.actEspera.startAnimating()
        
        // Token para authorization
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        // Enviar información al servidor
        let url = URL(string: URL_CREA_ISLA)
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-type")
        request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.httpBody = """
            {
            "name": "\(nombreIsla)",
            "members": \(arrAlias)
            }
            """.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.actEspera.stopAnimating()
            }
            if error == nil {   // No hay error
                if let response = response {
                    let respuesta = response as! HTTPURLResponse
                    //print("Respuesta ISLA: \(respuesta)")
                    if respuesta.statusCode == OK_ISLA_CREADA {
                        if data != nil {
                            //let body = String(data: data!, encoding: .utf8)!
                            //print("Body reporte ISLA: \(body)")
                            self.mostrarMensajeInfo(mensaje: "La isla '\(nombreIsla)' se ha creado con éxito", cerrar: true)
                        } else {
                            self.mostrarMensajeInfo(mensaje: "Error al enviar los datos", cerrar: false)
                        }
                    } else if respuesta.statusCode == ERROR_ALIAS_NO_EXISTE {
                        self.mostrarMensajeInfo(mensaje: "Alguno de los alias no existe", cerrar: false)
                    } else if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                        self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                    }
                } else {
                    self.mostrarMensajeInfo(mensaje:"Error en el sertvicio", cerrar: false)
                }
            } else {
                if let error = error {
                    //print("Error desconocido, \(error)")
                    let tipoError = error as NSError
                    if tipoError.code == ERROR_CONEXION_INTERNET {
                        self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error, al enviar los datos", cerrar: false)
                    }
                }
            }
        }
        task.resume()
    }
    
    // Muestra un mensaje de información en la pantalla
    func mostrarMensajeInfo(mensaje: String, cerrar: Bool) {
        DispatchQueue.main.async {
            let alerta = UIAlertController(title: "Aviso", message: mensaje, preferredStyle: .alert)
            let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: { (alerta) in
                if mensaje == MENSAJE_CREDENCIALES_INVALIDAS {
                    self.navigationController?.popToRootViewController(animated: true)
                } else
                if cerrar {
                    // Regresa al plan de seguridad
                    if let vc = self.planSeguridadVC {
                        self.navigationController?.popToViewController(vc, animated: true)
                    } else {
                        //let n = self.navigationController?.viewControllers.count
                        //self.navigationController?.viewControllers.remove(at: n!-2)
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            })
            alerta.addAction(aceptar)
            self.present(alerta, animated: true, completion: nil)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}
