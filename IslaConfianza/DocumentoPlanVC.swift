//
//  DocumentoPlanVC.swift
//  MKE
//
//  Created by Roberto Martínez Román on 9/12/19.
//  Copyright © 2019 Roberto Martinez Roman. All rights reserved.
//

// Agrega gráfica de colores, 27-ene-2020

import UIKit
import WebKit
import PDFKit

class DocumentoPlanVC: UIViewController, WKNavigationDelegate
{
    // Datos de la Isla para obtener ponderaciones, también para crear gráfica
    // Los mando el controlador anterior
    var dIsla: [String:Any]!
    var idIsla: Int!
    var nombreIsla: String!
    
    var nombreArchivoPDF: String!
    
    var imgGraficoPlan: UIImage!
    var personaAusente: String!
    
    let modelos = ["democratic":"Democrático", "weighted":"Ponderado", "centralized":"Centralizado"]
    
    //
    var arrPlanes: NSMutableArray!
    
    @IBOutlet weak var webPlan: WKWebView!
    @IBOutlet weak var actEspera: UIActivityIndicatorView!
    // 0:UIColor(displayP3Red: 0.1020, green: 1.0000, blue: 0.2824, alpha: 1)
    
    let colores = [0:UIColor(displayP3Red: 0.5, green: 0.5, blue: 0.5, alpha: 1), 1:UIColor(displayP3Red: 1.0000, green: 0.5020, blue: 0.8196, alpha: 1), 2:UIColor(displayP3Red: 1.0000, green: 0.2824, blue: 0.1020, alpha: 1), 3:UIColor(displayP3Red: 0.1020, green: 0.8196, blue: 1.0000, alpha: 1), 4:UIColor(displayP3Red: 1.0000, green: 1.0000, blue: 0.2980, alpha: 1), 5:UIColor(displayP3Red: 0.2980, green: 0.2980, blue: 1.0000, alpha: 1), 6:UIColor(displayP3Red: 1.0000, green: 0.2980, blue: 1.0000, alpha: 1), 7:UIColor(displayP3Red: 0.2980, green: 1.0000, blue: 0.2980, alpha: 1), 8:UIColor(displayP3Red: 0.6706, green: 1.0000, blue: 0.4824, alpha: 1), 9:UIColor(displayP3Red: 0.8118, green: 0.4824, blue: 1.0000, alpha: 1), 10:UIColor(displayP3Red: 1.0000, green: 0.4824, blue: 0.6706, alpha: 1), 11:UIColor(displayP3Red: 0.4824, green: 1.0000, blue: 0.8118, alpha: 1), 12:UIColor(displayP3Red: 1, green: 0.5882, blue: 0.196, alpha: 1)]
    
    let ALTO_FUENTE: CGFloat = 18
    
    let ANCHO_GRAFICA = 650
    let ALTO_GRAFICA = 1000
    
    // ROLES del servidor
    var dRoles = [Int : String]() // 2: "Negociación", 3: "rol", id:nombre
    // ID's de roles, porque no corresponden con los índices del arreglo
    var dIdRoles: [Int:Int] = [Int:Int]()   // indiceArr:IdRol
    
    var dRolesUsados = [Int:String]()   // Solo los utilizados, id:nombre
    // Por qué no? indiceColor:nombre
    
    // Colores de cada rol
    var dRolColor = [Int:Int]()         // id_rol:llave_color
    var siguienteLlaveColor = 1         // Siguiente Llave disponible de color, límite colores.count-1
    
    
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        descargarPlanSeguridad()
    }
    
    func descargarPlanSeguridad() {
        // Token para authorization
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        // Enviar información al servidor, recibe token
        var dirServicio = URL_LISTA_PLANES_DE_SEGURIDAD
        dirServicio = dirServicio.replacingOccurrences(of: ":id", with: "\(self.idIsla!)")
        let url = URL(string: dirServicio)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-type")
        request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil {   // No hay error
                if let response = response {
                    let respuesta = response as! HTTPURLResponse
                    if respuesta.statusCode == OK_LISTA_PLANES_SEGURIDAD {
                        if let body = String(data: data!, encoding: .utf8) {
                            //print("Body LISTA PLANES: \(body)")
                            self.arrPlanes = try? (JSONSerialization.jsonObject(with: body.data(using: .utf8)!, options: .mutableContainers) as? NSMutableArray)
                            self.cargarTiposRolesServidor()
                            // Al terminar de descargar los roles, imprimirPDF
                            //self.imprimirPDF(arrPlanes: self.arrPlanes!)
                        } else {
                            self.mostrarMensajeInfo(mensaje: "Error al solicitar planes al servidor", cerrar: false)
                        }
                    } else if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                        self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error al solicitar planes al servidor", cerrar: false)
                    }
                } else {
                    self.mostrarMensajeInfo(mensaje: "Error interno, intente nuevamente", cerrar: false)
                }
            } else {
                if let error = error {
                    //print("Error desconocido, \(error)")
                    let tipoError = error as NSError
                    if tipoError.code == ERROR_CONEXION_INTERNET {
                        self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error, al solicitar los planes", cerrar: false)
                    }
                }
            }
        }
        task.resume()
    }
    
    func imprimirPDF(arrPlanes: NSMutableArray) {
        DispatchQueue.main.async {
            let strHTML = self.crearDocumento(arrPlanes: arrPlanes)
            self.webPlan.navigationDelegate = self
            self.webPlan.loadHTMLString(strHTML, baseURL:Bundle.main.bundleURL)
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        // Crear PDF
        self.nombreArchivoPDF = self.webPlan.exportAsPdfFromWebView()
        actEspera.stopAnimating()
    }
    
    // Muestra un mensaje de información en la pantalla
    func mostrarMensajeInfo(mensaje: String, cerrar: Bool) {
        DispatchQueue.main.async {
            let alerta = UIAlertController(title: "Aviso", message: mensaje, preferredStyle: .alert)
            let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: { (alerta) in
                if mensaje == MENSAJE_CREDENCIALES_INVALIDAS {
                    self.navigationController?.popToRootViewController(animated: true)
                } else
                if cerrar {
                    self.navigationController?.popViewController(animated: true)
                }
            })
            alerta.addAction(aceptar)
            self.present(alerta, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func compartirPDF(_ sender: UIButton) {
        
        let path = self.nombreArchivoPDF!
        let url = URL(fileURLWithPath: path)
        
        let actividadComparte = UIActivityViewController(activityItems: [url, "Este es el plan de seguridad para la isla '\(self.nombreIsla!)'"], applicationActivities: nil)
        actividadComparte.excludedActivityTypes = [.markupAsPDF]
        actividadComparte.setValue("Plan de seguridad", forKey: "subject")
        if UIDevice.current.userInterfaceIdiom == .pad {
            actividadComparte.modalPresentationStyle = .popover
            actividadComparte.popoverPresentationController?.sourceView = sender
            
        }
        present(actividadComparte, animated: true)
    }
    
    func crearDocumento(arrPlanes: NSMutableArray) -> String {
        
        var texto = """
        <!DOCTYPE html>
        <html>
        <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Plan de seguridad</title>
        </head>
        <body style="font-size:12px; font-family:'Arial'">
        <img src="MAKE_512.png" width="128" alt="MaKE">
        <h2>Plan de seguridad para la isla <em>\(nombreIsla!)</em></h2>
        <br>
        <hr>
        """
        
        for plan in arrPlanes {
            let dPlan = plan as! [String:Any]
            let personaAusente = dPlan["missing_person"] as? String
            if personaAusente == nil {
                continue
            }
            
            texto += "<h3>Persona ausente: \(personaAusente!)</h3>"
            let dMiembro = dPlan["members_roles"] as! [String: Any]
            // Roles de cada miembro
            for (miembro, arrRoles) in dMiembro {
                texto += "<h4>El miembro: \(miembro)</h4>"
                texto += "Tiene los siguientes roles:"
                let arrDRoles = arrRoles as! [[String:Any]]
                texto += "<ul>"
                for rol in arrDRoles  {
                    texto += "<li> \(rol["role"] as! String)</li>"
                }
                texto += "</ul>"
            }
            // Gobernanza
            let dGobernanza = dPlan["governance_details"] as! [String: Any]
            
            texto += "<h4>Modelo de gobernanza</h4>"
            if let modeloGobernanza = dGobernanza["winner"] as! String?
                , let modeloGanador = modelos[modeloGobernanza] {
                texto += "<strong>\((modeloGanador))</strong>"
            } else {
                texto += "Desconocido"
            }
            texto += "<hr>"
        }
        
        texto += "</body></html>"
        return texto
    }
    
    func crearGraficaPlan(_ arrPlanes: NSMutableArray, _ indicePlan: Int) -> UIImage? {
        // Utiliza dIsla para obtener la información
        let plan = arrPlanes[indicePlan] as! [String:Any]
        let detalles = plan["governance_details"] as! [String:AnyObject]
        let modeloGobernanza = detalles["winner"] as! String
        let aliasCentralizado = plan["group_centralized_person"] as? String ?? "desconocido"
        let dPesos = plan["group_user_weights"] as? [String:Int] ?? [String:Int]()
        
        self.dRolesUsados.removeAll()
        self.dRolColor.removeAll()
        self.siguienteLlaveColor = 1;   // El cero es gris
        
        let renderer = UIGraphicsImageRenderer(size: CGSize(width: ANCHO_GRAFICA, height: ALTO_GRAFICA))
        
        let img = renderer.image { context in
            // Fondo
            floral_white.setFill()
            context.fill(CGRect(x: 2, y: 2, width: ANCHO_GRAFICA-4, height: ALTO_GRAFICA-4))
            
            let contexto = context.cgContext
            let rectImagen = CGRect(x: 0, y: 0, width: ANCHO_GRAFICA, height: ALTO_GRAFICA-200)
            
            // Marco
            contexto.setStrokeColor(UIColor.darkGray.cgColor)
            contexto.addRect(CGRect(x: 0, y: 0, width: ANCHO_GRAFICA-1, height: ALTO_GRAFICA-1))
            contexto.strokePath()
            
            // Hay información?
            if arrPlanes.count >= 1 {
                let personaAusente = plan ["missing_person"] as? String ?? "desconocido"
                self.personaAusente = personaAusente
                // Círculo central
                contexto.setStrokeColor(UIColor.black.cgColor)
                contexto.setLineWidth(3)
                let centro = CGPoint(x: rectImagen.width/2, y: rectImagen.height/2)
                contexto.move(to: centro)
                let radio: CGFloat = 70
                let cuadro = CGRect(x: centro.x-radio, y: centro.y-radio, width: 2*radio, height: 2*radio)
                contexto.addEllipse(in: cuadro)
                contexto.strokePath()
                
                dibujarTexto(contexto, CGPoint(x: centro.x, y: centro.y), personaAusente, UIColor.black)
                
                // N = alias alrededor
                // Hay tantos, como members_roles en el diccionario
                if let roles_miembro = plan["members_roles"] as? [String:Any] {
                    let n = roles_miembro.count
                    if n == 0 {
                        return      // No hay roles
                    }
                    
                    let pasoAngulo = Double(360/n)
                    var i = 0
                    for roles in roles_miembro {
                        // Visita cada miembro y sus roles
                        let arrRoles = roles.value as! [[String:Any]]
                        let alias_rol = roles.key
                        
                        agregarRolesUsados(arrRoles)    // Para desplegar solo los usados
                        
                        let alfa: Double = Double(Double(i)*pasoAngulo*Double.pi/180)
                        var peso = 1
                        if modeloGobernanza == "weighted" {
                            peso = dPesos[alias_rol]!
                        } else if modeloGobernanza == "centralized" && alias_rol == aliasCentralizado {
                            peso = 4
                        }
                        
                        dibujarCirculosRoles(rect: rectImagen, alfa: CGFloat(alfa), alias_rol, arrRoles, peso)
                        
                        i += 1
                    }
                }
                // Texto del plan gráfico
                agregarTitulosPlanGrafico(contexto, rectImagen, modeloGobernanza)
            }
        }
        
        self.imgGraficoPlan = img
        
        return img
    }
    
    func agregarRolesUsados(_ arrRoles: [[String:Any]]) {
        for dRol in arrRoles {
            if let idRol = dRol["role_id"] as? Int, let nombreRol = dRol["role"] as? String {
                // GUARDAR indiceColor:nombreRol???????
                self.dRolesUsados[idRol] = nombreRol
            }
        }
    }
    
    func agregarTitulosPlanGrafico(_ contexto: CGContext, _ rect: CGRect, _ modelo:String) {
        dibujarTextoPlano(contexto, CGPoint(x: rect.size.width/2, y: ALTO_FUENTE*1.5), "Plan de seguridad para '\(self.personaAusente!)'", UIColor.black, esTitulo: true, centrado: true)
        
        dibujarTextoPlano(contexto, CGPoint(x: rect.size.width/2, y: ALTO_FUENTE*3.5), "Modelo de gobernanza: \(self.modelos[modelo]! )", UIColor.black, esTitulo: false, centrado: true)
        
        
        // Para cada rol, O - Rol  DOS COLUMNAS
        // (px,py) esquina inferior izquierda
        var px: CGFloat = 20
        var py: CGFloat = CGFloat(ALTO_GRAFICA-220)
        let radio: CGFloat = 15
        var primerColumna = true
        
        var numeroLinea = 0
        let rolesUsados = dRolesUsados
        
        let maximoRoles = min(12, rolesUsados.count)
        var rolesAgregados = 0
        for k in rolesUsados.keys {
            // Círculo
            let indiceColor = self.dRolColor[k]
            let rolColor = colores[indiceColor ?? 0]
            
            contexto.setFillColor(rolColor!.cgColor )
            let cuadro = CGRect(x: px-radio, y: py-radio, width: 2*radio, height: 2*radio)
            contexto.fillEllipse(in: cuadro)
            contexto.strokePath()
            // Circunferencia del rol
            contexto.setStrokeColor(UIColor.darkGray.cgColor)
            contexto.setLineWidth(1)
            contexto.addEllipse(in: cuadro)
            contexto.strokePath()
            
            // Extraer el nombre de rol
            let nombreRol = dRolesUsados[k]!
            
            // Rol
            dibujarTextoPlano(contexto, CGPoint(x: px + 2*radio, y: py), nombreRol, UIColor.black, esTitulo: false, centrado: false)
            
            py += radio*2.5
            
            if numeroLinea >= (maximoRoles+1)/2 - 1 && primerColumna {
                px += rect.size.width/2
                py = CGFloat(ALTO_GRAFICA-220)
                primerColumna = false
            }
            
            numeroLinea += 1
            
            rolesAgregados += 1
            if rolesAgregados >= maximoRoles {
                break
            }
        }
    }
    
    func dibujarCirculosRoles(rect: CGRect, alfa: CGFloat, _ nombre_alias: String, _ arrRoles:[[String:Any]], _ peso: Int) {
        let contexto = UIGraphicsGetCurrentContext()!
        let radioAlias: CGFloat = 200   // Círculo de círculos
        
        var indiceColor = 0
        
        let numeroRoles = arrRoles.count
        let RADIO_CENTRO = 70
        let RADIO_INICIO = RADIO_CENTRO + 10*peso
        var listaRadios = [RADIO_INICIO]      // radio para cada alias
        for i in 1..<numeroRoles {
            listaRadios.append(RADIO_INICIO - 10*i)
        }
        
        var trazarLinea = true
        var i = 0
        for rol in arrRoles {
            let radio = listaRadios[i]
            
            indiceColor = rol["role_id"] as? Int ?? 0
            
            // BUSCARLO EN UN POOL DE COLORES
            let id_rol = rol["role_id"] as! Int
            let id_color = self.dRolColor[id_rol]   // Existe?
            if id_color == nil {
                // No existe aún, agregarlo
                if self.siguienteLlaveColor < colores.count {   // hay color?
                    self.dRolColor[id_rol] = siguienteLlaveColor
                    siguienteLlaveColor += 1
                } else {
                    self.dRolColor[id_rol] = 0  // gris
                }
            }

            if let _ = self.colores[indiceColor] {
                
            } else {
                indiceColor = 0
            }
            
            // NUEVO color
            let colorRol = colores[self.dRolColor[id_rol]!]
            
            // Círculo
            contexto.setFillColor(colorRol!.cgColor)
            indiceColor += 1
            // (px,py) esquina inferior izquierda
            let px = radioAlias*cos(alfa) + CGFloat(rect.width/2)
            let py = CGFloat(rect.height/2) -  radioAlias*sin(alfa)
            let radioA: CGFloat = CGFloat(radio)
            let cuadro = CGRect(x: px-radioA, y: py-radioA, width: 2*radioA, height: 2*radioA)
            contexto.fillEllipse(in: cuadro)
            contexto.strokePath()
            
            // Circunferencia del rol
            contexto.setStrokeColor(UIColor.darkGray.cgColor)
            contexto.setLineWidth(1)
            contexto.addEllipse(in: cuadro)
            contexto.strokePath()
            // Alias
            dibujarTexto(contexto, CGPoint(x: px, y: py), nombre_alias, calcularInverso(colorRol!))
            
            if trazarLinea {
                trazarLinea = false
                // Línea
                let centro = CGPoint(x: rect.width/2, y: rect.height/2)
                contexto.setStrokeColor(UIColor.black.cgColor)
                contexto.setLineWidth(3)
                let centroCirculo = CGPoint(x: centro.x + CGFloat(70)*CGFloat(cos(alfa)), y: centro.y - CGFloat(70)*CGFloat(sin(alfa)))
                let centroAlias = CGPoint(x: px - CGFloat(radioA)*CGFloat(cos(alfa)), y: py + CGFloat(radioA)*CGFloat(sin(alfa)))
                contexto.move(to: centroCirculo)
                contexto.addLine(to: centroAlias)
                contexto.strokePath()
            }
            
            i += 1
        }
    }
    
    func calcularInverso(_ color: UIColor) -> UIColor {
        let r = color.cgColor.components![0]
        let g = color.cgColor.components![1]
        let b = color.cgColor.components![2]
        
        let promedio = (r+g+b)/3
        
        if promedio < 0.7 {
            return UIColor.black
        }
        
        return UIColor.white
    }
    
    func dibujarTexto(_ contexto: CGContext, _ centro: CGPoint, _ mensaje: String, _ color: UIColor) {
        UIGraphicsPushContext(contexto)
        var font = UIFont.systemFont(ofSize: ALTO_FUENTE)
        var altoFuente = ALTO_FUENTE
        var string = NSAttributedString(string: mensaje, attributes: [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: color])
        
        var alias = mensaje
        while string.size().width >= 150 && altoFuente > 12 {
            altoFuente -= 1
            font = UIFont.systemFont(ofSize: altoFuente)
            string = NSAttributedString(string: alias, attributes: [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: color])
        }
        while string.size().width >= 150 {
            alias.remove(at: alias.lastIndex(of: alias.last!)!)
            string = NSAttributedString(string: alias, attributes: [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: color])
        }
        var c = CGPoint(x: centro.x, y: centro.y)
        c.x -= string.size().width/2
        c.y -= string.size().height/2
        string.draw(at: c)
        UIGraphicsPopContext()
    }
    
    func dibujarTextoPlano(_ contexto: CGContext, _ centro: CGPoint, _ mensaje: String, _ color: UIColor, esTitulo: Bool, centrado: Bool) {
        UIGraphicsPushContext(contexto)
        var altoFuente = esTitulo ? ALTO_FUENTE*2 : ALTO_FUENTE
        var font = UIFont.systemFont(ofSize: altoFuente)
        var string = NSAttributedString(string: mensaje, attributes: [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: color])
        
        if !esTitulo {
            // Ajustar tamaño de fuente. Límite 12
            var nombreRol = mensaje
            while string.size().width >= 320 - 50 && altoFuente>10 {
                altoFuente -= 1
                font = UIFont.systemFont(ofSize: altoFuente)
                string = NSAttributedString(string: nombreRol, attributes: [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: color])
            }
            
            // Ajustar ancho
            while string.size().width >= 320 - 50 {
                nombreRol.remove(at: nombreRol.lastIndex(of: nombreRol.last!)!)
                string = NSAttributedString(string: nombreRol, attributes: [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: color])
            }
        }
        
        var c = CGPoint(x: centro.x, y: centro.y)
        if centrado {
            c.x -= string.size().width/2
        }
        c.y -= string.size().height/2
        string.draw(at: c)
        UIGraphicsPopContext()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueGrafico" {
            let vc = segue.destination as! MuestraPlanGrafico
            vc.imgPlan = self.imgGraficoPlan
            vc.personaAusente = self.personaAusente
        }
    }
    
    @IBAction func mostrarPlanGrafico(_ sender: UIButton) {
        // Mostrar opciones de plan (persona ausente y ejecutar el segueGrafico
        var dIndices = [String:Int]()
        var indice = 0      // Indice de cada plan
        if let arrPlanes = self.arrPlanes {
            // Muestra el menú
            let alerta = UIAlertController(title: "Aviso", message: "Selecciona plan de seguridad", preferredStyle: .alert)
            // Del arreglo, en cada diccionario, extraer "missing_person" y formar el menú
            for plan in arrPlanes {
                let dPlan = plan as! [String:AnyObject]
                let personaAusente = dPlan["missing_person"] as? String ?? "<null>"
                //print("-> \(personaAusente)")
                if personaAusente != "<null>" {
                    dIndices[personaAusente] = indice   // Para saber la posición (por null)
                    let btnPlan = UIAlertAction(title: personaAusente, style: .default) { (action) in
                        // Lanzar gráfica con esta persona ausente
                        let btnIndex = dIndices[action.title!]!
                        self.imgGraficoPlan = self.crearGraficaPlan(arrPlanes, btnIndex)
                        self.performSegue(withIdentifier: "segueGrafico", sender: DocumentoPlanVC.self)
                    }
                    alerta.addAction(btnPlan)
                }
                indice += 1
            }
            let cancelar = UIAlertAction(title: "Cancelar", style: .cancel)
            alerta.addAction(cancelar)
            self.present(alerta, animated: true)
        }
    }
    
    // ROLES
    func cargarTiposRolesServidor() {
        // Token para authorization
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        // Enviar información al servidor, recibe token
        let url = URL(string: URL_LISTA_ROLES)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-type")
        request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil {   // No hay error
                if let response = response {
                    let respuesta = response as! HTTPURLResponse
                    if respuesta.statusCode == OK_LISTA_ROLES {
                        if let body = String(data: data!, encoding: .utf8) {
                            self.extraerRoles(body)
                        } else {
                            self.mostrarMensajeInfo(mensaje: "Error al solicitar roles al servidor", cerrar: false)
                        }
                    } else if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                        self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error al solicitar roles al servidor", cerrar: false)
                    }
                }
                // Crea el documento, después de descargar los roles
                if self.arrPlanes != nil {
                    self.imprimirPDF(arrPlanes: self.arrPlanes!)
                }
            } else {
                if let error = error {
                    let tipoError = error as NSError
                    if tipoError.code == ERROR_CONEXION_INTERNET {
                        self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error, al solicitar los roles", cerrar: false)
                    }
                }
            }
        }
        task.resume()
    }
    
    func extraerRoles(_ body: String) {
        if let arrRoles = try? (JSONSerialization.jsonObject(with: body.data(using: .utf8)!, options: .mutableContainers) as? NSArray)  {
            var indice = 0
            for dRol in arrRoles  {
                let rol = dRol as! [String:Any]
                let id = rol["id"] as! Int
                self.dIdRoles[id] = indice
                let nombre = rol["name"] as! String
                self.dRoles[indice] = nombre
                indice += 1
            }
        }
    }
}



extension WKWebView {
    
    // Llamar a esta función  cuando el WebView haya terminado de cargar
    func exportAsPdfFromWebView() -> String {
        let pdfData = createPdfFile(printFormatter: self.viewPrintFormatter())
        return self.saveWebViewPdf(data: pdfData)
    }
    
    func createPdfFile(printFormatter: UIViewPrintFormatter) -> NSMutableData {
        
        let originalBounds = self.bounds
        self.bounds = CGRect(x: originalBounds.origin.x, y: bounds.origin.y, width: self.bounds.size.width, height: self.scrollView.contentSize.height)
        let pdfPageFrame = CGRect(x: 10, y: 20, width: 612-10, height: 792-20)
        let printPageRenderer = UIPrintPageRenderer()
        printPageRenderer.addPrintFormatter(printFormatter, startingAtPageAt: 0)
        printPageRenderer.setValue(NSValue(cgRect: UIScreen.main.bounds), forKey: "paperRect")
        printPageRenderer.setValue(NSValue(cgRect: pdfPageFrame), forKey: "printableRect")
        self.bounds = originalBounds
        return printPageRenderer.generatePdfData()
    }
    
    // Save pdf file in document directory
    func saveWebViewPdf(data: NSMutableData) -> String {
        
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let docDirectoryPath = paths[0]
        let pdfPath = docDirectoryPath.appendingPathComponent("planSeguridad.pdf")
        if data.write(to: pdfPath, atomically: true) {
            return pdfPath.path
        } else {
            return ""
        }
    }
}


extension UIPrintPageRenderer {
    
    func generatePdfData() -> NSMutableData {
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, self.paperRect, nil)
        self.prepare(forDrawingPages: NSMakeRange(0, self.numberOfPages))
        let printRect = UIGraphicsGetPDFContextBounds()
        for pdfPage in 0..<self.numberOfPages {
            UIGraphicsBeginPDFPage()
            self.drawPage(at: pdfPage, in: printRect)
        }
        UIGraphicsEndPDFContext();
        return pdfData
    }
}

