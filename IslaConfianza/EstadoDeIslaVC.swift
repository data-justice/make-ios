//
//  EstadoDeIslaVC.swift
//  MKE
//
//  Created by Roberto Mtz. Román on 9/3/19.
//  Copyright © 2019 Roberto Martinez Roman. All rights reserved.
//

import UIKit

class EstadoDeIslaVC: UIViewController, DelegadoAlerta
{
    var nombreIsla: String!
    var idIsla: Int!
    var estadoIsla: String!
    var hayRoles = false
    
    var islaEstable = false
    
    var listaMiembros: Array<String> = Array()  // Miembros de esta Isla
    
    // Datos de la Isla para enviar al plan de seguridad
    var dIsla: [String:Any]!
    
    let modelos = ["democratic":"Democrático", "weighted":"Ponderado", "centralized":"Centralizado"]
    let modelosIngles = ["Democrático":"democratic", "Ponderado":"weighted", "Centralizado":"centralized"]
    
    @IBOutlet weak var lblTituloEstado: UILabel!
    @IBOutlet weak var lblEstado: UILabel!
    @IBOutlet weak var lblGobernanza: UILabel!
    @IBOutlet weak var actEspera: UIActivityIndicatorView!
    
    @IBOutlet weak var tablaMiembros: UITableView!
    
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = self.nombreIsla
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        descargarMiembros()
    }
    
    
    @IBAction func agregarNuevoMiembro(_ sender: UIButton) {
        // Cuadro de diálogo, captura y envía
        let alerta = UIAlertController(title: "Agregar a la isla", message: "Alias que quieres agregar.", preferredStyle: .alert)
        alerta.addTextField { (textField) in
            textField.placeholder = "alias"
        }
        let aceptar = UIAlertAction(title: "Aceptar", style: .default) { (aceptarAction) in
            if let nuevoAlias = alerta.textFields?[0].text {
                if !nuevoAlias.isEmpty {
                    // Enviar nuevo alias
                    self.enviarAgregarMiembro(nuevoAlias)
                }
            }
        }
        let cancelar = UIAlertAction(title: "Cancelar", style: .cancel)
        alerta.addAction(aceptar)
        alerta.addAction(cancelar)
        present(alerta, animated: true)
    }
    
    func enviarAgregarMiembro(_ alias: String) {
        self.actEspera.startAnimating()
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        // Enviar información al servidor
        let direccion = URL_AGREGAR_MIEMBRO.replacingOccurrences(of: ":id", with: "\(self.idIsla!)")
        let url = URL(string: direccion + "/\(alias)")
        
        var request = URLRequest(url: url!)
        request.httpMethod = "PUT"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.actEspera.stopAnimating()
            }
            if error == nil {
                let respuesta = response! as! HTTPURLResponse
                //print("Respuesta AGTREGAR: \(respuesta)")
                if respuesta.statusCode == OK_AGREGA_MIEMBRO {
                    if let datosBin = data, let _ = String(data: datosBin, encoding: .utf8) {
                        //print("BODY GRUPO: \(body)")
                        DispatchQueue.main.async {
                            //self.seleccionarMiembroAusente(dGrupos!)
                            self.descargarMiembros()
                        }
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error en los datos", cerrar: false)
                    }
                } else if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                    self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                } else {
                    self.mostrarMensajeInfo(mensaje: "Error en los datos del usuario", cerrar: false)
                }
            } else {
                let tipoError = error as NSError?
                if tipoError!.code == ERROR_CONEXION_INTERNET {
                    self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                } else {
                    self.mostrarMensajeInfo(mensaje: "Error en la red, intente más tarde", cerrar: false)
                }
                
            }
        }
        task.resume()
    }
    /*
     @IBAction func consultarPlan(_ sender: UIButton) {
     self.mostrarMensajeInfo(mensaje: "Aquí obtienes un archivo PDF con los planes de seguridad de la Isla (pendiente).", cerrar: false)
     }
     */
    @IBAction func cambiarEstadoIsla(_ sender: UIButton) {
        cambiarPlanSeguridad()
    }
    
    func descargarMiembros() {
        self.actEspera.startAnimating()
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        // Enviar información al servidor
        let url = URL(string: "\(URL_LISTA_GRUPOS)/\(self.idIsla!)")
        
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.actEspera.stopAnimating()
            }
            if error == nil {
                let respuesta = response! as! HTTPURLResponse
                //print("Respuesta GRUPO: \(respuesta)")
                if respuesta.statusCode == OK_LISTA_GRUPOS {
                    if let datosBin = data, let body = String(data: datosBin, encoding: .utf8) {
                        // Buscar y mostrar miembros
                        let dGrupos = try? (JSONSerialization.jsonObject(with: body.data(using: .utf8)!, options: .mutableContainers) as! NSDictionary)
                        //print("BODY GRUPO estado: \(body)")
                        self.dIsla = dGrupos as? [String:Any] ?? [String:Any]()
                        DispatchQueue.main.async {
                            self.mostrarEstadoIsla(dGrupos!)
                        }
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error en los datos", cerrar: false)
                    }
                } else if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                    self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                } else {
                    self.mostrarMensajeInfo(mensaje: "Error en los datos del usuario", cerrar: false)
                }
            } else {
                let tipoError = error as NSError?
                if tipoError!.code == ERROR_CONEXION_INTERNET {
                    self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                } else {
                    self.mostrarMensajeInfo(mensaje: "Error en la red, intente más tarde", cerrar: false)
                }
            }
        }
        task.resume()
    }
    
    func mostrarEstadoIsla(_ dGrupo: NSDictionary) {
        //  Extraer miembros
        listaMiembros.removeAll()
        
        let miembros = dGrupo.value(forKey: "members") as! NSArray
        for miembro in miembros {
            let dMiembro = miembro as! NSDictionary
            listaMiembros.append(dMiembro.value(forKey: "username") as! String)
        }
        
        let dPlanSeguridad = dGrupo.value(forKey: "active_security_plan")
        
        // PRIMERO prueba si está estable
        if let estable = dGrupo.value(forKey: "stable") as? Bool {
            self.lblTituloEstado.text = "Estado actual de la isla"
            if estable {
                self.islaEstable = true
                let modelo = dGrupo.value(forKey: "governance_model") as! String
                self.lblEstado.text = "Estable"
                self.lblGobernanza.text = self.modelos[modelo];
                if modelo == "centralized" {
                    if let strCentralizado = dGrupo.value(forKey: "centralized_user") {
                        self.lblGobernanza.text?.append(" (\(strCentralizado))")
                    }
                }
                self.lblEstado.backgroundColor = UIColor(displayP3Red: 0.93, green: 1, blue: 0.93, alpha: 1)
                self.lblGobernanza.backgroundColor = UIColor(displayP3Red: 0.93, green: 1, blue: 0.93, alpha: 1)
            } else {
                self.islaEstable = true
                self.lblEstado.text = "Estable"
                self.lblGobernanza.text = "NA"
                self.lblEstado.backgroundColor = UIColor(displayP3Red: 0.93, green: 1, blue: 0.93, alpha: 1)
                self.lblGobernanza.backgroundColor = UIColor(displayP3Red: 0.93, green: 1, blue: 0.93, alpha: 1)
                
                if let dPlan = dPlanSeguridad as? NSDictionary {
                    self.islaEstable = false
                    if let plan = dPlan.value(forKey: "missing_person") {
                        self.lblEstado.text = plan as? String
                        self.lblTituloEstado.text = "Hay una persona desaparecida:"
                    } else {
                        self.lblTituloEstado.text = "Estado actual de la isla"
                    }
                    if let dModeloGobernanza = dPlan.value(forKey: "governance_details") as? NSDictionary {
                        let modeloGobernanza = dModeloGobernanza.value(forKey: "winner")
                        let strModelo = modeloGobernanza as! String
                        self.lblGobernanza.text = self.modelos[strModelo]
                        if strModelo == "centralized" {
                            if let strCentralizado = dGrupo.value(forKey: "centralized_user") {
                                self.lblGobernanza.text?.append(" (\(strCentralizado))")
                            }
                        }
                    }
                    self.lblEstado.backgroundColor = UIColor(displayP3Red: 1, green: 0.93, blue: 0.93, alpha: 1)
                    self.lblGobernanza.backgroundColor = UIColor(displayP3Red: 1, green: 0.93, blue: 0.93, alpha: 1)
                }
            }
        } else {
            self.islaEstable = true
            self.lblEstado.text = "No hay plan de seguridad"
            self.lblGobernanza.text = "NA"
        }
        self.tablaMiembros.reloadData()
    }
    
    // Hay una persona ausente, activar el plan de seguridad correspondiente
    func cambiarPlanSeguridad()  {
        if listaMiembros.count > 0 {
            // Menú
            let alerta = UIAlertController(title: "Activar plan", message: "Selecciona a la persona ausente", preferredStyle: .actionSheet)
            for alias in listaMiembros {
                let accion = UIAlertAction(title: alias, style: .default) { (action) in
                    // Enviar plan de seguridad
                    self.activarPlanSeguridad(alias)
                }
                alerta.addAction(accion)
            }
            let cancelar = UIAlertAction(title: "Cancelar", style: .cancel) { (action) in
                
            }
            let regresarEstable = UIAlertAction(title: "Estable", style: .default) { (action) in
                self.activarPlanSeguridad("estable")
            }
            alerta.addAction(regresarEstable)
            alerta.addAction(cancelar)
            
            if ( UIDevice.current.userInterfaceIdiom  == UIUserInterfaceIdiom.pad )
            {
                if let ppOver = alerta.popoverPresentationController {
                    ppOver.sourceView = self.lblGobernanza
                    ppOver.sourceRect = self.lblGobernanza.frame
                    self.present(alerta, animated: true)
                }
            } else {
                present(alerta, animated: true)
            }
            
            //self.present(alerta, animated: true)
        } else {
            mostrarMensajeInfo(mensaje: "No hay miembros suficientes en la isla", cerrar: false)
        }
    }
    
    func activarPlanSeguridad(_ alias: String) {
        self.actEspera.startAnimating()
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        
        // Enviar información al servidor
        let direccion = URL_ACTIVA_PLAN_SEGURIDAD.replacingOccurrences(of: ":id", with: "\(self.idIsla!)")
        let url = URL(string: direccion)
        var request = URLRequest(url: url!)
        request.httpMethod = "PUT"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        if alias == "estable" {
            request.httpBody = """
            {
            "stable": true
            }
            """.data(using: .utf8)
        } else  {
            request.httpBody = """
                {
                "missing_person": "\(alias)"
                }
                """.data(using: .utf8)
        }
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.actEspera.stopAnimating()
            }
            if error == nil {
                let respuesta = response! as! HTTPURLResponse
                //print("Respuesta ACTIVA PLAN: \(respuesta)")
                if respuesta.statusCode == OK_LISTA_GRUPOS {
                    if let datosBin = data, let body = String(data: datosBin, encoding: .utf8) {
                        // Buscar el id de esta isla y mostrar miembros
                        let dGrupos = try? (JSONSerialization.jsonObject(with: body.data(using: .utf8)!, options: .mutableContainers) as! NSDictionary)
                        //print("Diccionario PLAN activado: \(dGrupos!)")
                        DispatchQueue.main.async {
                            self.actualizarDatosPlan(dGrupos!, alias)
                        }
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error en los datos", cerrar: false)
                    }
                } else if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                    self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                } else {
                    self.mostrarMensajeInfo(mensaje: "Error en los datos del usuario", cerrar: false)
                }
            } else {
                let tipoError = error as NSError?
                if tipoError!.code == ERROR_CONEXION_INTERNET {
                    self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                } else {
                    self.mostrarMensajeInfo(mensaje: "Error en la red, intente más tarde", cerrar: false)
                }
            }
        }
        task.resume()
    }
    
    func actualizarDatosPlan(_ dGrupo: NSDictionary, _ alias: String) {
        // Actualiza la información de esta Isla, con el nuevo plan
        
        // Primero pregunta si está estable
        if let estable = dGrupo.value(forKey: "stable") as? Bool {
            self.lblTituloEstado.text = "Estado actual de la isla"
            if estable {
                self.islaEstable = true
                self.lblEstado.text = "Estable"
                //self.lblGobernanza.text = "Democrático" //self.modelos[modelo];
                let modelo = dGrupo.value(forKey: "governance_model") as! String
                self.lblGobernanza.text = self.modelos[modelo]
                self.lblEstado.backgroundColor = UIColor(displayP3Red: 0.93, green: 1, blue: 0.93, alpha: 1)
                self.lblGobernanza.backgroundColor = UIColor(displayP3Red: 0.93, green: 1, blue: 0.93, alpha: 1)
            }
            return
        }
        
        let dPlanSeguridad = dGrupo.value(forKey: "active_security_plan")
        if let dPlan = dPlanSeguridad as? NSDictionary {
            self.islaEstable = false
            if let plan = dPlan.value(forKey: "missing_person") {
                self.lblEstado.text = plan as? String
            }
            if let dModeloGobernanza = dPlan.value(forKey: "governance_details") as? NSDictionary {
                let modeloGobernanza = dModeloGobernanza.value(forKey: "winner")
                let strModelo = modeloGobernanza as! String
                self.lblGobernanza.text = self.modelos[strModelo]
                if strModelo == "centralized" {
                    // PREGUNTAR:
                    // - QUIÉN TOMA LAS DECISIONES (centralizado)
                    self.designarCentralizado(alias)
                } else if strModelo == "weighted" {
                    // Pregunta los pesos de c/u (1-5)
                    let sb = UIStoryboard(name: "Main", bundle: nil)
                    let alerta = sb.instantiateViewController(withIdentifier: "vcPesos") as! CapturaPesosVC
                    alerta.delegado = self      // REGRESA a obtenerValores
                    alerta.arrAlias = self.listaMiembros
                    present(alerta, animated: true)
                }
            }
            self.lblEstado.backgroundColor = UIColor(displayP3Red: 1, green: 0.93, blue: 0.93, alpha: 1)
            self.lblGobernanza.backgroundColor = UIColor(displayP3Red: 1, green: 0.93, blue: 0.93, alpha: 1)
        } else {
            self.islaEstable = true
            self.lblTituloEstado.text = "Estado de la isla"
            self.lblEstado.text = "Estable"
            self.lblGobernanza.text = "Democrático"
            
            self.lblEstado.backgroundColor = UIColor(displayP3Red: 0.93, green: 1, blue: 0.93, alpha: 1)
            self.lblGobernanza.backgroundColor = UIColor(displayP3Red: 0.93, green: 1, blue: 0.93, alpha: 1)
        }
        
        self.mostrarMensajeInfo(mensaje: "Se ha cambiado con éxito el estado de la isla", cerrar: false)
    }
    
    // Designa al miembro que tomará decisiones en un modelo CENTRALIZADO
    func designarCentralizado(_ aliasAusente: String) {
        if listaMiembros.count > 0 {
            // Menú
            let alerta = UIAlertController(title: "Modelo CENTRALIZADO", message: "Selecciona a la persona que toma las decisiones", preferredStyle: .actionSheet)
            for alias in listaMiembros {
                if aliasAusente != alias {  // El ausente no puede ser
                    let accion = UIAlertAction(title: alias, style: .default) { (action) in
                        // Enviar persona designada
                        self.enviarMiembroCentralizado(alias)
                    }
                    alerta.addAction(accion)
                }
            }
            
            if ( UIDevice.current.userInterfaceIdiom  == UIUserInterfaceIdiom.pad )
            {
                if let ppOver = alerta.popoverPresentationController {
                    ppOver.sourceView = self.lblGobernanza
                    ppOver.sourceRect = self.lblGobernanza.frame
                    self.present(alerta, animated: true)
                }
            } else {
                present(alerta, animated: true)
            }
        }
    }
    
    func enviarMiembroCentralizado(_ alias: String) {
        actEspera.startAnimating()
        
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        // Enviar información al servidor
        let direccion = URL_ASIGNAR_CENTRALIZADO.replacingOccurrences(of: ":id", with: "\(self.idIsla!)")
        let url = URL(string: direccion)
        var request = URLRequest(url: url!)
        request.httpMethod = "PUT"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        request.httpBody = """
            {
            "user": "\(alias)"
            }
            """.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.actEspera.stopAnimating()
            }
            if error == nil {
                let respuesta = response! as! HTTPURLResponse
                //print("Respuesta PUT centralizado: \(respuesta)")
                if respuesta.statusCode == OK_ASIGNAR_CENTRALIZADO {
                    DispatchQueue.main.async {
                        self.lblGobernanza.text?.append(" (\(alias))")
                    }
                    self.mostrarMensajeInfo(mensaje: "Se asignó a \(alias) como la persona que toma decisiones", cerrar: false)
                    //let body = String(data: data!, encoding: .utf8)
                    //print("Body reporte CENTRALIZADO: \(body!)")
                } else if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                    self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                } else {
                    self.mostrarMensajeInfo(mensaje: "Error en el servidor", cerrar: false)
                }
            } else {
                let tipoError = error as NSError?
                if tipoError!.code == ERROR_CONEXION_INTERNET {
                    self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                } else {
                    self.mostrarMensajeInfo(mensaje: "Error en el servicios, intente más tarde", cerrar: false)
                }
            }
        }
        task.resume()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "seguePlanSeguridad" {
            let vc = segue.destination as! DocumentoPlanVC
            vc.idIsla = self.idIsla
            vc.nombreIsla = self.nombreIsla
            vc.dIsla = self.dIsla
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        /*
         if !hayRoles {
         mostrarMensajeInfo(mensaje: "No hay roles definidos para este plan de seguridad, no se puede generar el documento.", cerrar: false)
         }
         return hayRoles
         */
        return true
    }
    
    // Muestra un mensaje de información en la pantalla
    func mostrarMensajeInfo(mensaje: String, cerrar: Bool) {
        DispatchQueue.main.async {
            let alerta = UIAlertController(title: "Aviso", message: mensaje, preferredStyle: .alert)
            let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: { (alerta) in
                if mensaje == MENSAJE_CREDENCIALES_INVALIDAS {
                    self.navigationController?.popToRootViewController(animated: true)
                } else if cerrar {
                    self.navigationController?.popViewController(animated: true)
                }
            })
            alerta.addAction(aceptar)
            self.present(alerta, animated: true, completion: nil)
        }
    }
    
    func eliminarDeIsla(_ alias: String) {
        actEspera.startAnimating()
        
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        // Enviar información al servidor
        let direccion = URL_BORRAR_MIEMBRO.replacingOccurrences(of: ":id", with: "\(self.idIsla!)")
        let url = URL(string: direccion + "/\(alias)")
        var request = URLRequest(url: url!)
        request.httpMethod = "DELETE"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.actEspera.stopAnimating()
            }
            if error == nil {
                let respuesta = response! as! HTTPURLResponse
                //print("Respuesta DELETE: \(respuesta)")
                if respuesta.statusCode == OK_BORRAR_MIEMBRO {
                    self.mostrarMensajeInfo(mensaje: "Se ha eliminado a \(alias) de esta isla", cerrar: false)
                    DispatchQueue.main.async {
                        self.descargarMiembros()
                    }
                } else if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                    self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                } else {
                    self.mostrarMensajeInfo(mensaje: "Error en el servidor", cerrar: false)
                }
            } else {
                let tipoError = error as NSError?
                if tipoError!.code == ERROR_CONEXION_INTERNET {
                    self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                } else {
                    self.mostrarMensajeInfo(mensaje: "Error en el servicios, intente más tarde", cerrar: false)
                }
            }
        }
        task.resume()
    }
    
    // CAMBIAR el modelo de gobernanza
    @IBAction func cambiarModeloGobernanza(_ sender: UIButton) {
        
        if !self.islaEstable {
            mostrarMensajeInfo(mensaje: "Hay un plan de seguridad activado.\nPara modificar el modelo de gobernanza puedes redefinir roles y modelo de gobernanza.", cerrar: false)
            return
        }
        
        // Pregunta por el nuevo modelo
        // Menú de modelos
        let alerta = UIAlertController(title: "Cambiar modelo", message: "Selecciona el nuevo Modelo de gobernanza", preferredStyle: .actionSheet)
        let modelos = ["Democrático","Centralizado","Ponderado"]
        for modelo in modelos {
            let accion = UIAlertAction(title: modelo, style: .default) { (action) in
                // Enviar nuevo modelo
                self.enviarCambioModelo(modelo)
            }
            alerta.addAction(accion)
        }
        let cancelar = UIAlertAction(title: "Cancelar", style: .cancel)
        alerta.addAction(cancelar)
        if ( UIDevice.current.userInterfaceIdiom  == UIUserInterfaceIdiom.pad )
        {
            if let ppOver = alerta.popoverPresentationController {
                ppOver.sourceView = self.lblGobernanza
                ppOver.sourceRect = self.lblGobernanza.frame
                self.present(alerta, animated: true)
            }
        } else {
            present(alerta, animated: true)
        }
    }
    
    func enviarCambioModelo(_ modelo: String)   {
        //print("Cambia a \(modelo)")
        // ENVIAR al servidor el cambio de modelo mientras sigue ESTABLE
        actEspera.startAnimating()
        
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        // Enviar información al servidor
        let direccion = URL_CAMBIA_MG_ESTABLE.replacingOccurrences(of: ":id", with: "\(self.idIsla!)")
        let url = URL(string: direccion)
        var request = URLRequest(url: url!)
        request.httpMethod = "PUT"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        request.httpBody = """
            {
            "governance_model": "\(self.modelosIngles[modelo]!)"
            }
            """.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.actEspera.stopAnimating()
            }
            if error == nil {
                let respuesta = response! as! HTTPURLResponse
                //print("Respuesta PUT CAMBIA MG estable: \(respuesta)")
                if respuesta.statusCode == OK_CAMBIA_MG_ESTABLE {
                    //self.mostrarMensajeInfo(mensaje: "Se cambió el modelo de gobernanza a \(modelo)", cerrar: false)
                    //let body = String(data: data!, encoding: .utf8)
                    //print("Body CAMBIA MG: \(body!)")
                    DispatchQueue.main.async {
                        // Si es exitoso,
                        self.ajustarPesosModelo(modelo)
                        self.lblGobernanza.text = "\(modelo)"
                    }
                } else if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                    self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                } else {
                    self.mostrarMensajeInfo(mensaje: "Error en el servidor", cerrar: false)
                }
            } else {
                let tipoError = error as NSError?
                if tipoError!.code == ERROR_CONEXION_INTERNET {
                    self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                } else {
                    self.mostrarMensajeInfo(mensaje: "Error en el servicio, intente más tarde", cerrar: false)
                }
            }
        }
        task.resume()
    }
    
    func ajustarPesosModelo(_  modelo: String) {
        if (modelo=="Democrático")  {
            // Todos los pesos en 1
            
        } else if modelo == "Centralizado" {
            // Pregunta el alias centralizado, él en 1, los demás en cero
            let alerta = UIAlertController(title: "Modelo Centralizado", message: "Selecciona el alias que toma las decisiones", preferredStyle: .actionSheet)
            
            for alias in listaMiembros {
                let accion = UIAlertAction(title: alias, style: .default) { (action) in
                    // Enviar nuevo miembro que toma decisiones /*****/ revisar servicio
                    self.enviarMiembroCentralizado(alias)
                }
                alerta.addAction(accion)
            }
            let cancelar = UIAlertAction(title: "Cancelar", style: .cancel)
            alerta.addAction(cancelar)
            
            if ( UIDevice.current.userInterfaceIdiom  == UIUserInterfaceIdiom.pad )
            {
                if let ppOver = alerta.popoverPresentationController {
                    ppOver.sourceView = self.lblGobernanza
                    ppOver.sourceRect = self.lblGobernanza.frame
                    self.present(alerta, animated: true)
                }
            } else {
                present(alerta, animated: true)
            }
            
            //self.present(alerta, animated: true)
            
        } else if modelo == "Ponderado" {
            // Pregunta los pesos de c/u (1-5)
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let alerta = sb.instantiateViewController(withIdentifier: "vcPesos") as! CapturaPesosVC
            alerta.delegado = self
            alerta.arrAlias = self.listaMiembros
            present(alerta, animated: true)
        }
    }
    
    // Delegado que lee los pesos de los usuarios
    func obtenerValores(_ diccionarioPesos: [String : Int]) {
        //print(diccionarioPesos)
        // MANDA al servidor los nuevos valores
        if diccionarioPesos.count > 0 {
            self.enviarPesosPonderado(diccionarioPesos)
        }
    }
    
    func enviarPesosPonderado(_ diccionarioPesos: [String:Int]) {
        actEspera.startAnimating()
        
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        // Enviar información al servidor
        let direccion = URL_ASIGNAR_PESOS_PONDERADO.replacingOccurrences(of: ":id", with: "\(self.idIsla!)")
        let url = URL(string: direccion)
        var request = URLRequest(url: url!)
        request.httpMethod = "PUT"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        // Crear "alias":peso
        var aliasPesos = "";
        for aliasPeso in diccionarioPesos {
            aliasPesos += "\"\(aliasPeso.key)\":\(aliasPeso.value),\n"
        }
        
        request.httpBody = """
            {
            "weights": {
            \(aliasPesos.prefix(aliasPesos.count-2))
            }
            }
            """.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.actEspera.stopAnimating()
            }
            if error == nil {
                let respuesta = response! as! HTTPURLResponse
                //print("Respuesta PUT centralizado: \(respuesta)")
                if respuesta.statusCode == OK_ASIGNAR_CENTRALIZADO {
                    
                    self.mostrarMensajeInfo(mensaje: "Se asignaron  correctamente los pesos", cerrar: false)
                    //let body = String(data: data!, encoding: .utf8)
                    //print("Body PESOS: \(body!)")
                    DispatchQueue.main.async {
                        self.lblGobernanza.text = "Ponderado"
                    }
                } else if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                    self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                } else {
                    self.mostrarMensajeInfo(mensaje: "Error en el servidor", cerrar: false)
                }
            } else {
                let tipoError = error as NSError?
                if tipoError!.code == ERROR_CONEXION_INTERNET {
                    self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                } else {
                    self.mostrarMensajeInfo(mensaje: "Error en el servicios, intente más tarde", cerrar: false)
                }
            }
        }
        task.resume()
    }
}

extension EstadoDeIslaVC: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listaMiembros.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celda = tableView.dequeueReusableCell(withIdentifier: "celdaMiembro", for: indexPath)
        celda.textLabel?.text = self.listaMiembros[indexPath.row]
        return celda
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Eliminar de la isla"
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let alias = listaMiembros[indexPath.row]
            let alerta = UIAlertController(title: "Aviso", message: "¿Quieres eliminar a \(alias) de esta isla?", preferredStyle: .alert)
            let aceptar = UIAlertAction(title: "Si, quiero eliminarlo de la isla", style: .destructive) { (action) in
                //print("REGISTRO alias borrar: \(alias)")
                self.eliminarDeIsla(alias)
            }
            let cancelar = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            alerta.addAction(aceptar)
            alerta.addAction(cancelar)
            self.present(alerta, animated: true, completion: nil)
        }
    }
}
