//
//  InfoIslaVC.swift
//  MKE
//
//  Created by Roberto Martínez Román on 8/13/19.
//  Copyright © 2019 Roberto Martinez Roman. All rights reserved.
//

import UIKit

class InfoIslaVC: UIViewController
{
    var infoAceptada = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        leerInfoAceptada()
    }
    
    func leerInfoAceptada() {
        let preferencias = UserDefaults.standard
        self.infoAceptada = preferencias.bool(forKey: "infoAceptada")
    }
    
    // MARK: - Eventos
    
    @IBAction func mostrarCrearIsla(_ sender: UIButton) {
        self.infoAceptada = true
        let preferencias = UserDefaults.standard
        preferencias.set(self.infoAceptada, forKey: "infoAceptada")
        performSegue(withIdentifier: "segueCreaIsla", sender: self)
    }
    
    @IBAction func cancelarCrearIsla(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Navigation

    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        return self.infoAceptada
    }
}
