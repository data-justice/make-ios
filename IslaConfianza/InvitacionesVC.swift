//
//  EstadoIslaVC.swift
//  MKE
//
//  Created by Roberto Mtz. Román on 8/16/19.
//  Copyright © 2019 Roberto Martinez Roman. All rights reserved.
//

import UIKit

class InvitacionesVC: UIViewController
{
    var arrInvitaciones: NSMutableArray! = NSMutableArray()
    var arrAceptados: NSMutableArray! = NSMutableArray()
    
    var idIsla: Int!
    
    @IBOutlet weak var tablaInvitaciones: UITableView!
    @IBOutlet weak var tablaAceptados: UITableView!
    
    @IBOutlet weak var lblInvitacionesPendientes: UILabel!
    
    @IBOutlet weak var lblInvitacionesAceptadas: UILabel!
    @IBOutlet weak var actEspera: UIActivityIndicatorView!
    
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Cargar las invitaciones de este usuario
        cargarInvitaciones()
    }
    
    func cargarInvitaciones() {
        actEspera.startAnimating()
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        // Enviar información al servidor
        let url = URL(string: URL_LISTA_MEMBERSHIPS)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.actEspera.stopAnimating()
            }
            if error == nil {
                let respuesta = response! as! HTTPURLResponse
                //print("Respuesta GRUPOS islas: \(respuesta)")
                if respuesta.statusCode == OK_LISTA_GRUPOS {
                    if let datosBin = data, let body = String(data: datosBin, encoding: .utf8) {
                        //print("BODY  isla \(body)")
                        // Llega un diccionario con llaves 'pending', 'active'
                        let dGrupos = try? (JSONSerialization.jsonObject(with: body.data(using: .utf8)!, options: .mutableContainers) as! NSMutableDictionary)
                        //print(dGrupos!)
                        self.arrInvitaciones = dGrupos?["pending"]! as? NSMutableArray
                        self.arrAceptados = dGrupos?["active"]! as? NSMutableArray
                        DispatchQueue.main.async {
                            self.tablaInvitaciones.reloadData()
                            self.tablaAceptados.reloadData()
                        }
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error en los datos json", cerrar: false)
                    }
                } else if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                    self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                } else {
                    self.mostrarMensajeInfo(mensaje: "Error en los datos del usuario", cerrar: false)
                }
            } else {
                let tipoError = error as NSError?
                if tipoError!.code == ERROR_CONEXION_INTERNET {
                    self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                } else {
                    self.mostrarMensajeInfo(mensaje: "Error en la red, intente más tarde", cerrar: false)
                }
                
            }
        }
        task.resume()
    }
    
    // Muestra un mensaje de información en la pantalla
    func mostrarMensajeInfo(mensaje: String, cerrar: Bool) {
        DispatchQueue.main.async {
            let alerta = UIAlertController(title: "Aviso", message: mensaje, preferredStyle: .alert)
            let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: { (alerta) in
                if mensaje == MENSAJE_CREDENCIALES_INVALIDAS {
                    self.navigationController?.popToRootViewController(animated: true)
                } else if cerrar {
                    self.navigationController?.popViewController(animated: true)
                }
            })
            alerta.addAction(aceptar)
            self.present(alerta, animated: true, completion: nil)
        }
    }
    
    @IBAction func definirRoles(_ sender: UIButton) {
        if UserDefaults.standard.bool(forKey: "AceptaRoles")==false {
            let alerta = UIAlertController(title: "Aviso", message: """
Definir roles de manera preventiva ayuda a tomar decisiones objetivamente y ganar tiempo en caso de un secuestro o situación de crisis.
            
Algunos roles necesarios son: negociación, acercamiento con autoridades, compra de alimentos y/o medicinas, cuidado de menores, entre otros. Estos roles dependerán de la dinámica que cada Isla de confianza tenga, pudiendo surgir nuevos roles.
""", preferredStyle: .alert)
            let aceptar = UIAlertAction(title: "Entiendo, no mostrar nuevamente", style: .default) { (action) in
                UserDefaults.standard.set(true, forKey: "AceptaRoles")
                if self.shouldPerformSegue(withIdentifier: "segueRoles", sender: self) {
                    self.performSegue(withIdentifier: "segueRoles", sender: self)
                }
            }
            let cancelar = UIAlertAction(title: "Cancelar", style: .cancel) { (action) in
                UserDefaults.standard.set(false, forKey: "AceptaRoles")
            }
            alerta.addAction(aceptar)
            alerta.addAction(cancelar)
            self.present(alerta, animated: true)
        } else {
            DispatchQueue.main.async {
                if self.shouldPerformSegue(withIdentifier: "segueRoles", sender: self) {
                    self.performSegue(withIdentifier: "segueRoles", sender: self)
                }
            }
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        
        //print("Prueba salto ")
        if let _ = self.tablaAceptados.indexPathForSelectedRow {
            return true
        } else {
            mostrarMensajeInfo(mensaje: "Selecciona una isla para definir roles", cerrar: false)
            return false
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! RolesVC
        let indexPath = self.tablaAceptados.indexPathForSelectedRow!
        vc.dMembership = self.arrAceptados[indexPath.row] as? NSDictionary
    }
}

extension InvitacionesVC: UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView==self.tablaInvitaciones {
            if self.arrInvitaciones.count == 0 {
                self.lblInvitacionesPendientes.text = "No tienes invitaciones pendientes"
            } else {
                self.lblInvitacionesPendientes.text = "Invitaciones pendientes"
            }
            return self.arrInvitaciones.count
        } else {
            // Aceptados
            if self.arrAceptados.count == 0 {
                self.lblInvitacionesAceptadas.text = "No tienes invitaciones aceptadas"
            } else {
                self.lblInvitacionesAceptadas.text = "Invitaciones aceptadas"
            }
            return self.arrAceptados.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView==self.tablaInvitaciones {
            let celda = tableView.dequeueReusableCell(withIdentifier: "celdaInvitacion", for: indexPath)
            
            let registro = self.arrInvitaciones![indexPath.row] as! NSDictionary
            let grupo = registro["group"] as! NSDictionary
            let isla = grupo["name"] as! String
            celda.textLabel?.text = isla
            
            return celda
        } else {
            // Aceptados
            let celda = tableView.dequeueReusableCell(withIdentifier: "celdaAceptado", for: indexPath)
            
            let registro = self.arrAceptados![indexPath.row] as! NSDictionary
            let grupo = registro["group"] as! NSDictionary
            let isla = grupo["name"] as! String
            celda.textLabel?.text = isla
            
            return celda
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.tablaInvitaciones {
            // Confirmar
            let alerta = UIAlertController(title: "Confirmación", message: "¿Aceptas pertenecer a esta Isla de Confianza?", preferredStyle: .alert)
            alerta.addAction(UIAlertAction(title: "Si", style: .default, handler: { action in
                // Enviar acepta invitación
                let registro = self.arrInvitaciones![indexPath.row] as! NSDictionary
                let id = registro["membership_id"] as? Int ?? 0
                //print(id)
                self.enviarAceptaInvitacion(id)
            }))
            alerta.addAction(UIAlertAction(title: "No", style: .default, handler: { action in
                // No
            }))
            alerta.addAction(UIAlertAction(title: "Cancelar", style: .cancel))
            self.present(alerta, animated: true, completion: nil)
        }
    }
    
    // Solo se pueden borrar isla aceptadas
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if tableView == self.tablaInvitaciones {
            return false
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Abandonar isla"
    }
    
    // Filtrar, solo se puede abandonar una isla aceptada
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            let alerta = UIAlertController(title: "Aviso", message: "¿Quieres abandonar esta isla de confianza?", preferredStyle: .alert)
            let aceptar = UIAlertAction(title: "Si, quiero abandonar la isla", style: .destructive) { (action) in
                let registro = self.arrAceptados![indexPath.row] as! NSDictionary
                let grupo = registro["group"] as! NSDictionary
                if let islaDefecto = grupo["is_own"] as? Bool {
                    if islaDefecto {
                        // Es la isla por default, no borrarla
                        self.mostrarMensajeInfo(mensaje: "No puedes abandonar tu isla por default", cerrar: false)
                    } else {
                        self.idIsla = grupo["id"] as? Int ?? -1
                        self.abandonarIsla()
                    }
                } else {
                    self.mostrarMensajeInfo(mensaje: "No puedes abandonar esta isla", cerrar: false)
                }
            }
            let cancelar = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            alerta.addAction(aceptar)
            alerta.addAction(cancelar)
            self.present(alerta, animated: true, completion: nil)
        }
    }
    
    func abandonarIsla() {
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        actEspera.startAnimating()
        // Enviar información al servidor
        //let alias = UserDefaults.standard.value(forKey: LLAVE_USUARIO) as! String
        let alias = KeychainWrapper.standard.string(forKey: LLAVE_USUARIO)!
        
        let direccion = URL_BORRAR_MIEMBRO.replacingOccurrences(of: ":id", with: "\(self.idIsla!)")
        //print(direccion)
        let url = URL(string: direccion + "/\(alias)")
        var request = URLRequest(url: url!)
        request.httpMethod = "DELETE"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.actEspera.stopAnimating()
            }
            if error == nil {
                let respuesta = response! as! HTTPURLResponse
                //print("Respuesta DELETE: \(respuesta)")
                if respuesta.statusCode == OK_BORRAR_MIEMBRO {
                    
                    self.mostrarMensajeInfo(mensaje: "Has abandonado la isla de manera exitosa", cerrar: false)
                    //UserDefaults.standard.string(forKey: LLAVE_ID_ISLA)
                    UserDefaults.standard.set("-1", forKey: LLAVE_ID_ISLA)
                    
                    DispatchQueue.main.async {
                        self.cargarInvitaciones()
                    }
                } else if respuesta.statusCode == ERROR_BORRAR_MIEMBRO_ACTIVO {
                    self.mostrarMensajeInfo(mensaje: "No puedes abandonar la isla porque hay un plan de seguridad activo con tu alias", cerrar: false)
                } else if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                    self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                } else {
                    self.mostrarMensajeInfo(mensaje: "Error en el servidor", cerrar: false)
                }
            } else {
                let tipoError = error as NSError?
                if tipoError!.code == ERROR_CONEXION_INTERNET {
                    self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                } else {
                    self.mostrarMensajeInfo(mensaje: "Error en el servicios, intente más tarde", cerrar: false)
                }
            }
        }
        task.resume()
    }
    
    func enviarAceptaInvitacion(_ id: Int) {
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        actEspera.startAnimating()
        // Enviar información al servidor
        let url = URL(string: URL_ACEPTA_INVITACION + "/\(id)")
        var request = URLRequest(url: url!)
        request.httpMethod = "PATCH"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        request.httpBody = """
            {
            "pending": false
            }
            """.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.actEspera.stopAnimating()
            }
            if error == nil {
                let respuesta = response! as! HTTPURLResponse
                //print("Respuesta INVITACIOn acepta: \(respuesta)")
                if respuesta.statusCode == OK_ACEPTA_INVITACION {
                    if let datosBin = data, let _ = String(data: datosBin, encoding: .utf8) {
                        // Procesar el diccionario de respuesta
                        self.mostrarMensajeInfo(mensaje: "Registro aceptado", cerrar: false)
                        DispatchQueue.main.async {
                            self.cargarInvitaciones()
                        }
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error en los datos json", cerrar: false)
                    }
                } else if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                    self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                } else {
                    self.mostrarMensajeInfo(mensaje: "Error en los datos del usuario", cerrar: false)
                }
            } else {
                let tipoError = error as NSError?
                if tipoError!.code == ERROR_CONEXION_INTERNET {
                    self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                } else {
                    self.mostrarMensajeInfo(mensaje: "Error en la red, intente más tarde", cerrar: false)
                }
            }
        }
        task.resume()
    }
}
