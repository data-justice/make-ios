//
//  MuestraPlanGrafico.swift
//  MKE
//
//  Created by Roberto Mtz. Román on 27/01/20.
//  Copyright © 2020 Roberto Martinez Roman. All rights reserved.
//

import UIKit

class MuestraPlanGrafico: UIViewController, UIScrollViewDelegate
{
    
    @IBOutlet weak var lblPersonaAusente: UILabel!
    @IBOutlet weak var imgPlanGrafico: UIImageView!
    
    @IBOutlet weak var scroll: UIScrollView!
    
    var imgPlan: UIImage!
    var personaAusente: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if imgPlan != nil {
            imgPlanGrafico.image = imgPlan
        }
        
        if personaAusente != nil {
            lblPersonaAusente.text = "Plan de seguridad para\n\(personaAusente!)"
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        scroll.zoomScale = scroll.frame.size.height/imgPlanGrafico.frame.size.height
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imgPlanGrafico
    }
    
    
    @IBAction func compartirPlanGrafico(_ sender: UIButton) {
        
        if let imagen = self.imgPlan {
            let alertaCompartir = UIActivityViewController(activityItems: [imagen], applicationActivities: nil)
            alertaCompartir.setValue("Plan de seguridad", forKey: "subject")
            alertaCompartir.popoverPresentationController?.sourceView = self.imgPlanGrafico
            self.present(alertaCompartir, animated: true)
        }
    }
}
