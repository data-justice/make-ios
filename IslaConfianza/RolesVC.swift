//
//  RolesVC.swift
//  MKE
//
//  Created by Roberto Mtz. Román on 8/20/19.
//  Copyright © 2019 Roberto Martinez Roman. All rights reserved.
//

import UIKit

class RolesVC: UIViewController
{
    let ROL_PERSONALIZADO = "Otro rol"
    
    let colorSeleccionado = UIColor(displayP3Red: 0.9, green: 1, blue: 0.9, alpha: 1)
    
    // Se descargan desde la red
    var arrMiembros: [String] = [String]()
    // Para actualizar el plan de seguridad
    //var arrIdMiembros: [Int] = [Int]()
    var arrRoles = [Int : String]() // 2: "Negociación", 3: "rol"
    // 0,1,2 en cada plan [0, 1, 1, 0, 2]
    var indicesGobernanza: [Int]!
    
    // Recibe los datos del controlador previo
    var dMembership: NSDictionary!
    
    // Isla de confianza que configura (se asigna desde el controlador previo)
    var nombreIsla: String! = "No Existe"
    var idIsla: Int! = nil
    
    // Para actualizar el plan de seguridad
    var arrPlanId = [Int]()
    
    //  Define por primera vez el plan o está actualizando
    var definiendoPlan = true
    
    // Diccionario con los datos de la isla
    var dIsla: NSDictionary!
    
    // Alias del usuario loggeado
    var alias: String = ""
    
    // Miembro para el cual está definiendo roles
    var indiceMiembroSeleccionado = 0
    // Diccionario de >>"miembroAusente": [1, 5, 2]<<
    var diccionarioRoles: [String:[Int]] = [String:[Int]]()
    
    // Cadenas de modelo de gobernanza
    let strModeloGobernanza = ["democratic", "centralized", "weighted"]
    
    // ID's de roles, porque no corresponden con los índices del arreglo
    var dIdRoles: [Int:Int] = [Int:Int]()   // indiceArr:IdRol
    
    @IBOutlet weak var segModeloGobernanza: UISegmentedControl!
    @IBOutlet weak var tablaMiembros: UICollectionView!
    @IBOutlet weak var tablaRoles: UITableView!
    
    @IBOutlet weak var actEspera: UIActivityIndicatorView!
    @IBOutlet weak var tfNombreIsla: UITextField!
    
    var alerta: UIAlertController!
    
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //print("Recibe: \(dMembership!)")
        
        tablaRoles.backgroundColor = colorSeleccionado
        mostrarNombreIsla() // y carga información de los miembros
        
        //self.alias = UserDefaults.standard.string(forKey: LLAVE_USUARIO) ?? "anónimo"
        if let alias = KeychainWrapper.standard.string(forKey: LLAVE_USUARIO) {
            self.alias = alias
        }
        
        self.title = "Roles"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tablaMiembros.flashScrollIndicators()
    }
    
    // Y CARGA INFORMACION DE MIEMBROS-ROLES
    func mostrarNombreIsla() {
        DispatchQueue.main.async {
            self.actEspera.startAnimating()
        }
        if dMembership != nil {
            if let grupo = dMembership["group"] as? NSDictionary {
                let isla = grupo["name"] as? String ?? "Desconocida"
                self.tfNombreIsla.text = isla
                self.nombreIsla = isla
                self.idIsla = grupo["id"] as? Int ?? -1
                self.cargarMiembrosServidor(self.idIsla)
            }
        } else {
            self.tfNombreIsla.text = "Desconocida"
        }
    }
    
    func cargarMiembrosServidor(_ id: Int)  {
        // Token para authorization
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        //var token = "noexiste"
        //let token = KeychainWrapper.standard.string(forKey: LLAVE_USUARIO) ?? "noExiste"
        
        // Enviar información al servidor, recibe token
        var dirServicio = URL_TEMPLATE_ROLES
        dirServicio = dirServicio.replacingOccurrences(of: ":id", with: "\(id)")
        let url = URL(string: dirServicio)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-type")
        request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.actEspera.stopAnimating()
            }
            if error == nil {   // No hay error
                if let response = response {
                    let respuesta = response as! HTTPURLResponse
                    if respuesta.statusCode == OK_TEMPLATE_ROLES {
                        if let body = String(data: data!, encoding: .utf8) {
                            //print("Body LISTA TEMPLATE: \(body)")
                            DispatchQueue.main.async {
                                if self.extraerMiembros(body) {
                                    // También extraer roles "available_roles"
                                    self.extraerRoles(body)
                                    self.cargarPlanSeguridad()  // Cargar el plan de seguridad
                                }
                            }
                        } else {
                            self.mostrarMensajeInfo(mensaje: "Error al solicitar roles al servidor", cerrar: false)
                        }
                    } else if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                        self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error al solicitar roles al servidor", cerrar: false)
                    }
                }
            } else {
                if let error = error {
                    //print("Error desconocido, \(error)")
                    let tipoError = error as NSError
                    if tipoError.code == ERROR_CONEXION_INTERNET {
                        self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error, al solicitar los roles", cerrar: false)
                    }
                }
            }
        }
        task.resume()
    }
    
    func cargarPlanSeguridad()  {
        self.actEspera.startAnimating()
        // Token para authorization
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        // Enviar información al servidor
        var dirServicio = URL_LISTA_PLANES_DE_SEGURIDAD
        dirServicio = dirServicio.replacingOccurrences(of: ":id", with: "\(self.idIsla!)")
        let url = URL(string: dirServicio)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-type")
        request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.actEspera.stopAnimating()
            }
            if error == nil {   // No hay error
                if let response = response {
                    let respuesta = response as! HTTPURLResponse
                    if respuesta.statusCode == OK_LISTA_PLANES_SEGURIDAD {
                        if let body = String(data: data!, encoding: .utf8) {
                            //print("Body LISTA PLANES: \(body)")
                            self.extraerRolesPrevios(body)
                        } else {
                            self.mostrarMensajeInfo(mensaje: "Error al solicitar islas", cerrar: false)
                        }
                    } else if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                        self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error al solicitar islas al servidor", cerrar: false)
                    }
                }
            } else {
                if let error = error {
                    //print("Error desconocido, \(error)")
                    let tipoError = error as NSError
                    if tipoError.code == ERROR_CONEXION_INTERNET {
                        self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error, al solicitar los roles", cerrar: false)
                    }
                }
            }
        }
        task.resume()
    }
    
    // TODO: Revisar
    func extraerRolesPrevios(_ body: String) {
        if let arrPlanes = try? (JSONSerialization.jsonObject(with: body.data(using: .utf8)!, options: .mutableContainers) as! NSArray)  {
            
            // CREAR diccionario inverso de [RolID:indiceArr]
            var dIdRolesInverso = [Int:Int]()
            for llave in self.dIdRoles.keys {
                dIdRolesInverso[self.dIdRoles[llave]!] = llave
            }
            //print("ID inverso: \(dIdRolesInverso)")
            // Procesar para cada persona ausente
            var arrMiembrosAusentes = [String]()
            for iPlan in arrPlanes {
                let plan = iPlan as! NSDictionary
                // Guardar el plan ID para actualizar después
                //arrPlanId.append(plan.value(forKey: "id") as! Int)
                let personaAusente = plan.value(forKey: "missing_person") as? String ?? nil
                if personaAusente != nil && personaAusente != self.alias {
                    arrPlanId.append(plan.value(forKey: "id") as! Int)
                    // Agregar personaAusente al arreglo de Alias (tabala horizontal)
                    arrMiembrosAusentes.append(personaAusente!)
                    //print("arrMiembrosAusentes: \(arrMiembrosAusentes)")
                    //print("arrPlanID: \(arrPlanId)")
                    //print("Para \(personaAusente!), \(self.alias) tiene los roles: ")
                    let dRoles = plan.value(forKey: "members_roles") as? NSDictionary ?? [:]
                    let arrRoles = dRoles.value(forKey: self.alias) as? NSArray ?? []
                    //print("roles: \(arrRoles)")
                    var arrRolesAlias = [Int]()
                    for rol in arrRoles {
                        let dRol = rol as! NSDictionary
                        //arrRolesAlias.append(dRol.value(forKey: "role_id") as! Int - 1)
                        let idRol = dRol.value(forKey: "role_id") as! Int
                        let idKey = dIdRolesInverso[idRol]!
                        arrRolesAlias.append(idKey)
                        //arrRolesAlias.append(self.dIdRoles[idKey]!)
                    }
                    //print("*** \(personaAusente!): \(arrRolesAlias)")
                    self.diccionarioRoles[personaAusente!] = arrRolesAlias
                    if arrRolesAlias.count > 0 {
                        definiendoPlan = false;
                    }
                    // Modelo de gobernanza
                    if let detalleGobernanza = plan.value(forKey: "governance_details") as? NSDictionary {
                        let detalleVotos = detalleGobernanza.value(forKey: "votes_details") as! NSDictionary
                        var indiceModelo = 0
                        var voto = detalleVotos.value(forKey: "centralized") as! NSArray
                        var votoAlias = voto.count > 0 ? voto[0] as! String : ""
                        if votoAlias == "\(self.alias)" {
                            indiceModelo = 1
                        }
                        voto = detalleVotos.value(forKey: "weighted") as! NSArray
                        votoAlias = voto.count > 0 ? voto[0] as! String : ""
                        if votoAlias == "\(self.alias)" {
                            indiceModelo = 2
                        }
                        //print("votó por \(self.strModeloGobernanza[indiceModelo])")
                        
                        //if let indice = self.arrMiembros.firstIndex(of: personaAusente!) {
                        if let indice = arrMiembrosAusentes.firstIndex(of: personaAusente!) {
                            self.indicesGobernanza[indice] = indiceModelo
                        }
                    }
                }
                
            }
            //print(self.diccionarioRoles)
            // Actualizar tablas
            DispatchQueue.main.async {
                self.segModeloGobernanza.selectedSegmentIndex = self.indicesGobernanza[self.indiceMiembroSeleccionado]
                self.tablaRoles.reloadData()
                self.arrMiembros = arrMiembrosAusentes
                self.tablaMiembros.reloadData()
            }
        }
    }
    
    
    @IBAction func cambiaModeloGobernanza(_ sender: UISegmentedControl) {
        if UserDefaults.standard.bool(forKey: "ModelosAceptado")==true {
            indicesGobernanza[indiceMiembroSeleccionado] = sender.selectedSegmentIndex
            self.grabarModelosGobernanza()
        } else {
            let alerta = UIAlertController(title: "Modelo de gobernanza", message: """
            ¿Qué son?
            Son formatos para la toma de decisiones. Cada isla de confianza decidirá la manera en que tomará decisiones. Existen 3 tipos:
            
            1. Democrático: Cada miembro de la isla de confianza cuenta con un solo voto.
            
            2. Ponderado: El número de votos de cada miembro dependerá de sus habilidades y/o grados de aportación ante una crisis, como un supuesto caso de secuestro.
            
            3. Centralizado: Una sola persona tomará las decisiones por toda la isla de confianza.
            """, preferredStyle: .alert)
            let aceptar = UIAlertAction(title: "Entiendo, no mostrar nuevamente", style: .default) { (action) in
                UserDefaults.standard.set(true, forKey: "ModelosAceptado")
                self.indicesGobernanza[self.indiceMiembroSeleccionado] = sender.selectedSegmentIndex
                self.grabarModelosGobernanza()
            }
            let cancelar = UIAlertAction(title: "Cancelar", style: .cancel) { (action) in
                UserDefaults.standard.set(false, forKey: "ModelosAceptado")
            }
            alerta.addAction(aceptar)
            alerta.addAction(cancelar)
            self.present(alerta, animated: true)
        }
    }
    
    func cargarTiposRolesServidor() {
        // Token para authorization
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        // Enviar información al servidor, recibe token
        let url = URL(string: URL_LISTA_ROLES)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-type")
        request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil {   // No hay error
                if let response = response {
                    let respuesta = response as! HTTPURLResponse
                    if respuesta.statusCode == OK_LISTA_ROLES {
                        if let body = String(data: data!, encoding: .utf8) {
                            //print("Body LISTA ROLES: \(body)")
                            self.extraerRoles(body)
                        } else {
                            self.mostrarMensajeInfo(mensaje: "Error al solicitar roles al servidor", cerrar: false)
                        }
                    } else if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                        self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error al solicitar roles al servidor", cerrar: false)
                    }
                }
            } else {
                if let error = error {
                    //print("Error desconocido, \(error)")
                    let tipoError = error as NSError
                    if tipoError.code == ERROR_CONEXION_INTERNET {
                        self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error, al solicitar los roles", cerrar: false)
                    }
                }
            }
        }
        task.resume()
    }
    
    func extraerRoles(_ body: String) {
        if let diccionarioRoles = try? (JSONSerialization.jsonObject(with: body.data(using: .utf8)!, options: .mutableContainers) as! NSDictionary)  {
            let arrRoles = diccionarioRoles["available_roles"] as! NSArray
            var indice = 0
            for dRol in arrRoles  {
                let rol = dRol as! [String:Any]
                let id = rol["id"] as! Int
                self.dIdRoles[indice] = id
                let nombre = rol["name"] as! String
                self.arrRoles[indice] = nombre
                indice += 1
            }
            // AGREGAR Otro rol
            self.arrRoles[indice] = ROL_PERSONALIZADO
            //print("Roles encontrados: \(self.arrRoles)")
            //print("ID de roles: \(self.dIdRoles)")
            DispatchQueue.main.async {
                self.tablaRoles.reloadData()
            }
        }
    }
    
    func extraerMiembros(_ body: String) -> Bool {
        var resultado = false
        if let diccionarioMiembros = try? (JSONSerialization.jsonObject(with: body.data(using: .utf8)!, options: .mutableContainers) as! [String:AnyObject])  {
            self.idIsla = diccionarioMiembros["group_id"] as? Int ?? -1
            if let arrMiembros = diccionarioMiembros["available_members"] as? [String] {
                //print("*** Miembros leidos: \(arrMiembros)")
                self.arrMiembros = arrMiembros
                self.indicesGobernanza = [Int](repeating: 0, count: arrMiembros.count)
                if self.arrMiembros.count > 0 {
                    resultado = true
                    DispatchQueue.main.async {
                        self.tablaMiembros.reloadData()
                    }
                } else {
                    // No puede definir roles
                    self.mostrarMensajeInfo(mensaje: "Los invitados a esta isla no han  confirmado su participación. No puedes definir roles.", cerrar: true)
                    resultado = false
                }
            }
        }
        return resultado
    }
    
    // Muestra un mensaje de información en la pantalla
    func mostrarMensajeInfo(mensaje: String, cerrar: Bool) {
        DispatchQueue.main.async {
            if self.alerta == nil {
                self.alerta = UIAlertController(title: "Aviso", message: mensaje, preferredStyle: .alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: { (alerta) in
                    self.alerta.dismiss(animated: true)
                    if mensaje == MENSAJE_CREDENCIALES_INVALIDAS {
                        self.navigationController?.popToRootViewController(animated: true)
                    } else
                        if cerrar {
                            self.navigationController?.popViewController(animated: true)
                    }
                })
                self.alerta.addAction(aceptar)
            } else {
                self.alerta.message = mensaje
            }
            if !self.alerta.isBeingPresented {
                self.present(self.alerta, animated: true, completion: nil)
            }
        }
    }
    
    
    @IBAction func enviarRolesUsuario(_ sender: UIButton) {
        // Crea cada diccionario y lo manda al servidor, para definir el plan de seguridad
        //print(diccionarioRoles)
        // VERIFICA que haya roles para todos los integrantes
        for indice in 0..<arrMiembros.count {
            let personaAusente = arrMiembros[indice]
            // Crea el arreglo de roles
            let roles = diccionarioRoles[personaAusente] ?? [Int]()
            if roles.count == 0  {
                mostrarMensajeInfo(mensaje: "Debes seleccionar roles para todos los escenarios", cerrar: false)
                return
            }
        }
        //print(arrMiembros)
        for indice in 0..<arrMiembros.count {
            let personaAusente =  arrMiembros[indice]
            if self.alias == personaAusente {
                continue
            }
            // Crea el arreglo de roles
            var roles = diccionarioRoles[personaAusente] ?? [Int]()
            
            var rolesID = [Int]()
            // Suma 1 a cada elemento de 'roles'
            for i in 0..<roles.count {
                rolesID.append(self.dIdRoles[roles[i]]!)
                
                roles[i] = roles[i]+1
            }
            //print("ID de roles: \(rolesID)")
            
            let gobernanza: Int = indicesGobernanza[indice]
            
            // PREGUNTAR si es DEFINICIÓN o ACTUALIZACIÓN
            if definiendoPlan {
                let body = """
                { "group": \(self.idIsla!),
                "missing_person": "\(personaAusente)",
                "roles": \(rolesID),
                "governance_model": "\(self.strModeloGobernanza[gobernanza])"
                }
                """
                // Enviar este body
                enviarRolesServidor(body, personaAusente)
            } else {
                let body = """
                {
                "roles": \(rolesID),
                "governance_model": "\(self.strModeloGobernanza[gobernanza])"
                }
                """
                // Enviar este body con el planID correcto
                enviarActualizacionRolesServidor(body, arrPlanId[indice])
            }
        }
    }
    
    func enviarRolesServidor(_ body: String, _ personaAusente: String) {
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        actEspera.startAnimating()
        // Enviar información al servidor
        let direccion = URL_CREA_PLAN_SEGURIDAD.replacingOccurrences(of: ":id", with: "\(self.idIsla!)")
        let url = URL(string: direccion)
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        request.httpBody = body.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.actEspera.stopAnimating()
            }
            if error == nil {
                let respuesta = response! as! HTTPURLResponse
                //print("Respuesta CREA PLAN: \(respuesta)")
                if respuesta.statusCode == OK_ACEPTA_INVITACION {
                    if let datosBin = data, let _ = String(data: datosBin, encoding: .utf8) {
                        //print("body CREA PLAN: \(body)")
                        self.mostrarMensajeInfo(mensaje: "Plan registrado con éxito para \(personaAusente)", cerrar: true)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error en los datos json", cerrar: false)
                    }
                } else if respuesta.statusCode == ERROR_CREAR_PLAN_SEGURIDAD {
                    self.mostrarMensajeInfo(mensaje: "Ya existe un plan de seguridad para \(personaAusente)", cerrar: false)
                } else if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                    self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                } else {
                    self.mostrarMensajeInfo(mensaje: "Error en los datos del usuario", cerrar: false)
                }
            } else {
                let tipoError = error as NSError?
                if tipoError!.code == ERROR_CONEXION_INTERNET {
                    self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                } else {
                    self.mostrarMensajeInfo(mensaje: "Error en la red, intente más tarde", cerrar: false)
                }
            }
        }
        task.resume()
    }
    
    func enviarActualizacionRolesServidor(_ body: String, _ planID: Int) {
        // PUT /user/groups/1/security_plans/5
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        actEspera.startAnimating()
        // Enviar información al servidor
        let direccion = URL_ACTUALIZA_PLAN_SEGURIDAD.replacingOccurrences(of: ":group_id", with: "\(self.idIsla!)").replacingOccurrences(of: ":plan_id", with: "\(planID)")
        let url = URL(string: direccion)
        //print("url: \(direccion) \n body: \(body)")
        var request = URLRequest(url: url!)
        request.httpMethod = "PUT"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        request.httpBody = body.data(using: .utf8)
        
        //print("Actualiza ROLES")
        //print(String(data: request.httpBody!, encoding: .utf8) ?? "no existen datos")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.actEspera.stopAnimating()
            }
            if error == nil {
                let respuesta = response! as! HTTPURLResponse
                /*if let datosBin = data, let _ = String(data: datosBin, encoding: .utf8) {
                 print("body ACTUAL PLAN: \(body)")
                 }*/
                if respuesta.statusCode == OK_ACEPTA_INVITACION {
                    if let datosBin = data, let _ = String(data: datosBin, encoding: .utf8) {
                        //print("body CREA PLAN: \(body)")
                        self.mostrarMensajeInfo(mensaje: "Plan actualizado con éxito para la isla \(self.nombreIsla!)", cerrar: true)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error al actualizar", cerrar: false)
                    }
                } else if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                    self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                } else {
                    self.mostrarMensajeInfo(mensaje: "Error en los datos del usuario", cerrar: false)
                }
            } else {
                let tipoError = error as NSError?
                if tipoError!.code == ERROR_CONEXION_INTERNET {
                    self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                } else {
                    self.mostrarMensajeInfo(mensaje: "Error en la red, intente más tarde", cerrar: false)
                }
            }
        }
        task.resume()
    }
}


extension RolesVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrRoles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celda = tableView.dequeueReusableCell(withIdentifier: "celdaRol", for: indexPath)
        
        celda.textLabel?.text = arrRoles[indexPath.row]
        var arrIndices = [Int]()
        if arrMiembros.count > 0 {
            arrIndices = diccionarioRoles[arrMiembros[indiceMiembroSeleccionado]] ?? [Int]()
        }
        
        let seleccionado = arrIndices.contains(indexPath.row)
        if seleccionado {
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
            celda.accessoryType = .checkmark
        } else {
            tableView.deselectRow(at: indexPath, animated: false)
            celda.accessoryType = .none
        }
        return celda
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let marcados = tableView.indexPathsForSelectedRows
        if marcados?.count ?? 0 <= 3 {
            let celda = tableView.cellForRow(at: indexPath)!
            celda.accessoryType = .checkmark
            // Es personalizado??
            if let texto = celda.textLabel?.text {
                if texto == ROL_PERSONALIZADO {
                    // Agregar nuevo rol
                    self.agregarNuevoRol()
                } else {
                    grabarRolesSeleccionados()
                }
            }
        } else {
            tableView.deselectRow(at: indexPath, animated: true)
            mostrarMensajeInfo(mensaje: "No puedes seleccionar más de 3 roles", cerrar: false)
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let celda = tableView.cellForRow(at: indexPath)!
        celda.accessoryType = .none
        grabarRolesSeleccionados()
    }
    
    func grabarRolesSeleccionados() {
        var arrIndices: [Int] = [Int]()
        
        let indexs = tablaRoles.indexPathsForSelectedRows
        if let indexs = indexs {
            for ip in indexs {
                arrIndices.append(ip.row)
            }
        }
        let miembro = arrMiembros[indiceMiembroSeleccionado]
        diccionarioRoles[miembro] = arrIndices
        UserDefaults.standard.set(diccionarioRoles, forKey: nombreIsla)
        
        grabarModelosGobernanza()
    }
    
    
    func grabarModelosGobernanza() {
        // Grabar modelo de gobernanza
        let modelos = indicesGobernanza
        //print("Guarda: \(modelos!)")
        UserDefaults.standard.set(modelos, forKey: "modelosGobernanza")
    }
    
    func agregarNuevoRol() {
        // Captura nuevo rol
        let alerta = UIAlertController(title: "Aviso", message: "Teclea el nuevo rol", preferredStyle: .alert)
        alerta.addTextField { (textField) in
            textField.placeholder = "Nuevo rol"
        }
        let aceptar = UIAlertAction(title: "Aceptar", style: .default) { (aceptarAction) in
            if let nuevoRol = alerta.textFields?[0].text {
                //print(nuevoRol)
                if !nuevoRol.isEmpty {
                    // Enviar nuevo rol y al regresar, actualizar tabla de roles
                    self.enviarCreaNuevoRol(nuevoRol)
                }
            }
        }
        let cancelar = UIAlertAction(title: "Cancelar", style: .cancel)
        alerta.addAction(aceptar)
        alerta.addAction(cancelar)
        present(alerta, animated: true)
    }
    
    func enviarCreaNuevoRol(_ nuevoRol: String) {
        self.actEspera.startAnimating()
        // Token para authorization
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        
        // Enviar información al servidor, recibe token
        let url = URL(string: URL_CREA_NUEVO_ROL)
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-type")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.httpBody = """
            {
            "name": "\(nuevoRol)",
            "group": \(idIsla!)
            }
            """.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil {
                DispatchQueue.main.async {
                    self.actEspera.stopAnimating()
                }
                if let response = response  {
                    //print("Response crea rol: \(response)")
                    let respuesta = response as! HTTPURLResponse
                    //print("Respuesta crea rol: \(respuesta)")
                    if respuesta.statusCode == OK_NUEVO_ROL_CREADO {
                        if data != nil {
                            //let body = String(data: data!, encoding: .utf8)
                            //print("Body reporte: \(body!)")
                            // Mensaje de éxito
                            self.mostrarMensajeInfo(mensaje: "Se ha creado con éxito nuevo rol", cerrar: false)
                            // Recargar roles ********************************************º
                            DispatchQueue.main.async {
                                self.mostrarNombreIsla()
                            }
                        }
                    } else if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                        self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error al crear el nuevo rol", cerrar: false)
                    }
                } else {
                    self.mostrarMensajeInfo(mensaje: "Error en el servidor, intente más tarde", cerrar: false)
                }
            } else {
                if let error = error {
                    //print("Error desconocido, \(error)")
                    let tipoError = error as NSError
                    if tipoError.code == ERROR_CONEXION_INTERNET {
                        self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error, al enviar tu información", cerrar: false)
                    }
                }
            }
            DispatchQueue.main.async {
                self.actEspera.stopAnimating()
            }
        }
        task.resume()
    }
}

extension RolesVC: UICollectionViewDataSource, UICollectionViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrMiembros.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let celda = collectionView.dequeueReusableCell(withReuseIdentifier: "celdaMiembro", for: indexPath) as! CeldaRolGrupo
        
        celda.texto.text = arrMiembros[indexPath.row]
        
        if indexPath.row == indiceMiembroSeleccionado {
            celda.vistaSombra.backgroundColor = colorSeleccionado
        } else {
            celda.vistaSombra.backgroundColor = UIColor.clear
        }
        
        return celda
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        indiceMiembroSeleccionado = indexPath.row
        tablaMiembros.reloadData()
        tablaRoles.reloadData()
        
        // Muestra modelo seleccionado
        segModeloGobernanza.selectedSegmentIndex = indicesGobernanza[indiceMiembroSeleccionado]
    }
}
