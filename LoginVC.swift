//
//  ViewController.swift
//  MKE
//
//  Created by Roberto Martínez Román on 2/14/19.
//  Copyright © 2019 Roberto Martinez Roman. All rights reserved.
//
// REBASE 14/Marzo/2019

import UIKit

class LoginVC: UIViewController
{
    @IBOutlet weak var activEspera: UIActivityIndicatorView!
    @IBOutlet weak var tfUsuario: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var btnEntrar: UIButton!
    
    // Para hacer login automático
    @IBOutlet weak var btnPanico: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let delegado = UIApplication.shared.delegate as? AppDelegate {
            delegado.controladorRaiz = self.navigationController
        }
        
        if let aliasGuardado = KeychainWrapper.standard.string(forKey: LLAVE_USUARIO) {
            self.tfUsuario.text = aliasGuardado
        } else {
            if let aliasGuardado = UserDefaults.standard.string(forKey: LLAVE_USUARIO) {
                self.tfUsuario.text = aliasGuardado
            }
        }
        
        if let passwordGuardado = KeychainWrapper.standard.string(forKey: LLAVE_PASSWORD) {
            self.tfPassword.text = passwordGuardado
        }
        
        /*
        let prefs = UserDefaults.standard
        if let aliasGuardado = prefs.string(forKey: LLAVE_USUARIO) {
            self.tfUsuario.text = aliasGuardado
        }
        if let passwordGuardado = prefs.string(forKey: LLAVE_PASSWORD) {
            self.tfPassword.text = passwordGuardado
        }
         */
        
       self.navigationController?.navigationBar.barTintColor = UIColor(named: "MoradoMake")
        self.navigationController?.navigationBar.tintColor = UIColor(named: "AmarilloMake")
        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor(named: "AmarilloMake")!]
        
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Esconde la navigation bar
        self.navigationController?.navigationBar.isHidden = true
        self.btnPanico.isUserInteractionEnabled = true
    }
    
    @IBAction func regresar(sender: UIStoryboardSegue) {
        
    }
    
    @IBAction func solicitaLogin(_ sender: UIButton) {
        
        self.view.endEditing(true)
        if tfUsuario.text == "" || tfPassword.text == "" {
            self.mostrarMensajeInfo(mensaje: "Debes llenar ambos campos", cerrar: false)
        } else {
            if let usuario = tfUsuario.text, let password = tfPassword.text {
                if sender == btnEntrar {    // NORMAL
                    btnEntrar.isEnabled = false
                    hacerLogin(usuario, password, entradaAutomatica: false)
                } else {        // AUTOMATICO
                    self.btnPanico.isUserInteractionEnabled = false 
                    hacerLogin(usuario, password, entradaAutomatica: true)
                }
            } else {
                self.mostrarMensajeInfo(mensaje: "Debes llenar ambos campos", cerrar: false)
            }
        }
    }
    
    // hacerLogin
    func hacerLogin(_ usuario: String, _ password: String, entradaAutomatica: Bool) {
        activEspera.startAnimating()
        // Enviar información al servidor, recibe token
        let url = URL(string: URL_SERVICIO_LOGIN)
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-type")
        request.httpBody = """
            { "user":
            {
            "username": "\(usuario)",
            "password": "\(password)"
            }
            }
            """.data(using: .utf8)
        
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.activEspera.stopAnimating()
                self.btnEntrar.isEnabled = true
            }
            if error == nil {
                let respuesta = response! as! HTTPURLResponse
                //print("Response LOGIN: \(response!)")
                if respuesta.statusCode == OK_LOGIN {
                    if let data = data, let body = String(data: data, encoding: .utf8) {
                        // Guardar el TOKEN y...
                        //self.mostrarPantallaMenu(body)
                        self.guardarDatosUsuario(body)
                        //hacer el segue a la siguiente pantalla
                        if entradaAutomatica {
                            self.mostrarPantallaBtnPanico()
                        } else {
                            self.mostrarPantallaMenu()
                        }
                    }
                } else if respuesta.statusCode == ERROR_INFO_INVALIDA {
                    if let data = data, let body = String(data: data, encoding: .utf8) {
                        //print("Body LOGIN  Error: \(body)")
                        let diccionario = try? JSONSerialization.jsonObject(with: body.data(using: .utf8)!, options: .mutableContainers) as? NSDictionary
                        let mensajeError = diccionario?.value(forKey: "error")! as? String ?? "Error en  tus datos"
                        self.mostrarMensajeInfo(mensaje: mensajeError, cerrar: false)
                    }
                } else {
                    self.mostrarMensajeInfo(mensaje: "Error, revisa tus datos e intenta de nuevo", cerrar: false)
                }
            } else {
                if let error = error {
                    //print("Error desconocido, \(error)")
                    let tipoError = error as NSError
                    if tipoError.code == ERROR_CONEXION_INTERNET {
                        self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error, al enviar tu solicitud", cerrar: false)
                    }
                }
            }
        }
        task.resume()
    }
    
    // Olvidó contraseña
    @IBAction func olvidoPassword(_ sender: UIButton) {
        guard let url = URL(string: URL_SERVICIO_RESTAURAR_PASSWORD)
            else { return }

        UIApplication.shared.open(url)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // Esconder teclado
        self.view.endEditing(true)
    }
    
    // Muestra un mensaje de información en la pantalla
    func mostrarMensajeInfo(mensaje: String, cerrar: Bool) {
        DispatchQueue.main.async {
            let alerta = UIAlertController(title: "Aviso", message: mensaje, preferredStyle: .alert)
            let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: { (alerta) in
                if cerrar {
                    self.dismiss(animated: true, completion: nil)
                }
            })
            alerta.addAction(aceptar)
            self.present(alerta, animated: true, completion: nil)
        }
    }
    
    func guardarDatosUsuario(_ datosToken: String) {
        guard let diccionario = try? JSONSerialization.jsonObject(with: datosToken.data(using: .utf8)!, options: .mutableContainers) as? NSDictionary else { return }
        
        // Guarda el token en las preferencias
        DispatchQueue.main.async {
            // ---
            KeychainWrapper.standard.set(self.tfUsuario.text!, forKey: LLAVE_USUARIO)
            KeychainWrapper.standard.set(self.tfPassword.text!, forKey: LLAVE_PASSWORD)
            // ---
            let token = diccionario[LLAVE_TOKEN]!
            let preferencias = UserDefaults.standard
            preferencias.setValue(token, forKey: LLAVE_TOKEN)
            //preferencias.setValue(self.tfUsuario.text, forKey: LLAVE_USUARIO)
            //preferencias.setValue(self.tfPassword.text, forKey: LLAVE_PASSWORD)
        }
    }
    
    func mostrarPantallaBtnPanico() {
        DispatchQueue.main.async {
            //self.navigationController?.navigationBar.isHidden = false
            self.performSegue(withIdentifier: "segueBtnPanico", sender: self)
        }
    }
    
    func mostrarPantallaMenu() {
        // Menú principal
        DispatchQueue.main.async {
            // Esconde la navigation bar
            //self.navigationController?.navigationBar.isHidden = true
            self.performSegue(withIdentifier: "segueMenuPrincipal", sender: self)
        }
    }
}

// Para evitar copiar/pegar en los campos de captura
extension UITextField {
    open override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
    
}

extension UITextView {
    open override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
}
