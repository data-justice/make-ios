//
//  MakeVC.swift
//  MKE
//
//  Created by Roberto Mtz. Román on 10/5/19.
//  Copyright © 2019 Roberto Martinez Roman. All rights reserved.
//

import UIKit

class MakeVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.hidesBackButton = true
        
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "MoradoMake")
        self.navigationController?.navigationBar.tintColor = UIColor(named: "AmarilloMake")
        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor(named: "AmarilloMake")!]
    }
    
    @objc func cerrarSesion() {
        
    }
}
