//
//  MenuVC.swift
//  MKE
//
//  Created by Roberto Martínez Román on 4/4/19.
//  Copyright © 2019 Roberto Martinez Roman. All rights reserved.
//

import UIKit

class MenuVC: UIViewController
{
    var idIsla: Int!
    
    @IBOutlet weak var actEspera: UIActivityIndicatorView!
    
    // Arreglo de objetos para la tabla
    var arrEventosTabla = Array<EventoCorto>()
    
    @IBOutlet weak var btnEventoComplemento: UIButton!
    @IBOutlet weak var btnEventoNuevo: UIButton!
    @IBOutlet weak var btnEventoEnCurso: UIButton!
    @IBOutlet weak var tablaIncidentes: UITableView!
    @IBOutlet weak var lblEventosRegistrados: UILabel!
    
    var arrIncidentes: NSMutableArray!
    
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        cargarEventos()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueBotonPanico" || segue.identifier == "segueEventoEnCurso" {
            return
        }
        let reporteVC = segue.destination as! ReporteVC
        if let btn = sender as? UIButton {  // pide nuevo/complemento
            if btn == btnEventoNuevo {
                reporteVC.tipoEvento = .nuevo
            } else if btn == btnEventoComplemento {
                reporteVC.tipoEvento = .complemento
                // Buscar el índice
                if let indice = self.tablaIncidentes.indexPathForSelectedRow?.row {
                    let eventoBuscar = self.arrEventosTabla[indice]
                    let indiceAgregar = calcularIdBorrar(eventoBuscar)
                    
                    reporteVC.diccionarioDatosEvento = arrIncidentes![indiceAgregar.indiceBorrar] as? Dictionary<String, Any>
                }
            }
        } else if (sender as? UITableViewCell) != nil {  // Consulta directa (i) del COMPLEMENTO
            // Buscar el índice
            if let indice = self.tablaIncidentes.indexPathForSelectedRow?.row {
                reporteVC.tipoEvento = .consultaComplemento
                // Buscar en arrIncidentes, el índice del objeto con este id (eventoBuscar.idEvento)
                let eventoBuscar = self.arrEventosTabla[indice]
                if eventoBuscar.tipo == .principal {
                    let indiceAgregar = calcularIdBorrar(eventoBuscar)
                    reporteVC.diccionarioDatosEvento = self.arrIncidentes![indiceAgregar.indiceBorrar] as? Dictionary<String, Any>
                } else {
                    // Buscar complemento
                    let diccionarioConsulta = self.buscarRegistroComplemento(eventoBuscar)
                    reporteVC.diccionarioDatosEvento = diccionarioConsulta
                }
            }
        } else if let vc = sender as? MenuVC {  // Accesorio para consultar detalle de evento
            if vc == self {
                // Buscar el índice
                if let indice = self.tablaIncidentes.indexPathForSelectedRow?.row {
                    reporteVC.tipoEvento = .consulta
                    // Buscar en arrIncidentes, el índice del objeto con este id (eventoBuscar.idEvento)
                    let eventoBuscar = self.arrEventosTabla[indice]
                    let indiceAgregar = calcularIdBorrar(eventoBuscar)
                    reporteVC.diccionarioDatosEvento = self.arrIncidentes![indiceAgregar.indiceBorrar] as? Dictionary<String, Any>
                }
                reporteVC.tipoEvento = .consulta
            }
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        //print("haciendo segue")
        if let btn = sender as? UIButton {
            if btn == btnEventoNuevo || btn == btnEventoEnCurso {
                return true
            }
            if btn == btnEventoComplemento {
                if tablaIncidentes.indexPathForSelectedRow != nil {
                    if arrEventosTabla.count > 0 {
                        return true
                    }
                } else {
                    mostrarMensajeInfo(mensaje: "Debes seleccionar un evento previo para crear el complemento", cerrar: false)
                    return false
                }
            }
        } else if (sender as? UITableViewCell) != nil {
            // Seleccionó una celda
            if let indice = self.tablaIncidentes.indexPathForSelectedRow?.row {
                let eventoBuscar = self.arrEventosTabla[indice]
                if eventoBuscar.tipo == .principal {
                    return false
                } else if eventoBuscar.tipo == .complemento {
                    return true
                }
            }
        }
        
        if identifier == "segueBotonPanico" {
            return true
        }
        
        return false
    }
    
    func buscarRegistroComplemento(_ evento: EventoCorto) -> Dictionary<String, Any> {
        for dIncidente in self.arrIncidentes! {
            let incidentePrincipal = dIncidente as! NSMutableDictionary
            // Busca entre sus complementos
            if let dComplementos = incidentePrincipal["complements"] as? NSArray {
                for dComplemento in dComplementos {
                    let complemento = dComplemento as! NSDictionary
                    //print("complemento = \(complemento)")
                    let idEvento = complemento["id"]! as! Int
                    let id = "\(idEvento)"
                    if evento.idEvento == id {
                        let tituloCorto = complemento["title"]! as! String
                        let descripcion = complemento["description"] as! String
                        let fecha = complemento["date"]! as! String
                        let hora = complemento["time"]! as! String
                        let anexos = complemento["attachments"] as! NSArray
                        
                        incidentePrincipal["title"] = tituloCorto
                        incidentePrincipal["description"] = descripcion
                        incidentePrincipal["date"] = fecha
                        incidentePrincipal["time"] = hora
                        incidentePrincipal["attachments"] = anexos
                        return incidentePrincipal as! Dictionary<String, Any>
                    }
                }
            }
        }
        
        return [:]
    }
    
    func cargarEventos() {
        actEspera.startAnimating()
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        // Enviar información al servidor
        let direccion = URL_GRUPO_INCIDENTES.replacingOccurrences(of: ":id", with: "\(self.idIsla!)")
        //print("Solicita eventos de la isla:\n \(direccion)")
        
        let url = URL(string: direccion)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-type")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")

        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.actEspera.stopAnimating()
            }
            if let response = response {
                //print("Response INCIDENTES: \(response)")
                let respuesta = response as! HTTPURLResponse
                if respuesta.statusCode == OK_INCIDENTES {
                    if let datosBin = data, let body = String(data: datosBin, encoding: .utf8) {
                        //print("BODY GRUPO: \(body)")
                        // Procesar el arreglo de diccionarios
                        self.arrIncidentes = try? JSONSerialization.jsonObject(with: body.data(using: .utf8)!, options: .mutableContainers) as? NSMutableArray
                        //print("Arreglo de incidentes: \(self.arrIncidentes!)")
                        self.crearArregloIncidentes()
                        DispatchQueue.main.async {
                            self.tablaIncidentes.reloadData()
                        }
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error en los datos json", cerrar: false)
                    }
                } else if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                    self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                }  else {
                    self.mostrarMensajeInfo(mensaje: "Error en los datos del usuario", cerrar: false)
                }
            } else {
                if let error = error {
                    //print("Error desconocido, \(error)")
                    let tipoError = error as NSError
                    if tipoError.code == ERROR_CONEXION_INTERNET {
                        self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error en la red, intente más tarde", cerrar: false)
                    }
                }
            }
        }
        task.resume()
    }
 
    // Crea el arreglo de eventos con info corta a partir del arreglo de incidentes
    func crearArregloIncidentes() {
        self.arrEventosTabla.removeAll()
        for dIncidente in self.arrIncidentes! {
            let incidente = dIncidente as! NSDictionary
            let tituloCorto = incidente["title"]! as! String
            let fecha = incidente["date"]! as! String
            let idEvento = incidente["id"]! as! Int
            let tipo = TipoRegistro.principal
            let alias = incidente["user"] as! String
            let eventoPrincipal = EventoCorto(titulo: tituloCorto, fecha: fecha, idEvento: idEvento, tipo: tipo, alias: alias)
            self.arrEventosTabla.append(eventoPrincipal)
            // Procesa complementos
            if let dComplementos = incidente["complements"] as? NSArray {
                for dComplemento in dComplementos {
                    let complemento = dComplemento as! NSDictionary
                    let tituloCorto = complemento["title"]! as! String
                    let fecha = complemento["date"]! as! String
                    let idEvento = complemento["id"]! as! Int
                    let tipo = TipoRegistro.complemento
                    let eventoComplemento = EventoCorto(titulo: tituloCorto, fecha: fecha, idEvento: idEvento, tipo: tipo)
                    self.arrEventosTabla.append(eventoComplemento)
                }
            }
        }
    }
    
    // BORRAR REGISTRO
    func borrarRegistro(_ datosBorrar: (idBorrar: Int, indiceBorrar: Int)) {
        //print("Borrar: \(datosBorrar)")
        let idBorrar = datosBorrar.0
        let indiceBorrar = datosBorrar.1
        
        self.actEspera.startAnimating()
        
        // Token para authorization
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        // Enviar información al servidor, recibe token
        // 404 no existe, 204 ok
        let url = URL(string: URL_SERVICIO_INCIDENTE_BORRAR + "\(idBorrar)/")
        var request = URLRequest(url: url!)
        request.httpMethod = "DELETE"
        request.setValue("application/json", forHTTPHeaderField: "Content-type")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if error == nil {
                if let response = response {
                    let respuesta = response as! HTTPURLResponse
                    //print(respuesta)
                    if respuesta.statusCode == OK_REGISTRO_BORRADO {
                        if data != nil {
                            // Mensaje de éxito
                            self.mostrarMensajeInfo(mensaje: "Se ha borrado con éxito el reporte del evento", cerrar: false)
                            self.arrEventosTabla.remove(at: indiceBorrar)
                            self.borrarEventoIncidentes(idBorrar)   // Quita del arreglo descargado de la red este registro
                            self.recargarDatosTabla()
                        } else {
                            self.mostrarMensajeInfo(mensaje: "Error en los datos del usuario", cerrar: false)
                        }
                    } else if respuesta.statusCode == ERROR_REGISTRO_NO_EXISTE {
                        self.mostrarMensajeInfo(mensaje: "Este registro no existe, no se puede borrar", cerrar: false)
                    } else if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                        self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                    }
                    else {
                        self.mostrarMensajeInfo(mensaje: "Error en el servidor, intente más tarde", cerrar: false)
                    }
                }
            } else {
                if let error = error {
                    let tipoError = error as NSError
                    if tipoError.code == ERROR_CONEXION_INTERNET {
                        self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error, al enviar tu información", cerrar: false)
                    }
                }
            }
            DispatchQueue.main.async {
                self.actEspera.stopAnimating()
            }
        }
        task.resume()
    }
    
    func borrarEventoIncidentes(_ id: Int) {
        for i in  0..<arrIncidentes.count {
            let incidente = arrIncidentes[i] as! NSDictionary
            let idIncidente = incidente["id"] as! Int
            if id == idIncidente {
                arrIncidentes.remove(incidente)
                return
            }
        }
    }
    
    // Muestra un mensaje de información en la pantalla
    func mostrarMensajeInfo(mensaje: String, cerrar: Bool) {
        DispatchQueue.main.async {
            let alerta = UIAlertController(title: "Aviso", message: mensaje, preferredStyle: .alert)
            let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: { (alerta) in
                if mensaje == MENSAJE_CREDENCIALES_INVALIDAS {
                    self.navigationController?.popToRootViewController(animated: true)
                } else
                if cerrar {
                    self.dismiss(animated: true, completion: nil)
                }
            })
            alerta.addAction(aceptar)
            self.present(alerta, animated: true, completion: nil)
        }
    }
}

// DataSource para desplegar los incidentes del usuario
extension MenuVC: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrEventosTabla.count == 0 {
            self.lblEventosRegistrados.text = "No tienes eventos registrados"
        } else {
            self.lblEventosRegistrados.text = "Eventos registrados"
        }
        return arrEventosTabla.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celda = tableView.dequeueReusableCell(withIdentifier: "celdaEvento", for: indexPath)
        
        let evento = self.arrEventosTabla[indexPath.row]
        let titulo = evento.tituloCorto
        let fecha = evento.fecha
        let alias = evento.alias
        
        if evento.tipo == .complemento {
            celda.textLabel?.text = "➣ " + titulo
            celda.textLabel?.textColor = UIColor(displayP3Red: 0, green: 0.4, blue: 1, alpha: 1)
            celda.accessoryType = .none
            celda.detailTextLabel?.text = fecha
        }
        else {
            celda.textLabel?.text = titulo
            celda.textLabel?.textColor = UIColor.black
            celda.accessoryType = .detailButton
            celda.detailTextLabel?.text = "\(alias), \(fecha)"
        }

        return celda
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let eventoBuscar = self.arrEventosTabla[indexPath.row]
            var indiceAgregar = calcularIdBorrar(eventoBuscar)
            indiceAgregar.indiceBorrar = indexPath.row
            
            borrarRegistro(indiceAgregar)
        }
    }
    
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
        // Mostrar el detalle del registro
        performSegue(withIdentifier: "segueConsulta", sender: self)
    }
    
    func recargarDatosTabla()  {
        // Reconstruir arrEventosTabla
        self.crearArregloIncidentes()
        
        DispatchQueue.main.async {
            self.tablaIncidentes.reloadData()
        }
    }
    
    // Buscar en arrIncidentes el id de este título
    func calcularIdBorrar(_ evento: EventoCorto) -> (idBorrar: Int, indiceBorrar: Int) {
        for i in  0..<arrIncidentes.count {
            let incidente = arrIncidentes[i] as! NSDictionary
            let idIncidente = "\(incidente["id"] as! Int)"
            if evento.idEvento == idIncidente && evento.tipo == .principal {
                let id = incidente["id"] as! Int
                return (id, i)
            }
        }
        return (-1, 0)
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Borrar"
    }
    
    func tableView(_ tableView: UITableView, indentationLevelForRowAt indexPath: IndexPath) -> Int {
        if arrEventosTabla[indexPath.row].tipo == .complemento {
            return 3
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if arrEventosTabla[indexPath.row].tipo == .complemento {
            return false
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true
    }
}

