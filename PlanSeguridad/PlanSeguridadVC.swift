//
//  PlanSeguridadVC.swift
//  MKE
//
//  Created by Roberto Martínez Román on 8/13/19.
//  Copyright © 2019 Roberto Martinez Roman. All rights reserved.
//

import UIKit

class PlanSeguridadVC: UIViewController
{
    var arrAceptados: [[String:AnyObject]]! = [[String:AnyObject]]()
    
    var idIslaSeleccionada: Int! = nil
    var dIslaSeleccionada: [String:AnyObject]!
    
    @IBOutlet weak var btnSeleccionaIsla: UIButton!
    @IBOutlet weak var tfIslaSeleccionada: UILabel!
    @IBOutlet weak var actEspera: UIActivityIndicatorView!
    
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        btnSeleccionaIsla.contentHorizontalAlignment = .center
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Recuperar la última isla seleccionada y DESCARGAR EL DICCIONARIO dIslaSeleccionada
        //{{SERVER}}/user/groups/19,  URL_LISTA_GRUPOS
        if let idIsla = UserDefaults.standard.string(forKey: LLAVE_ID_ISLA) {
            if idIsla != "-1" {
                // Descargar dIslaSeleccionada
                self.idIslaSeleccionada = Int(idIsla)
                descargarDIsla()
            } else {
                self.tfIslaSeleccionada.text = "No seleccionada"
            }
        }
    }
    
    // Descarga los datos de la última isla seleccionada
    func descargarDIsla() {
        self.actEspera.startAnimating()
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        // Enviar información al servidor
        let url = URL(string: "\(URL_LISTA_GRUPOS)/\(self.idIslaSeleccionada!)")

        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.actEspera.stopAnimating()
            }
            if error == nil {
                let respuesta = response! as! HTTPURLResponse
                //print("Respuesta GRUPO: \(respuesta)")
                if respuesta.statusCode == OK_LISTA_GRUPOS {
                    if let datosBin = data, let body = String(data: datosBin, encoding: .utf8) {
                        // Buscar y mostrar miembros
                        let dGrupos = try? (JSONSerialization.jsonObject(with: body.data(using: .utf8)!, options: .mutableContainers) as! NSDictionary)
                        //print("Descarga ULTIMA isla: \(body)")
                        self.dIslaSeleccionada = dGrupos as? [String:AnyObject]
                        DispatchQueue.main.async {
                            if let nombreIsla = self.dIslaSeleccionada["name"] as? String {
                                self.tfIslaSeleccionada.text = nombreIsla
                            }
                        }
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error en los datos", cerrar: false)
                    }
                } else if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                    self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                } else {
                    self.idIslaSeleccionada = nil
                    DispatchQueue.main.async {
                        self.tfIslaSeleccionada.text = "No seleccionada"
                    }
                }
            } else {
                let tipoError = error as NSError?
                if tipoError!.code == ERROR_CONEXION_INTERNET {
                    self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                } else {
                    self.mostrarMensajeInfo(mensaje: "Error en la red, intente más tarde", cerrar: false)
                }
                
            }
        }
        task.resume()
    }
    
    @IBAction func crearIsla(_ sender: UIButton) {
        //UserDefaults.standard.set(false, forKey: "infoAceptada")
        let infoAceptada = UserDefaults.standard.bool(forKey: "infoAceptada")
        
        if infoAceptada {
            // Directo a crear la Isla
            performSegue(withIdentifier: "segueCrearIslaDirecto", sender: self)
        } else {
            // Info de la Isla de confianza
            let alerta = UIAlertController(title: "Aviso", message: """
            Descripción de una Isla de Confianza.

            Círculo de personas, familiares, amigos o colegas, que pueden apoyar o accionar de manera inmediata ante una crisis, como en un supuesto caso de secuestro.

            """, preferredStyle: .alert)
            let aceptar = UIAlertAction(title: "Entiendo, no mostrar nuevamente", style: .default) { (action) in
                UserDefaults.standard.set(true, forKey: "infoAceptada")
                            //if self.shouldPerformSegue(withIdentifier: "segueRoles", sender: self) {
                self.performSegue(withIdentifier: "segueCrearIslaDirecto", sender: self)
                            //}
                }
            let cancelar = UIAlertAction(title: "Cancelar", style: .cancel) { (action) in
                UserDefaults.standard.set(false, forKey: "infoAceptada")
            }
            alerta.addAction(aceptar)
            alerta.addAction(cancelar)
            self.present(alerta, animated: true)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueCrearIslaDirecto" {
            let vc = segue.destination as! CreaIslaConfianzaVC
            vc.planSeguridadVC = self
        } else if segue.identifier == "segueEstado" {
            let vc = segue.destination as! EstadoDeIslaVC
            vc.nombreIsla = self.tfIslaSeleccionada.text
            vc.idIsla = self.idIslaSeleccionada
        } else if segue.identifier == "segueVotaciones" {
            let vc = segue.destination as! VotacionesVC
            vc.idIsla = self.idIslaSeleccionada
            vc.dIsla = self.dIslaSeleccionada["group"] as? [String : AnyObject] ?? [String:AnyObject]()
        } else if segue.identifier == "segueRegistroEventos" {
            let vc = segue.destination as! MenuVC
            vc.idIsla = self.idIslaSeleccionada
        }
        let btnBack = UIBarButtonItem()
        btnBack.title = ""
        navigationItem.backBarButtonItem = btnBack
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if tfIslaSeleccionada.text! == "No seleccionada" && (identifier == "segueVotaciones" || identifier == "segueEstado" || identifier == "segueRegistroEventos" )  {
            mostrarMensajeInfo(mensaje: "Selecciona una isla antes de usar esta opción", cerrar: false)
            return false
        }
        return true
    }
    
    @IBAction func seleccionarIsla(_ sender: UIButton) {
        cargarInvitaciones()
    }
    
    func cargarInvitaciones() {
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        actEspera.startAnimating()
        // Enviar información al servidor
        let url = URL(string: URL_LISTA_MEMBERSHIPS)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.actEspera.stopAnimating()
            }
            if error == nil {
                let respuesta = response! as! HTTPURLResponse
                //print("Respuesta GRUPOS islas: \(respuesta)")
                if respuesta.statusCode == OK_LISTA_GRUPOS {
                    if let datosBin = data, let body = String(data: datosBin, encoding: .utf8) {
                        let dGrupos = try? (JSONSerialization.jsonObject(with: body.data(using: .utf8)!, options: .mutableContainers) as! [String:AnyObject])
                        // Llega un diccionario con llaves 'active'
                        self.arrAceptados = dGrupos?["active"]! as? [[String:AnyObject]] ?? [[:]]
                        //print("ISLAS activas--->>> \(self.arrAceptados!)")
                        DispatchQueue.main.async {
                            self.muestraMenuIslas()
                        }
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error en los datos", cerrar: false)
                    }
                } else if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                    self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                }  else {
                    self.mostrarMensajeInfo(mensaje: "Error en los datos del usuario", cerrar: false)
                }
            } else {
                let tipoError = error as NSError?
                if tipoError!.code == ERROR_CONEXION_INTERNET {
                    self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                } else {
                    self.mostrarMensajeInfo(mensaje: "Error en la red, intente más tarde", cerrar: false)
                }
            }
        }
        task.resume()
    }
    
    func muestraMenuIslas()  {
        
        if self.arrAceptados.count == 0 {
            // No hay, avisar y regresar
            self.mostrarMensajeInfo(mensaje: "No tienes islas en este momento", cerrar: false)
            return
        }
        
        // Mostrar alerta con islas en arrAceptados
        let alerta = UIAlertController(title: "Mis islas", message: "Selecciona una isla", preferredStyle: .actionSheet)
        // Agregar opciones
        //print("TODOS \(self.arrAceptados!)")
        for dMembership in self.arrAceptados  {
            let dIsla = dMembership["group"] as! [String:AnyObject]
            let nombre = dIsla["name"] as! String
            let idIsla = dIsla["id"] as! Int
            let  accion = UIAlertAction(title: nombre, style: .default) { (accion) in
                self.idIslaSeleccionada = idIsla
                // Guarda self.idIslaSeleccionada para los eventos de pánico
                UserDefaults.standard.set("\(self.idIslaSeleccionada!)", forKey: LLAVE_ID_ISLA)
                
                DispatchQueue.main.async {
                    self.tfIslaSeleccionada.text = nombre
                }
                //print("ISLA SELECCIONADA \(dMembership)")
                self.dIslaSeleccionada = dMembership
                
            }
            alerta.addAction(accion)
        }
        let accion = UIAlertAction(title: "Cancelar", style: .cancel)
        alerta.addAction(accion)
        
        if ( UIDevice.current.userInterfaceIdiom  == UIUserInterfaceIdiom.pad )
        {
            if let ppOver = alerta.popoverPresentationController {
                ppOver.sourceView = self.btnSeleccionaIsla
                ppOver.sourceRect = self.btnSeleccionaIsla.frame
                self.present(alerta, animated: true)
            }
        } else {
            present(alerta, animated: true)
        }
    }
    
    // Muestra un mensaje de información en la pantalla
    func mostrarMensajeInfo(mensaje: String, cerrar: Bool) {
        DispatchQueue.main.async {
            let alerta = UIAlertController(title: "Aviso", message: mensaje, preferredStyle: .alert)
            let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: { (alerta) in
                if mensaje == MENSAJE_CREDENCIALES_INVALIDAS {
                    self.navigationController?.popToRootViewController(animated: true)
                } else if cerrar {
                    self.navigationController?.popViewController(animated: true)
                }
            })
            alerta.addAction(aceptar)
            self.present(alerta, animated: true, completion: nil)
        }
    }
}
