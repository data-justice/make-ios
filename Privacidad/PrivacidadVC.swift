//
//  PrivacidadVC.swift
//  MKE
//
//  Created by Roberto Martínez Román on 9/23/19.
//  Copyright © 2019 Roberto Martinez Roman. All rights reserved.
//

import UIKit
import WebKit

class PrivacidadVC: UIViewController, WKNavigationDelegate
{

    @IBOutlet weak var webAvisoPrivacidad: WKWebView!
    @IBOutlet weak var actEspera: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webAvisoPrivacidad.navigationDelegate = self
        cargarHTML()
    }

    func cargarHTML()  {
        let url = Bundle.main.url(forResource: "AvisoPrivacidad", withExtension: "html")
        let request = URLRequest(url: url!)
        webAvisoPrivacidad.load(request)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.actEspera.stopAnimating()
    }
}
