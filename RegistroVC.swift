//
//  RegistroVC.swift
//  MKE
//
//  Created by Roberto Martínez Román on 2/19/19.
//  Copyright © 2019 Roberto Martinez Roman. All rights reserved.
//

import UIKit

class RegistroVC: UIViewController
{
    // Outlets
    @IBOutlet weak var tfCorreo: UITextField!
    @IBOutlet weak var tfNombreUsuario: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfPasswordSegundo: UITextField!
    
    @IBOutlet weak var actEspera: UIActivityIndicatorView!
    
    // Acepta política de privacidad
    var aceptaPolitica = false
    @IBOutlet weak var imgAcepta: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        aceptaPolitica = UserDefaults.standard.bool(forKey: "AceptaPolitica")
        if aceptaPolitica {
            self.imgAcepta.image = UIImage(named: "check_1")!
        } else {
          self.imgAcepta.image = UIImage(named: "uncheck_1")!
        }
    }
    
    // MARK: -
    
    @IBAction func confirmaRegistro(_ sender: UIButton) {
        let correo = tfCorreo.text!
        let nombreUsuario = tfNombreUsuario.text!
        let password = tfPassword.text!
        let passwordSegundo = tfPasswordSegundo.text!
        
        if !validarCampos(correo, nombreUsuario, password, passwordSegundo) {
            mostrarMensajeInfo(mensaje: "Llene todos los campos para completar el registro", cerrar: false)
        } else if !validarPassword(password: password, passwordSegundo: passwordSegundo) {
            mostrarMensajeInfo(mensaje: "El password no coincide", cerrar: false)
        } else if !validarCorreo(correo: correo) {
            mostrarMensajeInfo(mensaje: "El formato del correo es incorrecto", cerrar: false)
        } else if !validarPolitica() {
            mostrarMensajeInfo(mensaje: "Debe aceptar la politica de privacidad antes de registrarse", cerrar: false)
        } else {
            enviarRegistro(correo, nombreUsuario, password, passwordSegundo)
            actEspera.startAnimating()
        }
        view.endEditing(true)
    }
    
    func validarPolitica() -> Bool {
        return self.aceptaPolitica
    }
    
    // Valida que los campos tengan información
    func validarCampos(_ correo: String, _ nombreUsuario: String, _ password: String, _ passwordSegundo: String) -> Bool {
        return !correo.isEmpty && !nombreUsuario.isEmpty && !password.isEmpty && !passwordSegundo.isEmpty
    }
    
    func validarPassword(password: String, passwordSegundo: String) -> Bool {
        return !password.isEmpty && !passwordSegundo.isEmpty && (password == passwordSegundo)
    }
    
    // Muestra un mensaje de información en la pantalla
    func mostrarMensajeInfo(mensaje: String, cerrar: Bool) {
        DispatchQueue.main.async {
            let alerta = UIAlertController(title: "Aviso", message: mensaje, preferredStyle: .alert)
            let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: { (alerta) in
                if cerrar {
                    self.dismiss(animated: true, completion: nil)
                }
            })
            alerta.addAction(aceptar)
            self.present(alerta, animated: true, completion: nil)
        }
    }
    
    func enviarRegistro(_ correo: String, _ nombreUsuario: String, _ password: String, _ passwordSegundo: String) {
        
        let url = URL(string: URL_SERVICIO_SIGN_UP)
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-type")
        request.httpBody = """
            {
            "username": "\(nombreUsuario)",
            "password": "\(password)",
            "password_confirmation": "\(passwordSegundo)",
            "email": "\(correo)"
            }
            """.data(using: .utf8)
        
        //print(request.httpBody!)
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.actEspera.stopAnimating()
            }
            if let response = response {
                //print(response)
                let respuesta = response as! HTTPURLResponse
                if respuesta.statusCode == OK_REGISTRO_CREADO {
                    self.mostrarMensajeInfo(mensaje: "Registro exitoso.\nUn mensaje de correo le será enviado para confirmar su registro.", cerrar: true)
                } else if respuesta.statusCode == ERROR_INFO_INVALIDA || respuesta.statusCode == ERROR_PASSWORD_INVALIDO {
                    if let datosBin = data, let body = String(data: datosBin, encoding: .utf8) {
                        //print("Body-> \(body)")
                        let diccionario = try? (JSONSerialization.jsonObject(with: body.data(using: .utf8)!, options: .mutableContainers) as! NSDictionary)
                        //print(diccionario ?? [:])
                        let mensajeRegreso = diccionario!["message"] as! String
                        /*let mensajes = diccionario!["errors"] as! NSDictionary
                        let mensajeCorreo = mensajes["email"] as? [String] ?? []
                        let mensajeAlias = mensajes["username"] as? [String] ?? []
                        let mensajePassword1 = mensajes["password1"] as? [String] ?? []
                        let mensajePassword2 = mensajes["password2"] as? [String] ?? []
                        var mensajeErrores = [String]()
                        for mp in mensajeAlias {
                            mensajeErrores.append(mp)
                        }
                        for mp in mensajeCorreo {
                            mensajeErrores.append(mp)
                        }
                        for mp in mensajePassword1 {
                            mensajeErrores.append(mp)
                        }
                        for mp in mensajePassword2 {
                            mensajeErrores.append(mp)
                        }
 
                        var listaMensajes = ""
                        for strError in mensajeErrores {
                            listaMensajes = listaMensajes + strError
                            listaMensajes += "\n"
                        }
                        */
                        self.mostrarMensajeInfo(mensaje: mensajeRegreso, cerrar: false)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error en los datos del usuario", cerrar: false)
                    }
                } else if respuesta.statusCode == ERROR_SERVIDOR {
                    self.mostrarMensajeInfo(mensaje: "Error en el servidor, intente más tarde", cerrar: false)
                }
            } else {
                if let error = error {
                    //print("Error desconocido, \(error)")
                    let tipoError = error as NSError
                    if tipoError.code == -1009 {
                        self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error, al enviar tu registro", cerrar: false)
                    }
                }
            }
        }
        task.resume()
    }
    
    func validarCorreo(correo: String) -> Bool {
        let formatoCorrecto = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let predicado = NSPredicate(format: "SELF MATCHES %@", formatoCorrecto)
        return predicado.evaluate(with: correo)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    @IBAction func aceptar(_ sender: UITapGestureRecognizer) {
        self.aceptaPolitica = !self.aceptaPolitica
        if aceptaPolitica {
            self.imgAcepta.image = UIImage(named: "check_1")!
        } else {
          self.imgAcepta.image = UIImage(named: "uncheck_1")!
        }
    }
    
    @IBAction func regresar(sender: UIStoryboardSegue) {
        
    }
}
