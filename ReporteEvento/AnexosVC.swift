//
//  AnexosVC.swift
//  MKE
//
//  Created by Roberto Martínez Román on 4/25/19.
//  Copyright © 2019 Roberto Martinez Roman. All rights reserved.
//

import UIKit

class AnexosVC: UIViewController
{
    // Las imágenes seleccionadas ([UIImage])
    var arrImagenes: [UIImage] = []
    
    // El video seleccionado
    var datosVideo: Data!
    // El audio seleccionado
    var datosAudio: Data!
    
    var tipoArchivo: String = ""
    
    var delegadoAnexos: DelegadoAnexos!
    
    @IBOutlet weak var coleccionFotos: UICollectionView!
    
    // URL del video/audio seleccionado
    @IBOutlet weak var lblVideo: UILabel!
    @IBOutlet weak var lblAudio: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if delegadoAnexos != nil {
            delegadoAnexos.leerListaAnexos(arrImagenes)
            if let datosVideo = self.datosVideo {
                delegadoAnexos.leerVideo(datosVideo)
            }
            if let datosAudio = self.datosAudio {
                delegadoAnexos.leerAudio(datosAudio, self.tipoArchivo)
            }
        }
    }
    
    
    @IBAction func agregarAnexoAudio(_ sender: UIButton) {
        AttachmentHandler.shared.recuperarAudio(vc: self)
        AttachmentHandler.shared.audioPickBlock = { (data, url)  in
            self.tipoArchivo = url.pathExtension
            self.lblAudio.text = "Audio agregado (\(self.tipoArchivo))"
            self.datosAudio = data
        }
    }
    
    
    @IBAction func agregarAnexoVideo(_ sender: UIButton) {
        AttachmentHandler.shared.recuperarVideo(vc: self)
        AttachmentHandler.shared.videoPickedBlock = { (data, url)  in
            let tipoArchivo = url.pathExtension
            self.lblVideo.text = "Video agregado (\(tipoArchivo))"
            self.datosVideo = data
        }
    }
    
    
    @IBAction func agregarAnexoImagen(_ sender: UIButton) {
        if arrImagenes.count < 4 {
            AttachmentHandler.shared.recuperarImagen(vc: self)
            AttachmentHandler.shared.imagePickedBlock = { (item, tipoAnexo)  in
                self.arrImagenes.append(item)
                self.coleccionFotos.reloadData()
            }
        }else {
            mostrarMensajeInfo(mensaje: "No puedes agregar más de 4 imágenes", cerrar: false)
        }
    }
    
    
    func borrarCelda(_ indexPath: IndexPath) {
        arrImagenes.remove(at: indexPath.row)
        coleccionFotos.reloadData()
    }
    
    // Muestra un mensaje de información en la pantalla
    func mostrarMensajeInfo(mensaje: String, cerrar: Bool) {
        DispatchQueue.main.async {
            let alerta = UIAlertController(title: "Aviso", message: mensaje, preferredStyle: .alert)
            let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: { (alerta) in
                if cerrar {
                    self.navigationController?.popViewController(animated: true)
                }
            })
            alerta.addAction(aceptar)
            self.present(alerta, animated: true, completion: nil)
        }
    }
}

