//
//  AttachmentHandler.swift
//  AttachmentHandler
//
//  Created by Deepak on 25/01/18.
//  Copyright © 2018 Deepak. All rights reserved.
//  Modificado por: Roberto Martínez Román

import Foundation
import UIKit
import MobileCoreServices
import AVFoundation
import Photos

class AttachmentHandler: NSObject
{
    static let shared = AttachmentHandler()
    fileprivate var currentVC: UIViewController?
    
    //MARK: - Internal Properties
    var imagePickedBlock: ((UIImage, TipoAnexo) -> Void)?
    var videoPickedBlock: ((Data, URL) -> Void)?
    var audioPickBlock: ((Data, URL) -> Void)?
    
    
    enum AttachmentType: String{
        case camera, video, photo, audio
    }
    
    
    //MARK: - Constants
    struct Constants {
        static let actionFileTypeHeading = "Archivo"
        static let actionFileTypeDescription = "Selecciona  el tipo de anexo que quieres agregar"
        static let camera = "Camara"
        static let phoneLibrary = "Galería de fotos"
        static let video = "Video"
        static let audio = "Audio"
        
        static let alertForPhotoLibraryMessage = "La app no tiene permiso para acceder  a las fotos. Para dar permiso, selecciona Configuración y habilita el permiso."
        
        static let alertForCameraAccessMessage = "La app no tiene permiso para acceder  a la cámara. Para dar permiso, selecciona Configuración y habilita el permiso."
        
        static let alertForVideoLibraryMessage = "La app no tiene permiso para acceder  a los videos. Para dar permiso, selecciona Configuración y habilita el permiso."
        
        
        static let settingsBtnTitle = "Configuración"
        static let cancelBtnTitle = "Cancelar"
    }
    
    
    //MARK: - showAttachmentActionSheet
    // Muestra un sheet con camara, imagen, video y audio
    func showAttachmentActionSheet(vc: UIViewController) {
        currentVC = vc
        let actionSheet = UIAlertController(title: Constants.actionFileTypeHeading, message: Constants.actionFileTypeDescription, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: Constants.camera, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .camera, vc: self.currentVC!)
        }))
        
        actionSheet.addAction(UIAlertAction(title: Constants.phoneLibrary, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .photo, vc: self.currentVC!)
        }))
        
        actionSheet.addAction(UIAlertAction(title: Constants.video, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .video, vc: self.currentVC!)
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: Constants.audio, style: .default, handler: { (action) -> Void in
            self.recuperarAudio(vc: self.currentVC!)
        }))
        
        actionSheet.addAction(UIAlertAction(title: Constants.cancelBtnTitle, style: .cancel, handler: nil))
        
        vc.present(actionSheet, animated: true, completion: nil)
    }
    
    func recuperarAudio(vc: UIViewController) {
        currentVC = vc
        let pickerDocs = UIDocumentPickerViewController(documentTypes: [String(kUTTypeMP3), String(kUTTypeAudio)], in: .import)
        pickerDocs.delegate = self
        pickerDocs.allowsMultipleSelection = false
        pickerDocs.modalPresentationStyle = .formSheet
        self.currentVC?.present(pickerDocs, animated: true, completion: nil)
    }
    
    
    func recuperarImagen(vc: UIViewController) {
        currentVC = vc
        self.authorisationStatus(attachmentTypeEnum: .photo, vc: self.currentVC!)
    }
    
    func recuperarVideo(vc: UIViewController) {
        currentVC = vc
        self.authorisationStatus(attachmentTypeEnum: .video, vc: self.currentVC!)
    }
    
    func recuperarCamara(vc: UIViewController) {
        currentVC = vc
        self.authorisationStatus(attachmentTypeEnum: .camera, vc: self.currentVC!)
    }
    
    
    //MARK: - Authorisation Status
    // This is used to check the authorisation status whether user gives access to import the image, photo library, video.
    // if the user gives access, then we can import the data safely
    // if not show them alert to access from settings.
    func authorisationStatus(attachmentTypeEnum: AttachmentType, vc: UIViewController){
        currentVC = vc
        
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            if attachmentTypeEnum == AttachmentType.camera{
                openCamera()
            }
            if attachmentTypeEnum == AttachmentType.photo{
                photoLibrary()
            }
            if attachmentTypeEnum == AttachmentType.video{
                videoLibrary()
            }
        case .denied:
            //print("permission denied")
            self.addAlertForSettings(attachmentTypeEnum)
        case .notDetermined:
            //print("Permission Not Determined")
            PHPhotoLibrary.requestAuthorization({ (status) in
                if status == PHAuthorizationStatus.authorized{
                    // photo library access given
                    //print("access given")
                    if attachmentTypeEnum == AttachmentType.camera{
                        self.openCamera()
                    }
                    if attachmentTypeEnum == AttachmentType.photo{
                        self.photoLibrary()
                    }
                    if attachmentTypeEnum == AttachmentType.video{
                        self.videoLibrary()
                    }
                }else{
                    //print("restriced manually")
                    self.addAlertForSettings(attachmentTypeEnum)
                }
            })
        case .restricted:
            //print("permission restricted")
            self.addAlertForSettings(attachmentTypeEnum)
        default:
            break
        }
    }
    
    
    //MARK: - CAMERA PICKER
    //This function is used to open camera from the iphone and
    func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .camera
            currentVC?.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    
    //MARK: - PHOTO PICKER
    func photoLibrary(){
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            DispatchQueue.main.async {
                let myPickerController = UIImagePickerController()
                myPickerController.delegate = self
                myPickerController.sourceType = .photoLibrary
                self.currentVC?.present(myPickerController, animated: true, completion: nil)
            }
        }
    }
    
    //MARK: - VIDEO PICKER
    func videoLibrary(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            DispatchQueue.main.async {
                let myPickerController = UIImagePickerController()
                myPickerController.delegate = self
                myPickerController.sourceType = .photoLibrary
                myPickerController.mediaTypes = [kUTTypeMovie as String, kUTTypeVideo as String]
                self.currentVC?.present(myPickerController, animated: true, completion: nil)
            }
        }
    }
    
    
    //MARK: - SETTINGS ALERT
    func addAlertForSettings(_ attachmentTypeEnum: AttachmentType){
        var alertTitle: String = ""
        if attachmentTypeEnum == AttachmentType.camera{
            alertTitle = Constants.alertForCameraAccessMessage
        }
        if attachmentTypeEnum == AttachmentType.photo{
            alertTitle = Constants.alertForPhotoLibraryMessage
        }
        if attachmentTypeEnum == AttachmentType.video{
            alertTitle = Constants.alertForVideoLibraryMessage
        }
        
        let cameraUnavailableAlertController = UIAlertController (title: alertTitle, message: nil, preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: Constants.settingsBtnTitle, style: .destructive) { (_) -> Void in
            let settingsUrl = NSURL(string:UIApplication.openSettingsURLString)
            if let url = settingsUrl {
                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
            }
        }
        let cancelAction = UIAlertAction(title: Constants.cancelBtnTitle, style: .default, handler: nil)
        cameraUnavailableAlertController.addAction(cancelAction)
        cameraUnavailableAlertController.addAction(settingsAction)
        currentVC?.present(cameraUnavailableAlertController , animated: true, completion: nil)
    }
}

//MARK: - IMAGE PICKER DELEGATE
// This is responsible for image picker interface to access image, video and then responsible for canceling the picker
extension AttachmentHandler: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        currentVC?.dismiss(animated: true, completion: nil)
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            self.imagePickedBlock?(image, .foto)
        }
        
        if let videoUrl = info[UIImagePickerController.InfoKey.mediaURL] as? NSURL{
            //print("video url: ", videoUrl)
            let data = try? Data(contentsOf: videoUrl as URL)
            self.videoPickedBlock?(data!, videoUrl as URL)
            //print("Tamaño del video: \(Double(data!.count) / 1048576.0) MB")
        }
        
        currentVC?.dismiss(animated: true, completion: nil)
    }
}

extension AttachmentHandler:UIDocumentPickerDelegate {
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        controller.dismiss(animated: true)
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        //print(urls)
        if let audioUrl = urls.first {
            //print("audio url \(audioUrl)")
            let data = try? Data(contentsOf: audioUrl)
            self.audioPickBlock?(data!, audioUrl)
            //print("Tamaño del audio: \(Double(data!.count) / 1048576.0) MB")
        }
        controller.dismiss(animated: true)
    }
}
