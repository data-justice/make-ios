//
//  DelegadoAnexos.swift
//  MKE
//
//  Created by Roberto Martínez Román on 4/26/19.
//  Copyright © 2019 Roberto Martinez Roman. All rights reserved.
//

import UIKit

protocol DelegadoAnexos {
    func leerListaAnexos(_ listaAnexos: [UIImage])
    func leerVideo(_ datosVideo: Data)
    func leerAudio(_ datosAudio: Data, _ extTipo: String)
}
