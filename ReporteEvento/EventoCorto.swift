//
//  EventoCorto.swift
//  MKE
//
//  Created by Roberto Mtz. Román on 6/3/19.
//  Copyright © 2019 Roberto Martinez Roman. All rights reserved.
//

import Foundation

class EventoCorto
{
    var tituloCorto: String
    var fecha: String
    var idEvento: String
    var tipo: TipoRegistro
    var alias: String

    init(titulo: String, fecha: String, idEvento: Int, tipo: TipoRegistro, alias: String = "") {
        self.tituloCorto = titulo
        self.fecha = fecha
        self.idEvento = "\(idEvento)"
        self.tipo = tipo
        self.alias = alias
    }
}
