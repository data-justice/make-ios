//
//  MuestraAnexo.swift
//  MKE
//
//  Created by Roberto Mtz. Román on 5/30/19.
//  Copyright © 2019 Roberto Martinez Roman. All rights reserved.
//

import UIKit
import WebKit

class MuestraAnexo: UIViewController, WKNavigationDelegate
{

    @IBOutlet weak var webAnexo: WKWebView!
    
    @IBOutlet weak var actEspera: UIActivityIndicatorView!
    
    var urlRecurso: URL!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let recurso = urlRecurso {
            let request = URLRequest(url: recurso)
            webAnexo.navigationDelegate = self
            webAnexo.load(request)
        }
    }
    

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        actEspera.stopAnimating()
    }
    
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        actEspera.stopAnimating()
    }

}
