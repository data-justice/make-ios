//
//  ReporteVC.swift
//  MKE
//
//  Created by Roberto Martínez Román on 3/10/19.
//  Copyright © 2019 Roberto Martinez Roman. All rights reserved.
//

import UIKit
import CoreLocation

class ReporteVC: UIViewController, UITextFieldDelegate, UITextViewDelegate
{
    var idIsla: Int!
    
    // GPS
    let gps = CLLocationManager()
    var posicionActual: CLLocation!
    
    // Los municipios de un estado
    var arrMunicipios: [String] = []
    // Diccionario de estados:municipios
    var diccionarioEdos: [String:[String]] = [:]
    
    // Pickers
    var pickerEstados: UIPickerView!
    var pickerTipoEvento: UIPickerView!
    var pickerMunicipios: UIPickerView!
    
    // Datos capturados
    @IBOutlet weak var tfNombreCorto: UITextField!
    @IBOutlet weak var tfHora: UITextField!
    @IBOutlet weak var tfFecha: UITextField!
    @IBOutlet weak var tvDescripcion: UITextView!
    @IBOutlet weak var tfTipoEvento: UITextField!
    @IBOutlet weak var tfPais: UITextField!
    @IBOutlet weak var tfEstado: UITextField!
    @IBOutlet weak var tfMunicipio: UITextField!
    @IBOutlet weak var tfCodigoPostal: UITextField!
    @IBOutlet weak var tfDireccion: UITextField!
    @IBOutlet weak var lblCuenta: UILabel!
    @IBOutlet weak var scrollReporte: UIScrollView!
    
    @IBOutlet weak var activEspera: UIActivityIndicatorView!
    @IBOutlet weak var lblAnexos: UILabel!
    
    @IBOutlet weak var btnSeleccionarAnexos: UIButton!
    @IBOutlet weak var btnRecargarDir: UIButton!
    
    @IBOutlet weak var btnEnviarReporte: UIButton!
    
    var tfEditandoAhora: UITextField!
    var offsetScroll: CGPoint!
    
    // Anexos
    var listaAnexos: [UIImage] = [UIImage]()
    //var listaLigasAnexos: [String] = []
    var listaLigasAnexos: [Int] = []
    var datosVideo: Data!
    var datosAudio: Data!
    var extTipo: String!    // mp3, m4a
    
    // Tipo de evento (nuevo, complemento). Recibido del controlador anterior
    var tipoEvento: TipoEvento = .nuevo
    var diccionarioDatosEvento: Dictionary<String, Any>!
    
    // MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tvDescripcion.layer.borderWidth = 0.5
        tvDescripcion.layer.cornerRadius = 3
        tvDescripcion.layer.borderColor = UIColor.lightGray.cgColor
        
        crearPickerTipoEvento()
        crearPickerFecha()
        crearPickerHora()
        // Ubicación, Generar diccionarios
        cargarEdosMunicipios()
        arrMunicipios = diccionarioEdos[arrEstadosMexico[0]]!
        crearPickerMunicipios()
        crearPickerEstados()
        
        // Para scroll de los campos
        NotificationCenter.default.addObserver(self, selector: #selector(tecladoAparece(notif:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(tecladoDesaparece(notif:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        tvDescripcion.delegate = self
        
        let backBtn = UIBarButtonItem()
        backBtn.title = "Regresar"
        navigationItem.backBarButtonItem = backBtn
        
        // GPS
        gps.delegate = self
        gps.requestWhenInUseAuthorization()
        
        // Configurar evento nuevo / complemento
        if tipoEvento == .nuevo {
            self.title = "Nuevo reporte"
        } else if tipoEvento == .complemento {
            self.title = "Complemento de reporte"
            configurarCamposParaComplemento()
        } else if tipoEvento == .consulta {
            self.title = "Consulta reporte"
            configurarCamposParaComplemento()
            configurarCamposParaConsulta()
        } else if tipoEvento == .consultaComplemento {
            self.title = "Consulta complemento"
            configurarCamposParaComplemento()
            configurarCamposParaConsulta()
        }
    }
    
    func configurarCamposParaConsulta() {
        // Deshabilita campos partículares
        self.tfNombreCorto.text = diccionarioDatosEvento["title"] as? String ?? "No existe"
        self.tfNombreCorto.isEnabled = false
        // Decodificar la descripción
        let descripcionCodificada = diccionarioDatosEvento["description"] as? String ?? "No existe"
        let descripcion = descripcionCodificada.removingPercentEncoding
        self.tvDescripcion.text = descripcion
        // Configurar tvDescripcino para CONSULTA
        self.tvDescripcion.isEditable = false
        self.tvDescripcion.dataDetectorTypes = [.link]
        
        self.tfFecha.text = diccionarioDatosEvento["date"] as? String ?? "No existe"
        self.tfFecha.isEnabled = false
        self.tfHora.text = diccionarioDatosEvento["time"] as? String ?? "No existe"
        self.tfHora.isEnabled = false
        
        // Oculta botones
        self.btnRecargarDir.isHidden = true
        let listaAnexos = diccionarioDatosEvento["attachments"] as! NSArray
        if listaAnexos.count > 0 {
            self.btnSeleccionarAnexos.setTitle("Ver anexos", for: .normal)
        } else {
            self.btnSeleccionarAnexos.isHidden = true
        }
        self.btnEnviarReporte.isHidden = true
    }
    
    func configurarCamposParaComplemento() {
        //print("EDITAR: \(diccionarioDatosEvento!)")
        self.tfTipoEvento.text = diccionarioDatosEvento["category"] as? String ?? "No existe"
        self.tfTipoEvento.isEnabled = false
        self.tfPais.text = diccionarioDatosEvento["location_country"] as? String ?? "No existe"
        self.tfPais.isEnabled = false
        self.tfEstado.text = diccionarioDatosEvento["location_state"] as? String ?? "No existe"
        self.tfEstado.isEnabled = false
        self.tfMunicipio.text = diccionarioDatosEvento["location_city"] as? String ?? "No existe"
        self.tfMunicipio.isEnabled = false
        self.tfCodigoPostal.text = diccionarioDatosEvento["location_zip_code"] as? String ?? "00000"
        self.tfCodigoPostal.isEnabled = false
        self.tfDireccion.text = diccionarioDatosEvento["location_address"] as? String ?? "No existe"
        self.tfDireccion.isEnabled = false
        self.btnRecargarDir.isEnabled = false
        
        
        // Editables
        self.tfFecha.text = diccionarioDatosEvento["date"] as? String ?? "2000-01-01"
        let hora = formatearHora(diccionarioDatosEvento["time"] as? String ?? "00:00")
        self.tfHora.text = hora
        
        self.btnSeleccionarAnexos.isEnabled = true
    }
    
    func formatearHora(_ hora: String) -> String {
        let datos = hora.components(separatedBy: ":")
        //print(datos)
        if let horaNum = Int(datos[0]), let minuto = Int(datos[1]) {
            var horaFinal = horaNum
            if horaNum >= 13 {
                horaFinal -= 12
                return "\(horaFinal):\(minuto) PM"
            }
            if horaNum == 0 {
                return "0:\(minuto) AM"
            }
            if horaNum == 12 {
                return "12:\(minuto) PM"
            }
            return "\(datos[0]):\(minuto) AM"
        }
        return "0:0 AM"
    }
    
    
    func cargarEdosMunicipios() {
        let ruta = Bundle.main.path(forResource: "municipiosMexico", ofType: "txt")!
        let url = URL(fileURLWithPath: ruta)
        let contenido = try! String(contentsOf: url)
        
        let lineas = contenido.components(separatedBy: "\n")
        
        var indice: Int = 0
        while indice<lineas.count {
            var linea = lineas[indice]
            if linea.hasSuffix("\r") {
                linea.removeLast()
            }
            if linea.contains("#") {
                let indInicio = linea.firstIndex(of: " ") ?? linea.endIndex
                let inicio = linea.index(after: indInicio)
                let estado = String(linea[inicio...])
                // Buscar todos los municipios
                var municipios: [String] = []
                indice += 1
                while !lineas[indice].contains("#") {
                    var municipio = lineas[indice]
                    if municipio != "" {
                        if municipio.hasSuffix("\r") {
                            municipio.removeLast()
                        }
                        municipios.append(municipio)
                    } else {
                        diccionarioEdos[estado] = municipios
                        //print(diccionarioEdos.keys)
                        return
                    }
                    indice += 1
                }
                diccionarioEdos[estado] = municipios
            }
        }
    }
    
    
    func crearPickerEstados() {
        // Pickerview
        pickerEstados = UIPickerView()
        pickerEstados.delegate = self
        self.tfEstado.inputView = pickerEstados
        // Toolbar (botón Listo)
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        let btnListo = UIBarButtonItem(title: "Listo", style: .plain, target: self, action: #selector(ReporteVC.cerrarTeclado))
        let espaciador = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([espaciador, btnListo], animated: true)
        toolBar.isUserInteractionEnabled = true
        toolBar.barTintColor = UIColor.black
        toolBar.tintColor = UIColor.white
        toolBar.alpha = ALPHA
        
        tfEstado.inputAccessoryView = toolBar
        pickerView(pickerEstados, didSelectRow: 0, inComponent: 0)
    }
    
    func crearPickerMunicipios() {
        // Pickerview
        self.pickerMunicipios = UIPickerView()
        self.pickerMunicipios.delegate = self
        self.tfMunicipio.inputView = pickerMunicipios
        // Toolbar (botón Listo)
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        let btnListo = UIBarButtonItem(title: "Listo", style: .done, target: self, action: #selector(ReporteVC.cerrarTeclado))
        let espaciador = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([espaciador, btnListo], animated: true)
        toolBar.isUserInteractionEnabled = true
        toolBar.barTintColor = UIColor.black
        toolBar.tintColor = UIColor.white
        toolBar.alpha = ALPHA
        
        tfMunicipio.inputAccessoryView = toolBar
        pickerView(pickerMunicipios, didSelectRow: 0, inComponent: 0)
    }
    
    
    func crearPickerTipoEvento() {
        // Pickerview
        self.pickerTipoEvento = UIPickerView()
        pickerTipoEvento.delegate = self
        self.tfTipoEvento.inputView = pickerTipoEvento
        // Toolbar (botón Listo)
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        let btnListo = UIBarButtonItem(title: "Listo", style: .plain, target: self, action: #selector(ReporteVC.cerrarTeclado))
        let espaciador = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([espaciador, btnListo], animated: true)
        toolBar.isUserInteractionEnabled = true
        toolBar.barTintColor = UIColor.black
        toolBar.tintColor = UIColor.white
        toolBar.alpha = ALPHA
        
        tfTipoEvento.inputAccessoryView = toolBar
        pickerView(pickerTipoEvento, didSelectRow: 0, inComponent: 0)
    }
    
    func crearPickerHora() {
        let pickerHora = UIDatePicker()
        pickerHora.datePickerMode = .time
        // Limitar hora actual. Por ahora no, REVISAR si es necesario
        pickerHora.addTarget(self, action: #selector(cambiaPickerTime), for: .valueChanged)
        tfHora.inputView = pickerHora
        // Toolbar (botón Listo)
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let btnListo = UIBarButtonItem(title: "Listo", style: .plain, target: self, action: #selector(ReporteVC.cerrarTeclado))
        let espaciador = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([espaciador, btnListo], animated: true)
        toolBar.isUserInteractionEnabled = true
        toolBar.barTintColor = UIColor.black
        toolBar.tintColor = UIColor.white
        toolBar.alpha = ALPHA
        
        tfHora.inputAccessoryView = toolBar
        cambiaPickerTime(pickerTime: pickerHora)
    }
    
    @objc func cambiaPickerTime(pickerTime: UIDatePicker) {
        let formato = DateFormatter()
        formato.dateFormat = "h:mm a"
        tfHora.text = formato.string(from: pickerTime.date)
    }
    
    func crearPickerFecha() {
        let pickerFecha = UIDatePicker()
        pickerFecha.datePickerMode = .date
        // Limitar fecha
        var componentesFecha = DateComponents()
        componentesFecha.year = 2000
        pickerFecha.minimumDate = Calendar.current.date(from: componentesFecha)
        pickerFecha.maximumDate = Date()
        // Evento del pickerView
        pickerFecha.addTarget(self, action: #selector(cambiaPickerDate), for: .valueChanged)
        tfFecha.inputView = pickerFecha
        pickerFecha.locale = Locale(identifier: "es-MX")
        // Toolbar (botón Listo)
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let btnListo = UIBarButtonItem(title: "Listo", style: .plain, target: self, action: #selector(ReporteVC.cerrarTeclado))
        let espaciador = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([espaciador, btnListo], animated: true)
        toolBar.isUserInteractionEnabled = true
        toolBar.barTintColor = UIColor.black
        toolBar.tintColor = UIColor.white
        toolBar.alpha = ALPHA
        
        tfFecha.inputAccessoryView = toolBar
        cambiaPickerDate(pickerDate: pickerFecha)
    }
    
    @objc func cambiaPickerDate(pickerDate: UIDatePicker) {
        let formato = DateFormatter()
        formato.dateFormat = "yyyy-MM-dd"
        tfFecha.text = formato.string(from: pickerDate.date)
    }
    
    @objc func cerrarTeclado() {
        self.view.endEditing(true)
    }
    
    @IBAction func tapSobreVista(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func textViewDidChange(_ textView: UITextView) {
        lblCuenta.text = "\(5000 - tvDescripcion.text.utf8CString.count)"
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return tvDescripcion.text.utf8CString.count + (text.utf8CString.count - range.length) <= 5001
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.tfEditandoAhora = nil
    }
    
    // MARK: - Envío de información
    
    @IBAction func enviarReporteEvento(_ sender: UIButton) {
        
        if validarCampos() {
            if validarTamanoAnexos() {
                enviarArchivosAnexos()  // Al terminar de enviar, manda los datos del reporte
            } else {
                self.mostrarMensajeInfo(mensaje: "Alguno de los anexos excede el tamaño permitido", cerrar: false)
            }
        }  else {
            self.mostrarMensajeInfo(mensaje: "Llene todos los campos e intente nuevamente", cerrar: false)
        }
    }
    
    // Crear un grupo para regresar hasta que envíe todos los anexos
    func enviarArchivosAnexos() {
        self.listaLigasAnexos.removeAll()
        self.activEspera.startAnimating()
        
        DispatchQueue.global(qos: .userInitiated).async {
            let gpoDescarga = DispatchGroup()
            for indice in 0..<self.listaAnexos.count {
                gpoDescarga.enter()
                self.enviarImagen(self.listaAnexos[indice], gpoDescarga)
            }
            var numAnexos = 0
            if self.datosVideo != nil  {
                gpoDescarga.enter()
                self.enviarVideo(self.datosVideo!, gpoDescarga)
                numAnexos -= 1
            }
            if self.datosAudio != nil {
                gpoDescarga.enter()
                self.enviarAudio(self.datosAudio!, gpoDescarga, self.extTipo)
                numAnexos -= 1
            }
            gpoDescarga.wait()  // Espera a que se envíen todos los anexos
            DispatchQueue.main.async {
                self.activEspera.stopAnimating()
            }
            numAnexos += self.listaLigasAnexos.count
            //print("Termina de enviar")
            //  Ahora envía el reporte
            if self.listaAnexos.count == numAnexos {
                DispatchQueue.main.async {
                    if self.tipoEvento == .nuevo {
                        self.enviarDatosReporte()
                    } else {
                        self.enviarDatosComplemento()
                    }
                }
            }
        }
    }
    
    // Envía una imagen
    func enviarImagen(_ imgAnexo: UIImage, _ grupo: DispatchGroup) {

        // Token del usuario
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        var urlRequest = URLRequest(url: URL(string: URL_SERVICIO_ATTACHMENT)!)
        urlRequest.httpMethod = "POST"
        let datosBin = imgAnexo.jpegData(compressionQuality: 0.85)
        
        let filename = "imagen.jpg"

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // String para separar datoss
        let boundary = UUID().uuidString
        
        urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        var data = Data()
        
        // Add the image data to the raw http request data
        data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
        data.append("Content-Disposition: form-data; name=\"file\"; filename=\"\(filename)\"\r\n".data(using: .utf8)!)
        data.append("Content-Type: image/png\r\n\r\n".data(using: .utf8)!)
        data.append(datosBin!)
        
        // End the raw http request data, note that there is 2 extra dash ("-") at the end, this is to indicate the end of the data
        // According to the HTTP 1.1 specification https://tools.ietf.org/html/rfc7230
        data.append("\r\n--\(boundary)--\r\n".data(using: .utf8)!)
        
        // Manda los datos al servidor
        session.uploadTask(with: urlRequest, from: data, completionHandler: { data, response, error in
            
            if(error == nil){
                if let response = response {
                    //print("Response Anexo FILE: \(response)")
                    let respuesta = response as! HTTPURLResponse
                    if respuesta.statusCode == OK_ATTACHMENT_CREADO {
                        if let datosBin = data, let _ = String(data: datosBin, encoding: .utf8) {
                            let resultado = try! JSONSerialization.jsonObject(with: datosBin, options: []) as? NSDictionary ?? [:]
                            //print(resultado)
                            let liga = resultado["id"]! as! Int
                            //print("id imagen: \(liga)")
                            self.listaLigasAnexos.append(liga)
                        }
                    } else if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                        self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                    } else {
                        //if let datosBin = data, let body = String(data: datosBin, encoding: .utf8) {
                            //print("BodyError-> \(body)")
                        //}
                        self.mostrarMensajeInfo(mensaje: "Error al enviar el anexo", cerrar: false)
                    }
                    
                }
            } else {
                //print("Error desconocido, \(error!)")
                let tipoError = error! as NSError
                if tipoError.code == ERROR_CONEXION_INTERNET {
                    self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                } else {
                    self.mostrarMensajeInfo(mensaje: "Error, al enviar el anexo", cerrar: false)
                }
            }
            grupo.leave()   // Termina el envio del anexo
        }).resume()
    }
    
    // Envía un video
    func enviarVideo(_ datosVideoAnexo: Data, _ grupo: DispatchGroup) {

        let datosBin = datosVideoAnexo
        
        // Token del usuario
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        var urlRequest = URLRequest(url: URL(string: URL_SERVICIO_ATTACHMENT)!)
        urlRequest.httpMethod = "POST"
        //let datosBin = try? Data(contentsOf: urlArchivoVideo)
        
        let filename = "video.mp4"
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // String para separar datoss
        let boundary = UUID().uuidString
        
        urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        var data = Data()
        
        // Add the image data to the raw http request data
        data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
        data.append("Content-Disposition: form-data; name=\"file\"; filename=\"\(filename)\"\r\n".data(using: .utf8)!)
        data.append("Content-Type: video/mp4\r\n\r\n".data(using: .utf8)!)
        data.append(datosBin)
        
        data.append("\r\n--\(boundary)--\r\n".data(using: .utf8)!)
        
        session.uploadTask(with: urlRequest, from: data, completionHandler: { data, response, error in
            
            if let response = response {
                //print("Response: \(response)")
                let respuesta = response as! HTTPURLResponse
                if respuesta.statusCode == OK_ATTACHMENT_CREADO {
                    if let datosBin = data, let _ = String(data: datosBin, encoding: .utf8) {
                        //print("Body video-> \(body)")
                        let resultado = try! JSONSerialization.jsonObject(with: datosBin, options: []) as? NSDictionary ?? [:]
                        let liga = resultado["id"]! as! Int
                        self.listaLigasAnexos.append(liga)
                    }
                } else  if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                    self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                } else {
                    //if let datosBin = data, let body = String(data: datosBin, encoding: .utf8) {
                    //print("BodyError-> \(body)")
                    //}
                    self.mostrarMensajeInfo(mensaje: "Error al enviar el anexo", cerrar: false)
                }
                
            } else {
                if let error = error {
                    //print("Error desconocido, \(error)")
                    let tipoError = error as NSError
                    if tipoError.code == ERROR_CONEXION_INTERNET {
                        self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error, al enviar el anexo", cerrar: false)
                    }
                }
            }
            grupo.leave()   // Termina el envio del anexo
        }).resume()
    }
    
    // Envía un audio
    func enviarAudio(_ datosAudioAnexo: Data, _ grupo: DispatchGroup, _ extTipo: String) {
        
        let datosBin = datosAudioAnexo
        // Token del usuario
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        var urlRequest = URLRequest(url: URL(string: URL_SERVICIO_ATTACHMENT)!)
        urlRequest.httpMethod = "POST"
        
        let filename = "audio.\(extTipo)"
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // String para separar datos
        let boundary = UUID().uuidString
        
        urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        var data = Data()
        //print("Datos len=\(datosBin.count)")
        
        // Add the audio data to the raw http request data
        data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
        data.append("Content-Disposition: form-data; name=\"file\"; filename=\"\(filename)\"\r\n".data(using: .utf8)!)
        data.append("Content-Type: audio/mpeg\r\n\r\n".data(using: .utf8)!)
        data.append(datosBin)
        
        data.append("\r\n--\(boundary)--\r\n".data(using: .utf8)!)
        
        session.uploadTask(with: urlRequest, from: data, completionHandler: { data, response, error in
            
            if let response = response {
                //print("Respuesta audio: \(response)")
                let respuesta = response as! HTTPURLResponse
                if respuesta.statusCode == OK_ATTACHMENT_CREADO {
                    if let datosBin = data, let _ = String(data: datosBin, encoding: .utf8) {
                        //print("Body AUDIO-> \(body)")
                        let resultado = try! JSONSerialization.jsonObject(with: datosBin, options: []) as? NSDictionary ?? [:]
                        let liga = resultado["id"]! as! Int
                        //print("liga audio: \(liga)")
                        self.listaLigasAnexos.append(liga)
                    }
                } else if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                    self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                } else {
                    //if let datosBin = data, let body = String(data: datosBin, encoding: .utf8) {
                    //print("BodyError audio-> \(body)")
                    //}
                    self.mostrarMensajeInfo(mensaje: "Error al enviar el anexo", cerrar: false)
                }
                
            } else {
                if let error = error {
                    //print("Error desconocido, \(error)")
                    let tipoError = error as NSError
                    if tipoError.code == ERROR_CONEXION_INTERNET {
                        self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error, al enviar el anexo", cerrar: false)
                    }
                }
            }
            grupo.leave()   // Termina el envio del anexo
        }).resume()
    }
    
    
    func validarTamanoAnexos() -> Bool {
        var correcto = true
        for img in self.listaAnexos {
            let datos = img.jpegData(compressionQuality: 0.85)!
            //print("len(img) \(datos.count)")
            if datos.count >= MAX_TAM_IMAGEN {
                correcto = false
                return  correcto
            }
        }
        
        if let datosAudio = self.datosAudio {
            if datosAudio.count > MAX_TAM_AUDIO {
                correcto = false
                return  correcto
            }
        }
        
        if let datosVideo = self.datosVideo {
            if datosVideo.count > MAX_TAM_VIDEO {
                correcto = false
                return  correcto
            }
        }
        
        return correcto
    }
    
    func enviarDatosReporte() {
        DispatchQueue.main.async {
            self.activEspera.startAnimating()
        }
        // Leer campos
        let tituloCorto = self.tfNombreCorto.text!
        let descripcion = self.tvDescripcion.text!.addingPercentEncoding(withAllowedCharacters: .alphanumerics)
        let fecha = self.tfFecha.text!
        let hora = self.tfHora.text!
        let categoria = self.tfTipoEvento.text!
        var latitud = "0"
        var longitud = "0"
        if posicionActual != nil {
            latitud = "\(posicionActual.coordinate.latitude)"
            longitud = "\(posicionActual.coordinate.longitude)"
        }
        let pais = self.tfPais.text!
        let estado = self.tfEstado.text!
        let municipio = self.tfMunicipio.text!
        let codigoPostal = self.tfCodigoPostal.text!
        var direccion = self.tfDireccion.text!
        if direccion.count > 150 {
            direccion = String(direccion.prefix(150))
        }
        
        var group_id = ""
        if let idIsla = UserDefaults.standard.string(forKey: LLAVE_ID_ISLA) {
            group_id = """
            ,
            "group_id": \(idIsla)
            """
        }
        
        // Token para authorization
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        // Enviar información al servidor, recibe token
        // 400 credenciales incorrectas, 200 ok
        let url = URL(string: URL_SERVICIO_INCIDENTE)
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-type")
        request.setValue("no-cache", forHTTPHeaderField: "Cache-Control")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.httpBody = """
            {
            "title": "\(tituloCorto)",
            "category": "\(categoria)",
            "description": "\(descripcion ?? "No existe")",
            "date": "\(fecha)",
            "time": "\(hora)",
            "location_latitude": \(latitud),
            "location_longitude": \(longitud),
            "location_address" : "\(direccion)",
            "location_country" : "\(pais)",
            "location_state" : "\(estado)",
            "location_city": "\(municipio)",
            "location_zip_code": "\(codigoPostal)",
            "attachments": \(self.listaLigasAnexos)
            \(group_id)
            }
            """.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if let response = response {
                //print("Response  evento: \(response)")
                let respuesta = response as! HTTPURLResponse
                //print("Respuesta: \(respuesta)")
                if respuesta.statusCode == OK_REGISTRO_CREADO {
                    if data != nil {
                        let _ = String(data: data!, encoding: .utf8)
                        //print("Body reporte: \(body!)")
                        // Mensaje de éxito
                        self.mostrarMensajeInfo(mensaje: "Se ha creado con éxito el reporte de evento", cerrar: true)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error en los datos del usuario", cerrar: false)
                    }
                } else if respuesta.statusCode == ERROR_REGISTRO_EVENTO_DUPLICADO {
                    if data != nil {
                        let body = String(data: data!, encoding: .utf8)
                        //print("Body-> \(body!)")
                        self.mostrarMensajeInfo(mensaje: "Error: \(body!)", cerrar: false)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error en los datos del usuario", cerrar: false)
                    }
                } else if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                    self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                } else if respuesta.statusCode == ERROR_SERVIDOR {
                    self.mostrarMensajeInfo(mensaje: "Error en el servidor, intente más tarde", cerrar: false)
                } else {
                    self.mostrarMensajeInfo(mensaje: "Error en el servidor...", cerrar: false)
                }
            } else {
                if let error = error {
                    //print("Error desconocido, \(error)")
                    let tipoError = error as NSError
                    if tipoError.code == ERROR_CONEXION_INTERNET {
                        self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error, al enviar tu información", cerrar: false)
                    }
                }
            }
            DispatchQueue.main.async {
                self.activEspera.stopAnimating()
            }
        }
        task.resume()
    }
    
    func enviarDatosComplemento() {
        DispatchQueue.main.async {
            self.activEspera.startAnimating()
        }
        // Leer campos
        let incidente = diccionarioDatosEvento["id"]! as! Int
        let tituloCorto = self.tfNombreCorto.text!
        let descripcion = self.tvDescripcion.text!.addingPercentEncoding(withAllowedCharacters: .alphanumerics)
        let fecha = self.tfFecha.text!
        let strHora = self.tfHora.text!
        let datos = strHora.components(separatedBy: " ")
        let hora = "\(datos[0]):0"
        
        // Token para authorization
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        // Enviar información al servidor, recibe token
        // 400 credenciales incorrectas, 200 ok
        let url = URL(string: URL_SERVICIO_INCIDENTE+"\(incidente)"+URL_SERVICIO_INCIDENTE_COMPLEMENTO)
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-type")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.httpBody = """
            {
            "incident": \(incidente),
            "title": "\(tituloCorto)",
            "description": "\(descripcion ?? "No existe")",
            "date": "\(fecha)",
            "time": "\(hora)",
            "attachments": \(self.listaLigasAnexos)
            }
            """.data(using: .utf8)
        
        //print("Manda complemento: \(String(data: request.httpBody!, encoding: .utf8) ?? "no existen datos")")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if error == nil {
                let respuesta = response! as! HTTPURLResponse
                //print("Respuesta complemento: \(respuesta)")

                if respuesta.statusCode == OK_REGISTRO_CREADO {
                    if data != nil {
                        //let body = String(data: data!, encoding: .utf8)!
                        //print("Recupera CON COMPLEMENTO: \(body)")
                    }
                    self.mostrarMensajeInfo(mensaje: "Se ha creado con éxito el complemento", cerrar: true)
                } else if respuesta.statusCode == ERROR_REGISTRO_EVENTO_DUPLICADO {
                    self.mostrarMensajeInfo(mensaje: "Ya existe un reporte con este título", cerrar: false)
                } else if respuesta.statusCode == ERROR_SERVIDOR {
                    self.mostrarMensajeInfo(mensaje: "Error en el servidor, intente más tarde", cerrar: false)
                } else  if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                    self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                } else {
                    self.mostrarMensajeInfo(mensaje: "Error en el servidor...", cerrar: false)
                }
            } else {
                if let error = error {
                    //print("Error desconocido, \(error)")
                    let tipoError = error as NSError
                    if tipoError.code == ERROR_CONEXION_INTERNET {
                        self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error, al enviar tu información", cerrar: false)
                    }
                }
            }
            DispatchQueue.main.async {
                self.activEspera.stopAnimating()
            }
        }
        task.resume()
    }
    
    func validarCampos() -> Bool {
        
        if let tituloCorto = self.tfNombreCorto.text,
            let descripcion = self.tvDescripcion.text,
            let fecha = self.tfFecha.text,
            let hora = self.tfHora.text,
            let categoria = self.tfTipoEvento.text,
            let pais = self.tfPais.text,
            let estado = self.tfEstado.text,
            let municipio = self.tfMunicipio.text,
            let codigoPostal = tfCodigoPostal.text,
            let direccion = tfDireccion.text {
            if tituloCorto != "" && descripcion != "" && fecha != "" && hora != "" && categoria != "" && pais != "" && estado != "" &&  municipio != "" && codigoPostal != "" && direccion != "" {
                return true
            }
        }
        
        return false
    }
    
    // Muestra un mensaje de información en la pantalla
    func mostrarMensajeInfo(mensaje: String, cerrar: Bool) {
        DispatchQueue.main.async {
            let alerta = UIAlertController(title: "Aviso", message: mensaje, preferredStyle: .alert)
            let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: { (alerta) in
                if mensaje == MENSAJE_CREDENCIALES_INVALIDAS {
                    self.navigationController?.popToRootViewController(animated: true)
                } else
                if cerrar {
                    self.navigationController?.popViewController(  animated: true)
                }
            })
            alerta.addAction(aceptar)
            self.present(alerta, animated: true, completion: nil)
        }
    }
    
    // Manda un attachment al servidor
    @IBAction func seleccionarAnexos(_ sender: UIButton) {
        if tipoEvento == .nuevo || tipoEvento == .complemento {
            performSegue(withIdentifier: "segueAnexos", sender: self)
        } else {
            // Muestra el menú para ver los anexos
            mostrarMenuAnexos()
        }
    }
    
    func mostrarMenuAnexos() {
        var indiceFoto = 1
        let menu = UIAlertController(title: "Anexos", message: "Seleccione el anexo que quiere ver", preferredStyle: .actionSheet)
        // Iterar sobre los anexos para crear botones
        let listaAnexos = diccionarioDatosEvento["attachments"] as! NSArray
        //print("LISTA ANEXOS: \(listaAnexos)")  // PENDIENTE
        for anexo in listaAnexos {
            let dAnexo = anexo as! NSDictionary
            let direccion = dAnexo.value(forKey: "file_url") as! String
            let dir = dAnexo.value(forKey: "file") as! String
            var tituloBtn = "Otro"
            if dir.hasSuffix("jpg") || dir.hasSuffix("jpeg") || dir.hasSuffix("png"){
                tituloBtn = "Imagen \(indiceFoto)"
                indiceFoto += 1
            } else if dir.hasSuffix("mp3") || dir.hasSuffix("m4a") {
                tituloBtn = "Audio"
            } else if dir.hasSuffix("mov") || dir.hasSuffix("mp4"){
                tituloBtn = "Video"
            }
            let btnAnexo = UIAlertAction(title: tituloBtn, style: .default) { (action) in
                let url = URL(string: direccion)!
                self.mostrarAnexo(url)
            }
            menu.addAction(btnAnexo)
        }
        let cancelar = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        menu.addAction(cancelar)
        menu.popoverPresentationController?.sourceView = self.btnEnviarReporte
        /*
        if ( UIDevice.current.userInterfaceIdiom  == UIUserInterfaceIdiom.pad )
        {
            if let ppOver = menu.popoverPresentationController {
                ppOver.sourceView = self.btnEnviarReporte
                ppOver.sourceRect = self.btnEnviarReporte.frame
                self.present(menu, animated: true)
            }
        } else {
            present(menu, animated: true)
        }
 */
        self.present(menu, animated: true)
    }
    
    func mostrarAnexo(_ url: URL) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "muestraAnexos") as! MuestraAnexo
        vc.urlRecurso = url
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueAnexos" {
            let anexosVC = segue.destination as! AnexosVC
            anexosVC.delegadoAnexos = self
            self.datosAudio = nil
            self.datosVideo = nil
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        return true
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.gps.stopUpdatingLocation()
    }
    @IBAction func prenderGps(_ sender: UIButton) {
        gps.startUpdatingLocation()
    }
}


// MARK: -
extension ReporteVC: UIPickerViewDataSource, UIPickerViewDelegate
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == self.pickerTipoEvento {
            return arrTipoEvento.count
        } else if pickerView == self.pickerMunicipios {
            return arrMunicipios.count
        }
        
        return arrEstadosMexico.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == pickerTipoEvento {
            return arrTipoEvento[row]
        } else if pickerView == pickerMunicipios {
            return arrMunicipios[row]
        }
        
        return arrEstadosMexico[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == self.pickerTipoEvento {
            tfTipoEvento.text = arrTipoEvento[row]
        } else if pickerView == self.pickerEstados {
            tfEstado.text = arrEstadosMexico[row]
            // Carga los municipios
            self.arrMunicipios = diccionarioEdos[self.tfEstado.text!]!
            self.pickerMunicipios.reloadComponent(0)
            self.pickerMunicipios.selectRow(0, inComponent: 0, animated: true)
            self.pickerView(pickerMunicipios, didSelectRow: 0, inComponent: 0)
        } else if pickerView == self.pickerMunicipios {
            if arrMunicipios.count > 0 {
                tfMunicipio.text = arrMunicipios[row]
            }
        }
    }
    
    
    // MARK: - Maneja teclado
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.tfEditandoAhora = textField
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.tfEditandoAhora.resignFirstResponder()
        self.tfEditandoAhora = nil
        return true
    }
    
    @objc func tecladoAparece(notif: Notification) {
        let info = notif.userInfo!
        
        let tamTeclado = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
        let alturaTec = tamTeclado.height
        
        let contentInset = UIEdgeInsets(top: 0, left: 0, bottom: alturaTec, right: 0)
        self.scrollReporte.contentInset = contentInset
        self.scrollReporte.scrollIndicatorInsets = contentInset
        
        var frameVista = self.view.frame
        frameVista.size.height -= alturaTec
        
        // Mueve scroll para que se vea el campo
        if self.tfEditandoAhora == nil {
            return
        }
        if !frameVista.contains(self.tfEditandoAhora.frame.origin) {
            self.scrollReporte.scrollRectToVisible(self.tfEditandoAhora.frame, animated: true)
        }
    }
    
    @objc func tecladoDesaparece(notif: Notification) {
        let contentInset = UIEdgeInsets.zero
        self.scrollReporte.contentInset = contentInset;
        self.scrollReporte.scrollIndicatorInsets = contentInset;
        self.tfEditandoAhora = nil
    }
}
// MARK: - Anexos
extension ReporteVC: DelegadoAnexos
{
    func leerAudio(_ datosAudio: Data, _ extTipo: String) {
        self.datosAudio = datosAudio
        self.extTipo = extTipo;
        var numeroAnexos = listaAnexos.count + 1
        if self.datosVideo != nil {
            numeroAnexos += 1
        }
        self.lblAnexos.text = "Tienes seleccionados \(numeroAnexos) anexos"
    }
    
    func leerVideo(_ datosVideo: Data) {
        self.datosVideo = datosVideo
        var numeroAnexos = listaAnexos.count + 1
        if self.datosAudio != nil {
            numeroAnexos += 1
        }
        self.lblAnexos.text = "Tienes seleccionados \(numeroAnexos) anexos"
    }
    
    func leerListaAnexos(_ listaAnexos: [UIImage]) {
        self.listaAnexos.removeAll()
        self.listaAnexos = listaAnexos
        var numeroAnexos = listaAnexos.count
        if self.datosVideo != nil {
            numeroAnexos += 1
        }
        if self.datosAudio != nil {
            numeroAnexos += 1
        }
        //print("Regresa con: \(self.listaAnexos.count) anexos")
        self.lblAnexos.text = "Tienes seleccionados \(numeroAnexos) anexos"
    }
}

extension Data
{
    mutating func appendString(_ string: String) {
        let data = string.data(using: .utf8, allowLossyConversion: true)
        append(data!)
    }
}

// GPS para llenar los campos del reporte
extension ReporteVC: CLLocationManagerDelegate
{
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedWhenInUse, .authorizedAlways:
            // Autorizado
            gps.desiredAccuracy = kCLLocationAccuracyBest
            gps.activityType = .other
            if tipoEvento == .nuevo {
                gps.startUpdatingLocation()
            }
        case .denied:
            // Mensaje para reautorizar
            mostrarMensajeInfo(mensaje: "Debe activar el gps en Configuración-Privacidad-Localización", cerrar: false)
        default:
            return
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let posicion = locations.last {
            posicionActual = posicion
            // pedir dirección de estas coordenadas
            let geocoder = CLGeocoder()
            geocoder.reverseGeocodeLocation(posicion) { (arrPlacemark, error) in
                if let arrPlacemark = arrPlacemark {
                    if arrPlacemark.count == 0 {
                        self.tfDireccion.text = "Dirección desconocida"
                        return
                    }
                    let diccionario = arrPlacemark[0]
                    //print("Dirección: \(diccionario)")
                    
                    // Dirección
                    var direccionTexto = ""
                    var nombre = ""
                    
                    if diccionario.name != nil {
                        nombre = diccionario.name!
                        direccionTexto.append(nombre.appending(", "))
                    }
                    
                    if let calle = diccionario.thoroughfare {
                        if !calle.contains(nombre) && !nombre.contains(calle){
                            direccionTexto.append(calle.appending(", "))
                        }
                    }
                    if let colonia = diccionario.subLocality {
                        direccionTexto.append(colonia.appending(", "))
                    }
                    if let cp = diccionario.postalCode {
                        direccionTexto.append(cp.appending(", "))
                        self.tfCodigoPostal.text = cp
                    }
                    if let ciudad = diccionario.locality {
                        direccionTexto.append(ciudad.appending(", "))
                    }
                    if let estado = diccionario.administrativeArea {
                        direccionTexto.append(estado.appending(", "))
                    }
                    if let pais = diccionario.country {
                        direccionTexto.append(pais)
                    }
                    //print("*\(direccionTexto)*")
                    self.tfDireccion.text = "\(direccionTexto)"
                    // Fuera de México???
                    if let pais = diccionario.country {
                        self.tfPais.text = pais
                        if pais != "Mexico" && pais != "México"  {
                            // Quitar pickers
                            self.tfEstado.inputView = nil
                            self.tfMunicipio.inputView = nil
                            
                            if let cp = diccionario.postalCode {
                                self.tfCodigoPostal.text = cp
                            }
                            if let ciudad = diccionario.locality {
                                self.tfMunicipio.text = ciudad
                            }
                            if let estado = diccionario.administrativeArea {
                                self.tfEstado.text = estado
                            }
                            self.gps.stopUpdatingLocation()
                            return
                        }
                    }
                    // Obtener estado y municipio con el código postal en México
                    self.obtenerEstadoMunicipio(self.tfCodigoPostal.text!)
                        
                    self.gps.stopUpdatingLocation()
                } else if let _ = error {
                    // Error
                    //print("Error al obtener dirección \(error.localizedDescription)")
                }
            }
        }
    }
    
    func obtenerEstadoMunicipio(_ codigoPostal: String) {
        let url = URL(string: URL_SERVICIO_CODIGO_POSTAL+codigoPostal+"/")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-type")
        
        // Token para authorization
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let response = response {
                //print("Response CP Reporte: \(response)")
                let respuesta = response as! HTTPURLResponse
                if respuesta.statusCode == OK_CONSULTA_CODIGO_POSTAL {
                    if let data = data, let body = String(data: data, encoding: .utf8) {
                        let arrCodigos = try? (JSONSerialization.jsonObject(with: body.data(using: .utf8)!, options: .mutableContainers) as! NSArray)
                        let diccionario = arrCodigos?[0] as! NSDictionary
                        //print("Diccionario CP: \(diccionario)")
                        DispatchQueue.main.async {
                            // ACTUALIZAR picker de estados
                            if let estado = diccionario["d_estado"] {
                                if let indice = arrEstadosMexico.firstIndex(of: estado as! String) {
                                    self.pickerEstados.selectRow(indice, inComponent: 0, animated: false)
                                    self.tfEstado.text = arrEstadosMexico[indice]
                                    self.arrMunicipios = self.diccionarioEdos[self.tfEstado.text!]!
                                    // ACTUALIZAR picker de municipios
                                    if let municipio = diccionario["D_mnpio"] {
                                        if let indice = self.arrMunicipios.firstIndex(of: municipio as! String) {
                                            self.pickerMunicipios.reloadComponent(0)
                                            self.pickerMunicipios.selectRow(indice, inComponent: 0, animated: false)
                                            self.pickerView(self.pickerMunicipios, didSelectRow: indice, inComponent: 0)
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if let _ = error {
                        //print("Error al consultar código postal: \(error.localizedDescription)")
                    }
                }
            }
        }
        task.resume()
    }
}

