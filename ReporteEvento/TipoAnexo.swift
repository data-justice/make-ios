//
//  TipoAnexo.swift
//  MKE
//
//  Created by Roberto Martínez Román on 5/9/19.
//  Copyright © 2019 Roberto Martinez Roman. All rights reserved.
//

enum TipoAnexo: String
{
    case camara
    case video
    case foto
}
