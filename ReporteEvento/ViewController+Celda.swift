//
//  VieewController+Celda.swift
//  FotoCarrete
//
//  Created by Roberto Martínez Román on 4/25/19.
//  Copyright © 2019 Roberto Martinez Roman. All rights reserved.
//
import UIKit

extension AnexosVC: UICollectionViewDelegate, UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImagenes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let celda = collectionView.dequeueReusableCell(withReuseIdentifier: "celdaFoto", for: indexPath) as! CeldaOpcion
        
        celda.imgFoto.image = arrImagenes[indexPath.row]
        
        celda.delegado = self
        celda.indexPath = indexPath
        
        return celda
    }
}

