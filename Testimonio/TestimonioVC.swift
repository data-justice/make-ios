//
//  TestimonioVC.swift
//  MKE
//
//  Created by Roberto Martínez Román on 7/12/19.
//  Copyright © 2019 Roberto Martinez Roman. All rights reserved.
//

import UIKit
import CoreLocation

class TestimonioVC: UIViewController
{
    // Datos de posición/dirección. Para llenar automáticamente el registro
    var diccionarioInfo = Dictionary<String, Any>()
    var posicionActual: CLLocation!
    
    let gps = GPS()
    
    @IBOutlet weak var pickerTipoEvento: UIPickerView!
    @IBOutlet weak var tvConsola: UITextView!
    @IBOutlet weak var vistaVideo: UIView!
    
    @IBOutlet weak var activEspera: UIActivityIndicatorView!
    
    @IBOutlet weak var btnGrabarVideo: UIButton!
    // Video
    var estaGrabando = false
    var grabadora: GrabadorVideo = GrabadorVideo()
    var timerVideo: Timer!  // 2 minutos de video máximo
    var urlVideoGrabado: URL!
    //var ligaVideo: String!  // El que regresa el servidor
    var idVideoAnexo: Int!      // El que regresa el servidor
    
    // Tipo de evento reportado
    var tipoEvento = ""
    
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configurarGPS()
        self.title = "TESTIMONIO"
        
        self.tvConsola.text = ""
    }
    
    // Saliendo de la pantalla de Botón de pánico
    override func viewWillDisappear(_ animated: Bool) {
        gps.detenerGPS()
        gps.delegado = nil
    }
    
    func configurarGPS() {
        gps.iniciarGPS()
        self.gps.delegado = self
    }
    
    // Programar grabación de video
    func programarGrabacionVideo() -> Bool {
        // Prende el video
        self.grabadora.delegado = self
        self.grabadora.vistaCamara = self.vistaVideo
        if self.grabadora.iniciarGrabador() {
            self.vistaVideo.isHidden = false
            self.grabadora.iniciarGrabacion()
            //self.vistaVideo.bringSubviewToFront(self.btnDetenerVideo)
            self.timerVideo = Timer.scheduledTimer(withTimeInterval: TIEMPO_VIDEO, repeats: false, block: { (timer) in
                DispatchQueue.main.async {
                    self.estaGrabando = false
                    timer.invalidate()
                    self.grabadora.detenerGrabacion()
                }
            })
            return true
        }
        return false
    }
    
    func cambiarBoton(titulo: String, normal: Bool) {
        DispatchQueue.main.async {
            self.btnGrabarVideo.setTitle(titulo, for: .normal)
            if normal {
                self.btnGrabarVideo.backgroundColor = UIColor.orange
            } else {
                self.btnGrabarVideo.backgroundColor = UIColor.red
            }
        }
    }
    
    //MARK: - Eventos
    @IBAction func grabarVideo(_ sender: UIButton) {
        if !estaGrabando {
            if self.programarGrabacionVideo() {
                // Iniciar la grabación
                self.estaGrabando = true
                sender.setTitle("Detener Grabación", for: .normal)
                self.cambiarBoton(titulo: "Detener Grabación", normal: false)
                agregarMensajeConsola("\nGrabando video")
            } else {
                agregarMensajeConsola("\nNo hay cámara en este dispositivo")
            }
        } else {
            // Detener la grabación
            if let timer = self.timerVideo {
                timer.invalidate()
            }
            self.estaGrabando = false
            self.grabadora.detenerGrabacion()
            self.cambiarBoton(titulo: "Iniciar Grabación", normal: true)
            self.btnGrabarVideo.isEnabled = false
        }
    }
    
    // Envía el evento de pánico al servidor RESPONDE al botón ENVIAR
    func enviarEventoPanico() {
        // BLOQUEAR REGRESO y PICKERVIEW
        self.navigationItem.hidesBackButton = true
        self.pickerTipoEvento.isUserInteractionEnabled = false

        // Muestra la consola de mensajes
        self.activEspera.startAnimating()
        self.agregarMensajeConsola("\nInicia envío del evento")
        // Detiene el 'apagado' de la pantalla
        UIApplication.shared.isIdleTimerDisabled = true
        // Envíar registro padre
        self.enviarAnexo()  // El anexo al servidor
    }
    
    // Van datos al servidor (Evento principal)
    func enviarReportePanico() {
        self.agregarMensajeConsola("Enviando reporte al servidor")
        // Leer campos
        let indice = self.pickerTipoEvento.selectedRow(inComponent: 0)
        let categoria = arrTipoEvento[indice]
        let fecha = self.obtenerFechaActual()
        let hora = self.obtenerHoraActual()
 
        var ligas = [Int]()
        if let idAnexo = self.idVideoAnexo {
            ligas.append(idAnexo)
        }
        
        var latitud = "0"
        var longitud = "0"
        if posicionActual != nil {
            latitud = "\(posicionActual.coordinate.latitude)"
            longitud = "\(posicionActual.coordinate.longitude)"
        }
        // Datos opcionales
        let descripcion = ("Evento en curso\n\n" + self.obtenerURL()).addingPercentEncoding(withAllowedCharacters: .alphanumerics)!
        var direccion = "Desconocida"
        var pais = "México"
        var estado = "Desconocido"
        var municipio = "Desconocido"
        var cp = "00000"
        if diccionarioInfo.count > 0 {
            // Si hay datos de dirección
            direccion = diccionarioInfo["direccion"] as! String
            pais = diccionarioInfo["pais"] as! String
            estado = diccionarioInfo["estado"] as! String
            municipio = diccionarioInfo["municipio"] as! String
            cp = diccionarioInfo["cp"] as! String
        }
        
        var group_id = ""
        if let idIsla = UserDefaults.standard.string(forKey: LLAVE_ID_ISLA) {
            group_id = """
            ,
            "group_id": \(idIsla)
            """
        }
        
        // Token para authorization
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        // Enviar información al servidor
        let url = URL(string: URL_BOTON_PANICO)
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-type")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.httpBody = """
            {
            "title": "Evento en curso",
            "category": "\(categoria)",
            "testimony": true,
            "description": "\(descripcion)",
            "date": "\(fecha)",
            "time": "\(hora)",
            "location_latitude": \(latitud),
            "location_longitude": \(longitud),
            "location_address": "\(direccion)",
            "location_country": "\(pais)",
            "location_state": "\(estado)",
            "location_city": "\(municipio)",
            "attachments": \(ligas),
            "location_zip_code": "\(cp)"
            \(group_id)
            }
            """.data(using: .utf8)

        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil {   // No hay error
                if let response = response {
                    let respuesta = response as! HTTPURLResponse
                    if respuesta.statusCode == OK_REGISTRO_PANICO_CREADO {
                        if data != nil {
                            // Mensaje de éxito
                            self.agregarMensajeConsola("\nSe ha grabado el evento en el servidor\n")
                        }
                    } else if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                        self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                    }
                } else {
                    self.agregarMensajeConsola("\nError al enviar el evento")
                }
            } else {
                if let error = error {
                    let tipoError = error as NSError
                    if tipoError.code == ERROR_CONEXION_INTERNET {
                        self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error, al enviar tu información", cerrar: false)
                    }
                }
            }
            DispatchQueue.main.async {
                self.terminaEnviar()
            }
        }
        task.resume()
    }
    
    // Envía el video al servidor
    func enviarAnexo() {
        guard let urlVideo = self.urlVideoGrabado else {
            return
        }
        
        self.agregarMensajeConsola("Enviando video")

        
        // Token del usuario
        let token = UserDefaults.standard.string(forKey: LLAVE_TOKEN) ?? "noExiste"
        var urlRequest = URLRequest(url: URL(string: URL_SERVICIO_ATTACHMENT)!)
        urlRequest.httpMethod = "POST"
        let datosBin = try? Data(contentsOf: urlVideo)
        
        let filename = "video.mp4"
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // String para separar datoss
        let boundary = UUID().uuidString
        
        urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        var data = Data()
        
        // Add the image data to the raw http request data
        data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
        data.append("Content-Disposition: form-data; name=\"file\"; filename=\"\(filename)\"\r\n".data(using: .utf8)!)
        data.append("Content-Type: video/mp4\r\n\r\n".data(using: .utf8)!)
        data.append(datosBin!)
        
        data.append("\r\n--\(boundary)--\r\n".data(using: .utf8)!)
        
        session.uploadTask(with: urlRequest, from: data, completionHandler: { data, response, error in
            
            if let response = response {
                let respuesta = response as! HTTPURLResponse
                if respuesta.statusCode == OK_ATTACHMENT_CREADO {
                    if let datosBin = data, let _ = String(data: datosBin, encoding: .utf8) {
                        //print("Body TESTIMONIO ANEXO -> \(body)")
                        let resultado = try! JSONSerialization.jsonObject(with: datosBin, options: []) as? NSDictionary ?? [:]
                        let idAnexo = resultado["id"]! as! Int
                        self.idVideoAnexo = idAnexo
                        // Enviar el reporte de evento
                        DispatchQueue.main.async {
                            self.enviarReportePanico()
                        }
                    }
                } else if respuesta.statusCode == CREDENCIALES_INVALIDAS_401 {
                    self.mostrarMensajeInfo(mensaje: MENSAJE_CREDENCIALES_INVALIDAS, cerrar: false)
                } else {
                    if let datosBin = data, let _ = String(data: datosBin, encoding: .utf8) {
                        //print("BodyError-> \(body)")
                    }
                    self.mostrarMensajeInfo(mensaje: "Error al enviar el video", cerrar: false)
                    DispatchQueue.main.async {
                        self.terminaEnviar()
                    }
                }
                
            } else {
                if let error = error {
                    //print("Error desconocido, \(error)")
                    let tipoError = error as NSError
                    if tipoError.code == ERROR_CONEXION_INTERNET {
                        self.mostrarMensajeInfo(mensaje: "Error, revisa tu conexión a Internet e intenta nuevamente", cerrar: false)
                    } else {
                        self.mostrarMensajeInfo(mensaje: "Error, al enviar el anexo", cerrar: false)
                    }
                    DispatchQueue.main.async {
                        self.terminaEnviar()
                    }
                }
            }
        }).resume()
    }
    
    func terminaEnviar() {
        // Habilita back y pickerview
        self.navigationItem.hidesBackButton = false
        self.pickerTipoEvento.isUserInteractionEnabled = true
        self.activEspera.stopAnimating()
        self.btnGrabarVideo.isEnabled = true

        UIApplication.shared.isIdleTimerDisabled = false
    }
    
    // Muestra un mensaje de información en la pantalla
    func mostrarMensajeInfo(mensaje: String, cerrar: Bool) {
        DispatchQueue.main.async {
            let alerta = UIAlertController(title: "Aviso", message: mensaje, preferredStyle: .alert)
            let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: { (alerta) in
                if mensaje == MENSAJE_CREDENCIALES_INVALIDAS {
                    self.navigationController?.popToRootViewController(animated: true)
                } else
                if cerrar {
                    self.navigationController?.popViewController(animated: true)
                }
            })
            alerta.addAction(aceptar)
            self.present(alerta, animated: true, completion: nil)
        }
    }
    
    // Para desplegar el mapa
    func obtenerURL()->String {
        var latitud = 0.0
        var longitud = 0.0
        if let posicion = self.posicionActual {
            latitud = posicion.coordinate.latitude
            longitud = posicion.coordinate.longitude
        }
        let url = "https://www.google.com/maps/search/?api=1&query=\(latitud),\(longitud)"
        return url
    }
    
    func obtenerFechaActual() -> String {
        let fecha = Date()
        let formato = DateFormatter()
        formato.dateFormat = "yyyy-MM-dd"
        return formato.string(from: fecha)
    }
    
    func obtenerHoraActual() -> String {
        let fecha = Date()
        let calendario = Calendar.current
        let hora = calendario.component(.hour, from: fecha)
        let minuto = calendario.component(.minute, from: fecha)
        return "\(hora):\(minuto)"
    }
}

extension TestimonioVC: GPS_Delegado
{
    // MARK: - GPS_Delegado
    func gps(nuevaPosicion: CLLocation) {
        if let posicion = self.posicionActual {
            if posicion.distance(from: nuevaPosicion) > 30 {
                self.posicionActual = nuevaPosicion
                self.agregarMensajeConsola("Posición actualizada")
            }
        } else {
            self.posicionActual = nuevaPosicion
            self.agregarMensajeConsola("Posición actualizada")
        }
    }
    
    func gps(nuevaDireccion: String) {
        
    }
    
    func gps(nuevosDatos: [String : Any]) {
        diccionarioInfo = nuevosDatos
    }
    
    func mostrarMensaje(mensaje: String) {
        DispatchQueue.main.async {
            let alerta = UIAlertController(title: "Aviso", message: mensaje, preferredStyle: .alert)
            let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: nil)
            alerta.addAction(aceptar)
            self.present(alerta, animated: true, completion: nil)
        }
    }
    
    // MARK: - Auxiliares
    func agregarMensajeConsola(_ mensaje: String) {
        DispatchQueue.main.async {
            self.tvConsola.text.append(mensaje)
            self.tvConsola.text.append("\n")
            let longitud = self.tvConsola.text.count-1
            self.tvConsola.scrollRangeToVisible( NSMakeRange(longitud, 1))
        }
    }
}

// MARK: - Picker TipoEvento
extension TestimonioVC: UIPickerViewDataSource, UIPickerViewDelegate
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrTipoEvento.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrTipoEvento[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.tipoEvento = arrTipoEvento[row]
    }
}

extension TestimonioVC: DelegadoGrabadorVideo
{
    func terminaGrabacion(urlArchivo: URL) {
        self.vistaVideo.isHidden = true
        self.agregarMensajeConsola("Video listo para enviarse")
        self.cambiarBoton(titulo: "Iniciar Grabación", normal: true)
        // Enviar anexo, obtiene ligaAnexo
        self.urlVideoGrabado = urlArchivo
        self.enviarEventoPanico()
    }
}
